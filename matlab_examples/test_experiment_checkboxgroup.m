clc
clear all
addpath('../matlab_bridge')
BINPATH = '../bin/production';
javarmpath(BINPATH);
javaaddpath(BINPATH);

apikey = fileread('devkey');
URI = 'https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a5896b0c/system/140_item/resources/set1/100.mp3';

for ii = 1:5
    imageurl_val{ii} = ['https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a5896b0c/system/140_item/resources/img/SAM_V', num2str(ii) ,'.png'];
end

%% EXPERIMENT %%
exid = sprintf('a9f56a58-aaaa-eeee-1355-012345678904');
exname = 'checkboxgroup test';
exdesc = 'Description of the checkboxgroup test.';
exfooter = 'Footer for the checkboxgroup test';
ex = cosexperiment('name', exname, 'id', exid, 'LockQuestion', 0,'EnablePrevious', 1, 'CreatedBy', 'Jens Madsen', 'ExperimentDescription', exdesc, 'FooterLabel', exfooter);

%% TRIAL 1 %%
trial1 = coscreatetrial(ex);
cosaddheader(trial1, 'HeaderLabel', '{{center|checkboxgroup test}}');


cosaddcheckboxgroup(trial1, 'HeaderLabel', 'checkboxgroup (AlignForStimuli=1)','Items', {'yes','no'}, 'AlignForStimuli', 1, 'ValidationID', '0:yes');
cosaddcheckboxgroup(trial1, 'HeaderLabel', 'checkboxgroup (AlignForStimuli=0)','Items', {'yes','no'}, 'AlignForStimuli', 0);
cosaddcheckboxgroup(trial1, 'HeaderLabel', 'checkboxgroup with stimuli','Items', {'yes','no'}, 'AlignForStimuli', 1,'Stimulus', {URI, 'This is my stimuli Label'});


%% TRIAL 2 %%
trial2 = coscreatetrial(ex);
cosaddheader(trial2, 'HeaderLabel', '{{center|checkboxgroup test}}');

itemsword = {'bla1', 'bla12', 'bla123', 'bla1234', 'bla12345', 'bla123456', 'bla1234567', 'bla12345678', 'bla123456789', 'bla1234567890', 'blablabla', 'blablablabla', ...
            'bla bla bla bla bla bla bla bla', 'bla bla bla bla bla bla bla bla bla', 'bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla', '{{center|centered bla}}',...
            '{{n}}{{center|centered bla}}', '{{n}}{{n}}{{b|bold bla}}', 'bla', 'bla', 'bla', 'bla', 'bla', 'bla', 'bla', 'bla', 'bla'};

cosaddcheckboxgroup(trial2, 'HeaderLabel', 'checkboxgroup (AlignForStimuli=1, minSelect=2,maxSelect=10)','Items', itemsword, 'AlignForStimuli', 1,'minNoOfSelections', 2,'maxNoOfSelections', 10, 'ValidationId', '0:bla1|1:bla12');
cosaddcheckboxgroup(trial2, 'HeaderLabel', 'checkboxgroup (AlignForStimuli=0, minSelect=10,maxSelect=10))','Items', itemsword, 'AlignForStimuli', 0,'minNoOfSelections', 10,'maxNoOfSelections', 10);
cosaddcheckboxgroup(trial2, 'HeaderLabel', 'checkboxgroup with stimuli','Items', itemsword, 'AlignForStimuli', 1,'Stimulus', {URI, 'This is my stimuli Label'});


%% TRIAL 2 %%
trial3 = coscreatetrial(ex);

items = {'Lovely','Great','Awesome','Something really really long text that simply never stops','{{n}}{{b|sleepy}}{{n}}{{b|tired}}{{n}}{{b|drowsy}}',...
    ['{{image|' imageurl_val{1} '|124|133|center}}'],['{{image|' imageurl_val{2} '|124|133|center}}']};

cosaddcheckboxgroup(trial3, 'HeaderLabel', 'Radiobuttongroup with stimuli','Items', items, 'AlignForStimuli', 1,'Stimulus', {URI, 'This is my stimuli Label'});
cosaddcheckboxgroup(trial3, 'HeaderLabel', '{{b|How do you like the hair?}}{{n}}{{n}}{{image|http://sites.psu.edu/juanitauribercl/wp-content/uploads/sites/38826/2016/01/donald-trumps-hair_zpswqc4ev8g.jpg|300|339|center}}','Items', items, 'AlignForStimuli', 1);

%% TRIAL 3 %%
trial4 = coscreatetrial(ex);
cosaddheader(trial4, 'HeaderLabel', 'Thank you for your participation');

%% End experiment with experiment method:
ex.endExperiment;

%% Write XML to file
xml = ex.getXMLRepresentation();

fid = fopen('../xml_examples/checkboxgrouptest.xml', 'w+');
fprintf(fid, '%s', char(xml));
fclose(fid);

cosuploadexperiments(apikey, 'env', 'dev', ex);
