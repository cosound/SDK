% Absolute or relative path to jar-file
clear all, clc;
BINPATH = '../bin/production';
javarmpath(BINPATH);
javaaddpath(BINPATH);
addpath('../matlab_bridge');
ex = com.Utilities.ChaosSDK.loadExperimentFromFile('test.xml');
a=getAnswers(ex);

% Answers will be in nested array
answers = ex.getAnswers;

% prod_guid = ...
chaos = com.Utilities.ChaosSDK(prod_guid, 'prod');


files = {'dtu_emotion_likert_001_e3cc09b6-f3ee-ef7e-0001-000000000001.xml',...
         'dtu_emotion_likert_001_e3cc09b6-f3ee-ef6e-0001-000000000001.xml',...
         'dtu_emotion_likert_001_e3cc09b6-f3ee-ef5e-0001-000000000001.xml',...
         'dtu_emotion_likert_001_e3cc09b6-f3ee-ef4d-0001-000000000001.xml',...
         'dtu_emotion_likert_001_e3cc09b6-f3ee-cf3c-0001-000000000002.xml',...
         'dtu_emotion_likert_001_e3cc09b6-f3ee-af3c-0001-000000000001.xml'};
basedir = '../xml_examples/';
for f=files
    % Load either via com.Utilities.ChaosSDK-object or statically
    ex = chaos.loadExperimentFromFile([basedir f{1}]);
    ex = com.Utilities.ChaosSDK.loadExperimentFromFile([basedir f{1}]);
end


% Write to disk in XML-format same way (will fail if file already exists)
%chaos.writeExperimentToFile(ex, 'filename.xml');
%com.Utilities.ChaosSDK.writeExperimentToFile(ex, 'filename.xml');

for i=1:480
	exid = sprintf('e3cc09b6-f3ee-af3c-0001-000000000%03d', i);
    ex = chaos.getObject(exid);
    answers = cell(ex.getAnswers());
    fprintf(['Parsed experiment ' exid '\n']);
end

for i=2:325
    exid = sprintf('e3cc09b6-f3ee-cf3c-0001-000000000%03d', i);
	ex = chaos.getObject(exid);
    answers = cell(ex.getAnswers());
    fprintf(['Parsed experiment ' exid '\n']);
end

for i=1:100
    exid = sprintf('e3cc09b6-f3ee-ef4d-0001-000000000%03d', i);
	ex = chaos.getObject(exid);
    answers = cell(ex.getAnswers());
    fprintf(['Parsed experiment ' exid '\n']);
end

for i=1:100
    exid = sprintf('e3cc09b6-f3ee-ef6e-0001-000000000%03d', i);
    ex = chaos.getObject(exid);
    answers = cell(ex.getAnswers());
    fprintf(['Parsed experiment ' exid '\n']);
end

for i=1:100
    exid = sprintf('e3cc09b6-f3ee-ef7e-0001-000000000%03d', i);
    ex = chaos.getObject(exid);
    answers = cell(ex.getAnswers());
    fprintf(['Parsed experiment ' exid '\n']);
end
