clc
clear all
addpath('../matlab_bridge')
BINPATH = '../bin/production';
javarmpath(BINPATH);
javaaddpath(BINPATH);

apikey = fileread('../matlab_examples/devkey');
URI = 'https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a5896b0c/system/140_item/resources/set1/100.mp3';

for ii = 1:5
    imageurl_val{ii} = ['https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a5896b0c/system/140_item/resources/img/SAM_V', num2str(ii) ,'.png'];
end



%% EXPERIMENT %%
exid = sprintf('e3cc09b6-f3ee-af3c-0001-223c3ae2df3d');
exname = 'OneDScale test';
exdesc = 'Description of the OneDScale test.';
exfooter = 'Footer for the OneDScale test';
ex = cosexperiment('name', exname, 'id', exid, 'LockQuestion', 0,...
    'EnablePrevious', 1, 'CreatedBy', 'Anders Kirk Uhrenholt',...
    'ExperimentDescription', exdesc, 'FooterLabel', exfooter);

%% TRIAL 1 %%
trial1 = coscreatetrial(ex);

x1ticks = {{'First x1 tick', -0.5}, {'Second x1 tick', 0.0}, {'Third x1 tick', 0.5}};
x2ticks = {{'First x2 tick', -1}, {'Second x2 tick', 0.0}, {'Third x2 tick', 1}};

cosaddonedscale(trial1, 'HeaderLabel', 'OneDScale Header Label', 'X1Ticks',...
    x1ticks', 'X2Ticks', x2ticks', 'Y1Ticks', {{'Only y1 tick', 0.5}},...
    'Y2Ticks', {{'Only y2 tick', -0.5}}, 'AlignForStimuli', 0, 'Position', 0.9,...
    'X1AxisLabel', 'Label for X1 axis', 'X2AxisLabel', 'Label for X2 axis', ...
    'Y1AxisLabel', 'Label for Y1 axis', 'Y2AxisLabel', 'Label for Y2 axis');


cosuploadexperiments(apikey, 'env', 'dev', ex);
