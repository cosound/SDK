clc
clear all
addpath('../matlab_bridge')
BINPATH = '../bin/production';
javarmpath(BINPATH);
javaaddpath(BINPATH);

apikey = fileread('prodkey');
URI = 'https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a5896b0c/system/140_item/resources/set1/100.mp3';

%% EXPERIMENT %%
exid = sprintf('a9f56a58-aaaa-eeee-1355-012345678906');
exname = 'Tagging test';
exdesc = 'Description of the Tagging test.';
exfooter = 'Footer for the Tagging test';
ex = cosexperiment('name', exname, 'id', exid, 'LockQuestion', 0, 'EnablePrevious', 1, 'CreatedBy', 'Jens Madsen', 'ExperimentDescription', exdesc, 'FooterLabel', exfooter);

%% TRIAL 1 %%
trial1 = coscreatetrial(ex);

cosaddheader(trial1, 'HeaderLabel', '{{center|Tagging A test}}');


cosaddtagginga(trial1, 'HeaderLabel', 'TA: Header Label w. stimulus',...
    'SelectionTagBoxLabel', 'Selection tags', 'UserTagBoxLabel', 'User tags',...
    'TextField', 'Tags go here', 'SelectionTags', {'ST1', 'ST2', 'ST3'},...
    'UserTags', {'UT1', 'UT2', 'UT3'}, 'Stimulus', {URI, 'My Stimulus'});

cosaddtagginga(trial1, 'HeaderLabel', 'TA: Header Label no stimulus',...
    'SelectionTagBoxLabel', 'Selection tags', 'UserTagBoxLabel', 'User tags',...
    'TextField', 'Tags go here', 'SelectionTags', {'ST1', 'ST2', 'ST3'},...
    'UserTags', {'UT1', 'UT2', 'UT3'});

cosaddtagginga(trial1, 'HeaderLabel', 'TA: Header Label no usertags',...
    'SelectionTagBoxLabel', 'Selection tags', 'TextField', 'Tags go here', 'SelectionTags', {'ST1', 'ST2', 'ST3'});

cosaddtagginga(trial1, 'HeaderLabel', 'TA: Header Label no tags','TextField', 'Tags go here');

cosaddtagginga(trial1, 'HeaderLabel', 'TA: Header Label nothing');


%% TRIAL 2 %%
trial2 = coscreatetrial(ex);

cosaddheader(trial2, 'HeaderLabel', '{{center|Tagging B test}}');

cosaddtagginga(trial2, 'HeaderLabel', 'TB: Header Label w. stimulus',...
    'SelectionTagBoxLabel', 'Selection tags', 'UserTagBoxLabel', 'User tags',...
    'TextField', 'Tags go here', 'SelectionTags', {'ST1', 'ST2', 'ST3'},...
    'UserTags', {'UT1', 'UT2', 'UT3'}, 'Stimulus', {URI, 'My Stimulus'});

cosaddtagginga(trial2, 'HeaderLabel', 'TB: Header Label no stimulus',...
    'SelectionTagBoxLabel', 'Selection tags', 'UserTagBoxLabel', 'User tags',...
    'TextField', 'Tags go here', 'SelectionTags', {'ST1', 'ST2', 'ST3'},...
    'UserTags', {'UT1', 'UT2', 'UT3'});

cosaddtagginga(trial2, 'HeaderLabel', 'TB: Header Label no usertags',...
    'SelectionTagBoxLabel', 'Selection tags', 'TextField', 'Tags go here', 'SelectionTags', {'ST1', 'ST2', 'ST3'});

cosaddtagginga(trial2, 'HeaderLabel', 'TB: Header Label no tags','TextField', 'Tags go here');

cosaddtagginga(trial2, 'HeaderLabel', 'TB: Header Label nothing');


cosuploadexperiments(apikey, 'env', 'dev', ex);
