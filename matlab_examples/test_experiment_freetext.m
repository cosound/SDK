clc
clear all
addpath('../matlab_bridge')
BINPATH = '../bin/production';
javarmpath(BINPATH);
javaaddpath(BINPATH);

apikey = fileread('devkey');

%% EXPERIMENT %%
exid = sprintf('a9f56a58-aaaa-eeee-1355-012345678901');
exname = 'freetext test';
exdesc = 'Description of the freetext test.';
exfooter = 'Footer for the freetext test';
ex = cosexperiment('name', exname, 'id', exid, 'LockQuestion', 0,'EnablePrevious', 1, 'CreatedBy', 'Jens Madsen', 'ExperimentDescription', exdesc, 'FooterLabel', exfooter);

%% TRIAL 1 %%
trial1 = coscreatetrial(ex);
cosaddheader(trial1, 'HeaderLabel', 'Freetext test');

cosaddfreetext(trial1, 'Label', 'Only digits here (LabelPosition=top)', 'LabelPosition',...
    'top', 'Validation', '^[0-9]+$');

cosaddfreetext(trial1, 'Label', 'Any char here (LabelPosition=right)', 'LabelPosition',...
    'right', 'Validation', '.+');

cosaddfreetext(trial1, 'Label', 'This wants an email (LabelPosition=left)', 'LabelPosition',...
    'left', 'Validation', '[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}');


%% TRIAL 2 %%
trial2 = coscreatetrial(ex);
cosaddheader(trial2, 'HeaderLabel', 'Thank you for your participation');

%% End experiment with experiment method:
ex.endExperiment;

%% Write XML to file
xml = ex.getXMLRepresentation();

fid = fopen('../xml_examples/freetexttest.xml', 'w+');
fprintf(fid, '%s', char(xml));
fclose(fid);

cosuploadexperiments(apikey, 'env', 'dev', ex);
