% Absolute or relative path to jar-file
clear all
clc
addpath('../../../../wp2x-cockpit/03_implementation/experimentgenerator/debug/functions/')
BINPATH = '../bin/production';
javarmpath(BINPATH);
javaaddpath(BINPATH);

%% stimuli
experiment_list = sprintf('https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a5896b0c/system/140_item/resources/set1/%03d.mp3',1);

excerptList = [];
for k = 1:360
    excerptList{size(excerptList,1)+1,1}=sprintf('https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a5896b0c/system/140_item/resources/set1/%03d.mp3',k);
end



%% EXPERIMENT %%
ex = com.MetaComponents.Experiment;
ex.name = 'Cockpit:Test';
ex.id = 'a9f56a58-aaaa-eeee-1351-fa435ed70004';

ex.lockQuestion = false;
ex.enablePrevious = true;
ex.createdBy = 'Jens Madsen';
ex.experimentDescription = 'This experiment tests fucktionalities of the Cockpit SDK. In this case the implementation of the STOMP experiment.';


% com.Components should be added through trial objects, i.e.
% >> trialObject.addHeader('com.Components.Header text');

%% TRIAL 1 %%
% Non-interactable components. Just needs a string for initialization.
trial1 = ex.createTrial();
trial1.addHeader('{{center|Welcome to this experiment}}');

%% TRIAL 2 %%
% checkboxOptions = {'Option 1', 'Option 2', '', 'Option 4', 'Option 5'};

% trial2 = ex.createTrial();
% trial2.addHeader('{{center|This is a bunch of Checkboxes}}');
% trial2.addCheckBoxGroup('This is my label (min=1, max=5)',checkboxOptions);
% trial2.addCheckBoxGroup('This is my label (min=2, max=5)',checkboxOptions,2);
% trial2.addCheckBoxGroup('This is my label (min=2, max=3)',checkboxOptions,2,3);
% trial2.addCheckBoxGroup('This is my label (min=2, max=3,alignForStimuli=1)',checkboxOptions,2,3,1);
% trial2.addCheckBoxGroup('This is my label (min=2, max=3,alignForStimuli=0)',checkboxOptions,2,3,0);
% trial2.addCheckBoxGroup('This is my label (min=2, max=3, alignForStimuli=1, stimuli)',checkboxOptions,2,3,1,{excerptList{1}, 'This is my stimuli Label'});


radiobuttonOptions = {'low','','medium','','high'};

trial2 = ex.createTrial();
trial2.addHeader('{{center|This is a bunch of addRadioButtonGroups}}');
trial2.addRadioButtonGroup('This is my label (alignForStimuli = 0)',radiobuttonOptions);
trial2.addRadioButtonGroup('This is my label (alignForStimuli = 1)',radiobuttonOptions,1);
trial2.addRadioButtonGroup('This is my label (alignForStimuli = 1, with stimuli)',radiobuttonOptions,1,{excerptList{1}, 'This is my stimuli Label'});

% alignForStimuli = 0

% trial2.addCheckBoxGroup('This is my label (min=2, max=5)',checkboxOptions,2);
% trial2.addCheckBoxGroup('This is my label (min=2, max=3)',checkboxOptions,2,3);
% trial2.addCheckBoxGroup('This is my label (min=2, max=3,alignForStimuli=1)',checkboxOptions,2,3,1);
% trial2.addCheckBoxGroup('This is my label (min=2, max=3,alignForStimuli=0)',checkboxOptions,2,3,0);
% trial2.addCheckBoxGroup('This is my label (min=2, max=3, alignForStimuli=1, stimuli)',checkboxOptions,2,3,1,{excerptList{1}, 'This is my stimuli Label'});


%% TRIAL 3 %%
% trial3 = ex.createTrial();
% trial3.addFreeText('Occupation', 'top', 100, 100, 1, '.*');


%% 
% %% TRIAL 3 %%
% trial3 = ex.createTrial();
% trial3.addTextBlock('For the following items, please indicate your basic preference level for the genres listed using the scale provided.');
% 
%     likertOptions = {...
%         'Strongly dislike';
%         '';
%         '';
%         'Neither like nor dislike';
%         '';
%         '';
%         'Strongly like'
%         };
% 
% likertQ = {...
%         'Classical';
%         'Blues';
%         'Country';
%         'Dance/Electronica';
%         'Folk';
%         'Rap/hip-hop';
%         'Soul/funk';
%         'Religious';
%         'Alternative';
%         'Jazz';
%         'Rock';
%         'Pop';
%         'Heavy Metal';
%         'Soundtracks/theme songs';
%         };
%     
% 
% for ii = 1:size(likertQ,1)
% %     trial2.addCheckBoxGroup('HeaderLabel', {'',''}, 1,1);
%     trial3.addLikertScale(likertQ{ii},likertOptions,0);
% end


%% TRIAL 4 %%
trial4 = ex.createTrial();
trial4.addHeader('{{center|This is the end}}');
trial4.addTextBlock('My only friend, the end!');

%% End experiment with experiment method:
 ex.endExperiment;

%% Verify experiment by ASCII representation:
ex.getTextRepresentation

%% Verify experiment by XML representation:
ex.getXMLRepresentation()

%get status of experiment
ex.getStateReport()

%get revision ID
ex.revisionID

%get experiment ID
ex.id


%% commit stuff
% apikey = loadCSApiKey
% chaos = com.Utilities.ChaosSDK(loadCSApiKey,'prod');

chaos = com.Utilities.ChaosSDK('a4dfcd16da84ab06ccbe6e0786fd67a9aeae0f0ab432fcee3d502c9c97e08062','dev');

try
    chaos.deleteObject(ex.id);
catch
   fprintf('Cannot delete\n') 
end

% Commit experiment to Cockpit server. If the experiment is not in a valid
% state (fx if no trials have been added), and exception will be thrown. A
% detailed account of errors to be fixed can be obtained by
% >> ex.getStateReport()
% Note that if no ID has been set, a random will be created
for i=1:10
    try
        tic
        chaos.reindexDatabase();
        toc
        chaos.commitExperiment(ex,'debug');
        break
    catch
        toc
        fprintf('Retrying %i/10...\n', i);
    end
end


%% Retrieving experiment after user has reponded
% User response is not registered until after another reindexation
chaos.reindexDatabase();

finishedEx = chaos.getObject(ex.id);
finishedEx.getTextRepresentation()
