clc
clear all
addpath('../matlab_bridge')
BINPATH = '../bin/production';
javarmpath(BINPATH);
javaaddpath(BINPATH);

apikey = fileread('devkey');
URI = 'https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a5896b0c/system/140_item/resources/set1/100.mp3';

for ii = 1:5
    imageurl_val{ii} = ['https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a5896b0c/system/140_item/resources/img/SAM_V', num2str(ii) ,'.png'];
end



%% EXPERIMENT %%
exid = sprintf('a9f56a58-aaaa-eeee-1355-012345678902');
exname = 'likertscale test';
exdesc = 'Description of the likertscale test.';
exfooter = 'Footer for the likertscale test';
ex = cosexperiment('name', exname, 'id', exid, 'LockQuestion', 0,'EnablePrevious', 1, 'CreatedBy', 'Jens Madsen', 'ExperimentDescription', exdesc, 'FooterLabel', exfooter);

%% TRIAL 1 %%
trial1 = coscreatetrial(ex);
cosaddheader(trial1, 'HeaderLabel', 'Likertscale test');

    likertOptions = {...
        '1{{n}}{{b|sleepy}}{{n}}{{b|tired}}{{n}}{{b|drowsy}}';...
        '2';...
        '3';...
        '4';...
        '5{{n}}{{b|neutral}}';...
        '6';...
        '7';...
        '8';...
        '9{{n}}{{b|alert}}{{n}}{{b|wakeful}}{{n}}{{b|awake}}';...
        };

cosaddlikertscale(trial1, 'HeaderLabel', 'Question for the likert scale (AlignForStimuli=0)', 'items', likertOptions, 'AlignForStimuli', 0);

cosaddlikertscale(trial1, 'HeaderLabel', 'Question for the likert scale (AlignForStimuli=1)', 'items', likertOptions, 'AlignForStimuli', 1);
    
cosaddlikertscale(trial1, 'HeaderLabel', 'Question with sound stimuli','Items', likertOptions, 'AlignForStimuli', 1, 'Stimulus', {URI, 'Music excerpt'});

%% TRIAL 2 %%
trial2 = coscreatetrial(ex);
cosaddheader(trial2, 'HeaderLabel', 'Likertscale test');

cosaddlikertscale(trial2, 'HeaderLabel', 'Question for the likert scale (MinNoOfScalings=0)', 'items', likertOptions, 'AlignForStimuli', 0, 'MinNoOfScalings', 0);

cosaddlikertscale(trial2, 'HeaderLabel', 'Question for the likert scale (only answer=2)', 'items', likertOptions, 'AlignForStimuli', 1, 'ValidationID', '1:2');

cosaddlikertscale(trial2, 'HeaderLabel', 'Question for the likert scale (only answer=2 or 3)', 'items', likertOptions, 'AlignForStimuli', 1, 'ValidationID', '1:2|2:3');

%% TRIAL 3 %%
trial3 = coscreatetrial(ex);
cosaddheader(trial3, 'HeaderLabel', 'Pictures as ticklabels');

  likertOptionsVal = {...
        ['{{image|' imageurl_val{1} '|124|133|center}}'];...
        '';...
        ['{{image|' imageurl_val{2} '|124|133|center}}'];...
        '';...
        ['{{image|' imageurl_val{3} '|124|133|center}}'];...
        '';...
        ['{{image|' imageurl_val{4} '|124|133|center}}'];...
        '';...
        ['{{image|' imageurl_val{5} '|124|133|center}}'];...
        };
    
cosaddlikertscale(trial3, 'HeaderLabel', 'Question with pictures as ticklabels (AlignForStimuli=0)', 'AlignForStimuli', 0, 'Items', likertOptionsVal);

cosaddlikertscale(trial3, 'HeaderLabel', 'Question with pictures as ticklabels (AlignForStimuli=1)', 'AlignForStimuli', 1, 'Items', likertOptionsVal);
    
cosaddlikertscale(trial3, 'HeaderLabel', 'Question with pictures as ticklabels with stimuli(AlignForStimuli=0)', 'AlignForStimuli', 0, 'Items', likertOptionsVal,'Stimulus', {URI, 'Music excerpt'});


%% TRIAL 4 %%
trial4 = coscreatetrial(ex);
cosaddtextblock(trial4, 'text', 'This is a trial with no header or components');

%% TRIAL 5 %%
trial5 = coscreatetrial(ex);
cosaddheader(trial5, 'HeaderLabel', 'This tests likert scale with images');

cosaddlikertscale(trial5, 'HeaderLabel', '{{b|How do you like the hair?}}{{n}}{{n}}{{image|http://sites.psu.edu/juanitauribercl/wp-content/uploads/sites/38826/2016/01/donald-trumps-hair_zpswqc4ev8g.jpg|300|339|center}}', 'AlignForStimuli', 0, 'Items', likertOptionsVal);


%% TRIAL 6 %%
trial6 = coscreatetrial(ex);
cosaddheader(trial6, 'HeaderLabel', 'Thank you for your participation');

%% End experiment with experiment method:
ex.endExperiment;

%% Write XML to file
xml = ex.getXMLRepresentation();

fid = fopen('../xml_examples/likertscaletest.xml', 'w+');
fprintf(fid, '%s', char(xml));
fclose(fid);

cosuploadexperiments(apikey, 'env', 'dev', ex);
