clc
clear all
addpath('../matlab_bridge')
BINPATH = '../bin/production';
javarmpath(BINPATH);
javaaddpath(BINPATH);

apikey = fileread('../matlab_examples/devkey');
noexperiments = 5;
experiments = cell(noexperiments,1);
for i = 1:noexperiments
    exid = sprintf('e3cc09b6-f3ee-af3c-0001-223c3ae2d%03d', i); 
    exname = sprintf('Experiment List Test %d', i); 
    ex = cosexperiment('name', exname, 'id', exid, 'CreatedBy', 'Anders Kirk Uhrenholt');
    
    %% TRIAL 1 %%
    trial1 = coscreatetrial(ex);
    cosaddheader(trial1, 'HeaderLabel', sprintf('Experiment %d', i));
    
    experiments(i) = ex;
end

cosuploadexperimentlist(apikey, experiments, 'env', 'dev');
