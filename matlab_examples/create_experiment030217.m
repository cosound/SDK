clc
clear all
addpath('../../../../wp2x-cockpit/03_implementation/experimentgenerator/debug/functions/')
addpath('../matlab_bridge')
BINPATH = '../bin/production';
javarmpath(BINPATH);
javaaddpath(BINPATH);
apikey = fileread('devkey');

load('../../../../wp2x-cockpit/03_implementation/experimentgenerator/dtuemotion/data/explist_bias_16.mat')

% explist_confidence_50 = importdata('C:\Users\jenma\Desktop\Dropbox\Experiments\confidence_experiments\experiment_2\confidence_stimuli_sequence.mat');

%% images
for ii = 1:5
    imageurl_val{ii} = ['https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a5896b0c/system/140_item/resources/img/SAM_V', num2str(ii) ,'.png'];
end

for ii = 1:5
    imageurl_aro{ii} = ['https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a5896b0c/system/140_item/resources/img/SAM_A', num2str(ii) ,'.png'];
end
%% stimuli
% experiment_list = sprintf('https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a5896b0c/system/140_item/resources/set1/%03d.mp3',1);

no_bias = 4;
no_bias_rand = 1;

iExp = 1;
iBias = 100;
iBias_rand = 100;

for cyc = 1:20
    % confidence experiment
    for ee = 1:no_bias
%         experiment_list_bias{iExp} = explist_bias_16(iBias,:);
        experiment_list_bias{iExp} = randi(10,1);
        iExp = iExp+1;
        iBias = iBias+1;
    end
    
    for ee = 1:no_bias_rand
%         experiment_list_bias{iExp} = explist_bias_rand_16(iBias_rand,:);
        experiment_list_bias{iExp} = randi(10,1);
        iExp = iExp+1;
        iBias_rand = iBias_rand+1;
    end
end

excerptList = [];
for k = 1:360
    excerptList{size(excerptList,1)+1,1}=sprintf('https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a5896b0c/system/140_item/resources/set1/%03d.mp3',k);
end

excerptListSet3 = [];
for k = 1:140
    excerptListSet3{size(excerptListSet3,1)+1,1}=sprintf('https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a5896b0c/system/140_item/resources/set3/%03d.mp3',k);
end


for iExp=1
    %% EXPERIMENT %%
    exid = sprintf('a9f56a58-aaaa-eeee-1355-aa435ed70%03d',iExp);
    exname = 'MusicEmotion';
    exdesc = 'This experiment investigates the confidence effect of music emotions elicitation.';
    exfooter = ['For questions or comments contact us via this {{link|mailto:jenma@dtu.dk?subject=Comment%20about%20',exname,'&body=%0D%0A%0D%0A%0D%0A%0D%0A%0D%0AExperiment%20ID%0D%0A(',exid,')|e-mail}}.'];
    ex = cosexperiment('name', exname, 'id', exid, 'LockQuestion', 0,...
        'EnablePrevious', 0, 'CreatedBy', 'Jens Madsen',...
        'ExperimentDescription', exdesc, 'FooterLabel', exfooter);    
    
    %% TRIAL 1 %%
    % Non-interactable components. Just needs a string for initialization.
    % expLength = 45;
    %
    % trial1 = ex.createTrial();
    % trial1.addHeader('{{center|Welcome to this experiment}}');
    % trial1.addTextBlock(['{{n}}{{n}}{{center|{{style|font-size: 16px;|Thank you for your interest and your participation in this listening experiment.}}}}',...
    %                '{{n}}{{n}}{{center|{{style|font-size: 16px;|The experiment focuses on the emotions expressed in music.}}}}',...
    %                '{{n}}{{n}}{{center|{{style|font-size: 16px;|It will take approximately ', num2str(expLength) ,' minutes to complete. You can follow your progress in the bottom right corner.}}}}',...
    %                '{{n}}{{n}}{{center|{{style|font-size: 16px;|The experiment is divided in two parts, in between these two parts you can take a short brake if you need.}}}}',...
    %                '{{n}}{{n}}{{center|{{style|font-size: 16px;|In the end of the experiment, you will be presented with a link you press to get your free cinema ticket.}}}}']);
    %% TRIAL 2 %%
    % trial2 = ex.createTrial();
    % trial2.addHeader('{{center|Instructions}}');
    % trial2.addTextBlock(['{{n}}{{n}}{{center|{{style|font-size: 16px;|The experiment concentrates on rating musical excerpts using the emotions expressed in the music.}}}}{{n}}' , ...
    %         '{{center|{{style|font-size: 16px;|The emotions expressed in music are different from how the music makes you feel.}}}}{{n}}' , ...
    %         '{{center|{{style|font-size: 16px;|So focus on what emotions the music expresses and try the best you can not to rate excerpts based on how the music makes you feel.}}}}{{n}}' , ...
    %         '{{center|{{style|font-size: 16px;|For example, you think some music sounds very negative, but for some reason the music makes you feel positive.}}}}{{n}}' , ...
    %         '{{center|{{style|font-size: 16px;|Then you should not rate it as being positive but rather negative, since this is what you think the music expresses.}}}}{{n}}']);
    %
    
    
    %% TRIAL 3 %%
    % trial3 = ex.createTrial();
    % trial3.addHeader('{{center|Aspects of emotions}}');
    % trial3.addTextBlock(['{{n}}{{n}}{{center|{{style|font-size: 16px;|To capture the emotions expressed in the music we will ask you to use the two aspects of {{b|valence}} and {{b|arousal}}. }}}}{{n}}{{n}}' , ...
    %         '{{center|{{style|font-size: 16px;|{{b|Valence}} is described as the aspect of emotions in music that ranges from }}}} {{n}}',...
    %         '{{center|{{style|font-size: 16px;|(unpleasant/bad/negative) to (pleasant/good/positive) }}}} {{n}}{{n}}{{n}}{{n}}',...
    %         '{{center|{{style|font-size: 16px;|{{b|Arousal}} is described as the aspect of emotions in music that ranges from }}}} {{n}}',...
    %         '{{center|{{style|font-size: 16px;|(sleepy/tired/drowsy) to (alert/wakeful/awake) }}}} {{n}}{{n}}{{n}}{{n}}']);
    %
    %% TRIAL 4 %%
    % trial4 = ex.createTrial();
    % trial4.addHeader('{{center|Instructions}}');
    % trial4.addTextBlock(['{{n}}{{n}}{{center|{{style|font-size: 16px;|The experiment will be as follows, there are two different parts of the experiment.}}}}{{n}}' , ...
    %                     '{{center|{{style|font-size: 16px;|Each part will start with you answering personal questions, followed by annotation of music stimuli.}}}}{{n}}' , ...
    %                     '{{center|{{style|font-size: 16px;|You will be asked to listen to a number of excerpts and rate them on the valence and arousal scales.}}}}{{n}}' , ...
    %                     '{{center|{{style|font-size: 16px;|After each rating, you will be asked to answer how sure you were of your rating.}}}}{{n}}',...
    %                     '{{center|{{style|font-size: 16px;|Please carefully (re)evaluate each excerpt, even if you are asked to rate the same excerpt more than once.}}}}{{n}}',...
    %                     '{{center|{{style|font-size: 16px;|Remember this is not a test, there is no such thing as right or wrong answers so please answer honestly on all ratings.}}}}{{n}}',...
    %                     '{{center|{{style|font-size: 14px;|{{n}}{{n}}{{n}}Data collection and analysis is performed in accordance with the Danish Act of Processing of Personal Data.}}}}{{n}}' , ...
    %                     '{{center|{{style|font-size: 14px;|No sensitive or confidential data (name, address, social security number etc.) is saved after the completion of the experiment.}}}}{{n}}']);
    %
    %
    %% TRIAL 5 %%
    trial5 = coscreatetrial(ex);
    cosaddheader(trial5, 'HeaderLabel', '{{center|Scales}}');

    cosaddtextblock(trial5, 'Text', '{{n}}{{n}}{{center|{{style|font-size: 16px;|These are the scales you will use in the first part of the experiment to rate each of the musical excerpts.}}}}');
    
    %     % Valence
    likertQval = {'{{center|{{style|font-size: 25px;|Valence scale}}}}'};
    %
    likertOptionsVal = {...
        '1{{n}}{{b|unpleasant}}{{n}}{{b|bad}}{{n}}{{b|negative}}';...
        '2';...
        '3';...
        '4';...
        '5{{n}}{{b|neutral}}';...
        '6';...
        '7';...
        '8';...
        '9{{n}}{{b|pleasant}}{{n}}{{b|good}}{{n}}{{b|positive}}';...
        };
    
    % Arousal
    likertQaro = {'{{center|{{style|font-size: 25px;|Arousal scale}}}}'};
    
    likertOptionsAro = {...
        '1{{n}}{{b|sleepy}}{{n}}{{b|tired}}{{n}}{{b|drowsy}}';...
        '2';...
        '3';...
        '4';...
        '5{{n}}{{b|neutral}}';...
        '6';...
        '7';...
        '8';...
        '9{{n}}{{b|alert}}{{n}}{{b|wakeful}}{{n}}{{b|awake}}';...
        };
    
    cosaddlikertscale(trial5, 'HeaderLabel', likertQval,...
        'items', likertOptionsVal, 'AlignForStimuli', 0);
    cosaddlikertscale(trial5, 'HeaderLabel', likertQaro,...
        'items', likertOptionsAro, 'AlignForStimuli', 0);    
        
    %% TRIAL 6 %%
    trial6 = coscreatetrial(ex);
    cosaddheader(trial6, 'HeaderLabel', '{{center|Volume test}}');
    cosaddtextblock(trial6, 'Text', '{{n}}{{center|Please adjust the volume so that the music is played at a pleasent level}}. {{center|After you have found an appropriate listening level, please refrain from adjusting the volume throughout the experiment}}');
    cosaddradiobuttongroup(trial6, 'HeaderLabel', 'Have you adjusted the volume appropriately?',...
        'Items', {'yes','no'}, 'AlignForStimuli', 1,...
        'Stimulus', {excerptList{1}, 'This is my stimuli Label'});
        
    %% TRIAL 7 %%
    trial7 = coscreatetrial(ex);
    cosaddheader(trial7, 'HeaderLabel', '{{center|Personal information}}');
    cosaddtextblock(trial7, 'Text', 'Please answer the following questions about yourself.');
    cosaddradiobuttongroup(trial7, 'HeaderLabel', 'Do you have any hearing disorders',...
        'Items', {'Yes','No','Dont know'});
    cosaddradiobuttongroup(trial7, 'HeaderLabel', 'Gender',...
        'Items', {'Male','Female','Other'});
    
    cosaddfreetext(trial7, 'Label', 'Age', 'LabelPosition', 'top',...
        'AlignForStimuli', 1, 'Validation', '.*');
    cosaddfreetext(trial7, 'Label', 'Nationality', 'LabelPosition', 'top',...
        'AlignForStimuli', 1, 'Validation', '.*');
    cosaddfreetext(trial7, 'Label', 'Occupation', 'LabelPosition', 'top',...
        'AlignForStimuli', 1, 'Validation', '.*');
    cosaddfreetext(trial7, 'Label', 'Occupational field', 'LabelPosition', 'top',...
        'AlignForStimuli', 1, 'Validation', '.*');
    cosaddfreetext(trial7, 'Label', 'What instrument(s) do you play? (separate by comma)',...
        'LabelPosition', 'top', 'AlignForStimuli', 1, 'Validation', '.*');
    cosaddfreetext(trial7, 'Label', 'How many hour(s) a day do you actively listen to music? {{n}} (i.e. you select what music to listen to)',...
        'LabelPosition', 'top', 'AlignForStimuli', 1, 'Validation', '.*');
    cosaddfreetext(trial7, 'Label', 'How many hour(s) a day do you passively listen to music? {{n}} (e.g. radio, premade playlists, etc.)?',...
        'LabelPosition', 'top', 'AlignForStimuli', 1, 'Validation', '.*');
    cosaddfreetext(trial7, 'Label', 'How many year(s) of musical training have you had? {{n}} (e.g. playing an instrument, theoretical training, etc.)',...
        'LabelPosition', 'top', 'AlignForStimuli', 1, 'Validation', '.*');
    cosaddfreetext(trial7, 'Label', 'How many hour(s) a day do you play an instrument?',...
        'LabelPosition', 'top', 'AlignForStimuli', 1, 'Validation', '.*');
    cosaddfreetext(trial7, 'Label', 'How many times have you participated{{n}}in a listening experiment before?',...
        'LabelPosition', 'top', 'AlignForStimuli', 1, 'Validation', '.*');
    cosaddfreetext(trial7, 'Label', 'How many times have you participated{{n}}in a listening experiment regarding emotions and audio?',...
        'LabelPosition', 'top', 'AlignForStimuli', 1, 'Validation', '.*');
    
    %% TRIAL 8 %%
    trial8 = coscreatetrial(ex);
    
    likertQ2 = {'How tired would you consider yourself right now?'};
    likertQ2Options = {...
        'very tired';
        'tired';
        'somewhat tired';
        'neither tired or awake';
        'somewhat awake';
        'awake';
        'very awake'
        };
    
    cosaddlikertscale(trial8, 'HeaderLabel', likertQ2,...
        'Items', likertQ2Options, 'AlignForStimuli', 0);
    
    % Mood - own premood  x 2 (AV) likert
    likertQaro = {'How would you rate your own mood right now on the {{n}}{{n}} Arousal scale'};
    
    likertQval = {'How would you rate your own mood right now on the {{n}}{{n}} Valence scale'};

    cosaddlikertscale(trial8, 'HeaderLabel', likertQval,...
        'Items', likertOptionsVal, 'AlignForStimuli', 0);
    cosaddlikertscale(trial8, 'HeaderLabel', likertQaro,...
        'Items', likertOptionsAro, 'AlignForStimuli', 0);
    
    %% TRIAL 9 %%
    trial9 = coscreatetrial(ex);
    cosaddtextblock(trial9, 'Text', '{{n}}{{n}}{{center|{{style|font-size: 16px;|Now we are ready to start the first part of the experiment}}}}{{n}}');
    
    %% TRIAL 10-xx %%
    %% This is the bias experiment
    
    %     likertOptionsConf = {...
    %         '{{b|Very{{n}}unsure}}';...
    %         '{{b|Unsure}}';...
    %         '{{b|Somewhat{{n}}unsure}}';...
    %         '{{b|Neither sure{{n}}nor unsure}}';...
    %         '{{b|Somewhat{{n}}sure}}';...
    %         '{{b|Sure}}';...
    %         '{{b|Very{{n}}sure}}';...
    %         };
    
    %     likertOptionsConf = {...
    %         '{{b|Very{{n}}unsure}}';...
    %         '{{b|Unsure}}';...
    %         '{{b|Somewhat{{n}}unsure}}';...
    %         '{{b|Neither sure{{n}}nor unsure}}';...
    %         '{{b|Somewhat{{n}}sure}}';...
    %         '{{b|Sure}}';...
    %         '{{b|Very{{n}}sure}}';...
    %         };
    
    likertOptionsConf = {...
        '1{{n}}{{b|very unsure}}';...
        '2';...
        '3{{n}}{{b|unsure}}';...
        '4';...
        '5{{n}}{{b|neither sure{{n}}nor unsure}}';...
        '6';...
        '7{{n}}{{b|sure}}';...
        '8';...
        '9{{n}}{{b|very sure}}';...
        };
    
    %     for iExcerpt = 1:length(experiment_list_bias{iExp})
    for iExcerpt = 1
        for iAV = randperm(2)
            trialxx = coscreatetrial(ex);
            cosaddtextblock(trialxx, 'Text', '{{n}}{{n}}{{center|{{style|font-size: 16px;|Start by listening to the audio excerpt, then rate the excerpt and evaluate how sure you are of your rating.}}}}{{n}}');

            k=experiment_list_bias{iExp}(iExcerpt);
            switch iAV
                case 1
                    cosaddlikertscale(trialxx, 'HeaderLabel', 'Rate the excerpt on the {{n}} Arousal scale',...
                        'Items', likertOptionsAro, 'AlignForStimuli', 1,...
                        'Stimulus', {excerptList{k,1}, 'Music excerpt'});
                    
                    cosaddlikertscale(trialxx, 'HeaderLabel', 'How confident are you of your rating?',...
                        'Items', likertOptionsConf, 'AlignForStimuli', 1);

                    %                     E.Trial{1,iTrial} = combineExperiment({@header,'{{tab}}',NaN},{@textBlock,textstr,NaN},{@likertScale_r002,HeaderLabel(1),likertOptionsAro,excerptList(k,1),1,1,NaN},{@likertScale_r002,HeaderLabel(2),likertOptionsConf,{''},1,1,'_1'},{'1'});
                    
                case 2
                    cosaddlikertscale(trialxx, 'HeaderLabel', 'Rate the excerpt on the {{n}} Valence scale',...
                        'Items', likertOptionsVal, 'AlignForStimuli', 1,...
                        'Stimulus', {excerptList{k,1}, 'Music excerpt'});
                    
                    cosaddlikertscale(trialxx, 'HeaderLabel', 'How confident are you of your rating?',...
                        'Items', likertOptionsConf, 'AlignForStimuli', 1);
                    
                    %                     HeaderLabel = [{'Rate the excerpt on the {{n}} Valence scale'};{'How confident are you of your rating?'}];
                    %                     E.Trial{1,iTrial} = combineExperiment({@header,'{{tab}}',NaN},{@textBlock,textstr,NaN},{@likertScale_r002,HeaderLabel(1),likertOptionsVal,excerptList(k,1),1,1,NaN},{@likertScale_r002,HeaderLabel(2),likertOptionsConf,{''},1,1,'_1'},{'1'});
            end
        end
    end
    
    
    
    %% PART 2
    trial10 = coscreatetrial(ex);
    cosaddheader(trial10, 'HeaderLabel', '{{center|End of part 1}}');
    
    cosaddtextblock(trial10, 'Text', ['{{n}}{{n}}{{center|{{style|font-size: 16px;|That was the end of the first part.}}}}',...
        '{{n}}{{n}}{{center|{{style|font-size: 16px;|If you need to take a break, this would be the time, else please continue.}}}}']);
    
    %% STOMP experiment
    % trial11 = STOMP_experiment_sdk(ex);
    
    %%
    likertOptionsVal = {...
        ['{{image|' imageurl_val{1} '|124|133|center}}'];...
        '';...
        ['{{image|' imageurl_val{2} '|124|133|center}}'];...
        '';...
        ['{{image|' imageurl_val{3} '|124|133|center}}'];...
        '';...
        ['{{image|' imageurl_val{4} '|124|133|center}}'];...
        '';...
        ['{{image|' imageurl_val{5} '|124|133|center}}'];...
        };
    
    % Arousal
    
    likertOptionsAro = {...
        ['{{image|' imageurl_aro{1} '|124|133|center}}'];...
        '';...
        ['{{image|' imageurl_aro{2} '|124|133|center}}'];...
        '';...
        ['{{image|' imageurl_aro{3} '|124|133|center}}'];...
        '';...
        ['{{image|' imageurl_aro{4} '|124|133|center}}'];...
        '';...
        ['{{image|' imageurl_aro{5} '|124|133|center}}'];...
        };
    
    
    
    trial12 = coscreatetrial(ex);
    cosaddheader(trial12, 'HeaderLabel', '{{center|Scales}}');
    cosaddtextblock(trial12, 'Text', '{{n}}{{n}}{{center|{{style|font-size: 16px;|These are the scales you will use in the second part of the experiment to rate each of the musical excerpts.}}}}');
    cosaddlikertscale(trial12, 'HeaderLabel', likertQval, 'AlignForStimuli', 0,...
        'Items', likertOptionsVal);
    cosaddlikertscale(trial12, 'HeaderLabel', likertQaro, 'AlignForStimuli', 0,...
        'Items', likertOptionsAro);
    
    %%
    for iExcerpt = 1
        for iAV = randperm(2)
            trialxx = coscreatetrial(ex);
            cosaddtextblock(trialxx, 'Text', '{{n}}{{n}}{{center|{{style|font-size: 16px;|Start by listening to the audio excerpt, then rate the excerpt and evaluate how sure you are of your rating.}}}}{{n}}');

%             k=explist_confidence_50(iExp,iExcerpt);
              k=randi(10,1);
              
            % removed stimulus here because the link
            % https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a5896b0c/system/140_item/resources/set3/001.mp3
            % gives an error
            

            switch iAV
                case 1
                    cosaddlikertscale(trialxx, 'HeaderLabel', 'Rate the excerpt on the {{n}} Arousal scale',...
                        'Items', likertOptionsAro, 'AlignForStimuli', 1);%,...
                        %'Stimulus', {excerptListSet3{k,1}, 'Music excerpt'});
        
                    cosaddlikertscale(trialxx, 'HeaderLabel', 'How confident are you of your rating?',...
                        'Items', likertOptionsConf, 'AlignForStimuli', 1);
                    
                    %E.Trial{1,iTrial} = combineExperiment({@header,'{{tab}}',NaN},{@textBlock,textstr,NaN},{@likertScale_r002,HeaderLabel(1),likertOptionsAro,excerptList(k,1),1,1,NaN},{@likertScale_r002,HeaderLabel(2),likertOptionsConf,{''},1,1,'_1'},{'1'});
                    
                case 2
                    cosaddlikertscale(trialxx, 'HeaderLabel', 'Rate the excerpt on the {{n}} Valence scale',...
                        'Items', likertOptionsVal, 'AlignForStimuli', 1);%,...
                        %'Stimulus', {excerptListSet3{k,1}, 'Music excerpt'});
                    
                    cosaddlikertscale(trialxx, 'HeaderLabel', 'How confident are you of your rating?',...
                        'Items', likertOptionsConf, 'AlignForStimuli', 1);
                    
                    HeaderLabel = [{'Rate the excerpt on the {{n}} Valence scale'};{'How confident are you of your rating?'}];
                    %E.Trial{1,iTrial} = combineExperiment({@header,'{{tab}}',NaN},{@textBlock,textstr,NaN},{@likertScale_r002,HeaderLabel(1),likertOptionsVal,excerptList(k,1),1,1,NaN},{@likertScale_r002,HeaderLabel(2),likertOptionsConf,{''},1,1,'_1'},{'1'});
            end
        end
    end
    
    
    
    %% End experiment with experiment method:
    ex.endExperiment;
    
    %% Verify experiment by XML representation:
    ex.getXMLRepresentation()
    
    %get status of experiment
    ex.getStateReport()
    
end

cosuploadexperiments(apikey, 'env', 'dev', ex);
% end

%% Retrieving experiment after user has reponded
% User response is not registered until after another reindexation
% chaos.reindexDatabase();

bla = 1;