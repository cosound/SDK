% Absolute or relative path to jar-file
clear all, clc;
BINPATH = '../bin/production';
javarmpath(BINPATH);
javaaddpath(BINPATH);
addpath('../matlab_bridge');
prodkey = fileread('prodkey');
debugkey = fileread('debugkey');

exid = 'e3cc09b6-f3ee-af3c-0001-000000000032';

xmlfilename = ['../xml_examples/' exid '.xml'];
for i=1:100
    fprintf('%d/%d\n', i, 100);
    ex = com.Utilities.ChaosSDK.loadExperimentFromFile(xmlfilename);
    answers = getAnswers(ex);
    t19 = answers{19};
    if ~strcmp(t19(1).Activity(1).Type, 'Start')
        error('error')
    end
end

%% EXPERIMENT %%
ex = com.MetaComponents.Experiment;
ex.name = 'STOMP:Test';
ex.id = 'a9f56a58-aaaa-eeee-1351-aa435ed70001';

ex.lockQuestion = false;
ex.enablePrevious = true;
ex.createdBy = 'Anders Kirk Uhrenholt';
ex.experimentDescription = 'This experiment tests fucktionalities of the Cockpit SDK. In this case the implementation of the STOMP experiment.';
ex.footerLabel = 'My Footer Label';
ex.redirectOnCloseUrl = 'http://redirection.url';

URI = ['https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a589'...
       '6b0c/system/140_item/resources/set1/006.mp3'];

% com.Components should be added through trial objects, i.e.
%% TRIAL 1 %%
% Non-interactable components. Just needs a string for initialization.
trial1 = ex.createTrial();

kacps = trial1.addKACPS('K-AC-PS label');
for i = 1:3
    if i==2
        selStr = ' (selected)';
    else
        selStr = '';
    end
    buttonLabel = sprintf('with stimulus %d%s', i, selStr);
    stimLabel = sprintf('stim%d', i);
    kacps.addButton(buttonLabel, i==2, URI, stimLabel);
end

for i = 1:3
    if i==2
        selStr = ' (selected)';
    else
        selStr = '';
    end
    buttonLabel = sprintf('with stimulus %d%s', i, selStr);
    kacps.addButton(buttonLabel, i==2);
end

kacps = trial1.addKACPS('K-AC-PS label');
for i = 1:3
    if i==2
        selStr = ' (selected)';
    else
        selStr = '';
    end
    buttonLabel = sprintf('with stimulus %d%s', i, selStr);
    kacps.addButton(buttonLabel, i==2);
end

ods = trial1.addOneDScale('Just header label');
ods.getTextRepresentation()

ods = trial1.addOneDScale('Label and alignment', 1);
ods.getTextRepresentation()

ods = trial1.addOneDScale('Label, alignment, and position', 1, 0.5);
ods.getTextRepresentation()

ods = trial1.addOneDScale('Label, alignment, position, and stimulus', 1,...
    -0.5, {URI, 'com.Components.Stimulus label'});
ods.getTextRepresentation()

ods = trial1.addOneDScale('Label, alignment, position, stimulus, and labels',...
    1, -0.5, {URI, 'com.Components.Stimulus label'}, 'X1 label',...
    'X2 label', 'Y1 label', 'Y2 label');
ods.getTextRepresentation()

x1AxisTicks = {'x1tick1', -1, 'x1tick2', 0, 'x1tick3', 1};
x2AxisTicks = {'x2tick1', -1, 'x2tick2', 0, 'x2tick3', 1};
y1AxisTicks = {'y1tick1', -1, 'y1tick2', 0, 'y1tick3', 1};
y2AxisTicks = {'y2tick1', -1, 'y2tick2', 0, 'y2tick3', 1};
ods = trial1.addOneDScale(['Label, alignment, position, stimulus, labels'...
    ', and ticks'], 1, -0.5, {URI, 'com.Components.Stimulus label'},...
    'X1 label', 'X2 label', 'Y1 label', 'Y2 label', x1AxisTicks,...
    x2AxisTicks, y1AxisTicks, y2AxisTicks);
ods.getTextRepresentation()

%% End experiment with experiment method:
ex.endExperiment;

%% Verify experiment by ASCII representation:
ex.getTextRepresentation

%% Verify experiment by XML representation:
ex.getXMLRepresentation()

%get status of experiment
ex.getStateReport()

%get revision ID
ex.revisionID

%get experiment ID
ex.id


%% commit stuff
chaos = com.Utilities.ChaosSDK(debugkey);

try
    chaos.deleteObject(ex.id);
catch
   fprintf('Cannot delete\n');
end

% Commit experiment to Cockpit server. If the experiment is not in a valid
% state (fx if no trials have been added), and exception will be thrown. A
% detailed account of errors to be fixed can be obtained by
% >> ex.getStateReport()
% Note that if no ID has been set, a random will be created
for i=1:10
    try
        chaos.reindexDatabase();
        chaos.commitExperiment(ex, 'debug');
        break
    catch
        fprintf('Retrying %i/10...\n', i);
    end
end

if i == 10
    throw(MException('SDK:ConnectionError','Could not commit'));
end

%% Retrieving experiment after user has reponded
% User response is not registered until after another reindexation
chaos.reindexDatabase();

% Answers are wrapped in a new experiment object
finishedEx = chaos.getObject(ex.id);
finishedEx.getTextRepresentation()
