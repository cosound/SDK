clc
clear all
addpath('../matlab_bridge')
BINPATH = '../bin/production';
javarmpath(BINPATH);
javaaddpath(BINPATH);

apikey = fileread('../matlab_examples/devkey');
URI = 'https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a5896b0c/system/140_item/resources/set1/100.mp3';

%% EXPERIMENT %%
exid = sprintf('e3cc09b6-f3ee-af3c-0001-223c3ae12345');
exname = 'Tagging test';
exdesc = 'Description of the Tagging test.';
exfooter = 'Footer for the Tagging test';
ex = cosexperiment('name', exname, 'id', exid, 'LockQuestion', 0,...
    'EnablePrevious', 1, 'CreatedBy', 'Anders Kirk Uhrenholt',...
    'ExperimentDescription', exdesc, 'FooterLabel', exfooter);

%% TRIAL 1 %%
trial1 = coscreatetrial(ex);

cosaddtagginga(trial1, 'HeaderLabel', 'TaggingA Header Label',...
    'SelectionTagBoxLabel', 'Selection tags', 'UserTagBoxLabel', 'User tags',...
    'TextField', 'Text field text', 'SelectionTags', {'ST1', 'ST2', 'ST3'},...
    'UserTags', {'UT1', 'UT2', 'UT3'}, 'Stimulus', {URI, 'My Stimulus'});

cosaddtaggingb(trial1, 'HeaderLabel', 'TaggingB Header Label',...
    'SelectionTagBoxLabel', 'Selection tags', 'UserTagBoxLabel', 'User tags',...
    'TextField', 'Text field text', 'SelectionTags', {'ST1', 'ST2', 'ST3'},...
    'UserTags', {'UT1', 'UT2', 'UT3'}, 'Stimulus', {URI, 'My Stimulus'});

cosuploadexperiments(apikey, 'env', 'dev', ex);
