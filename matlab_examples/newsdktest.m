clear all, clc;
BINPATH = '../bin/production';
javarmpath(BINPATH);
javaaddpath(BINPATH);
addpath('../matlab_bridge');

experiment = cosexperiment;
experiment.name = 'SDK revision test';
trial = coscreatetrial(experiment);
cosaddlikertscale(trial, 'HeaderLabel', 'Likert scale header label',...
    'items', {'1','2','3','4','5'}, 'alignforstimuli', 0);

debugkey = fileread('debugkey');
cosuploadexperiments(debugkey, 'env', 'dev', experiment);
