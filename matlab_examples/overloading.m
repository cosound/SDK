BINURL = '../bin/production';
javarmpath(BINURL);
javaaddpath(BINURL);


stimurl = ['https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b'...
           '7c8a5896b0c/system/140_item/resources/set1/006.mp3'];
ex = com.MetaComponents.Experiment();
t = ex.createTrial();

%% Radio buttons %%
t.addRadioButtonGroup('HeaderLabel', {'low','','medium','','high'});
% -> alignForStimuli = 0
%    no stimulus

t.addRadioButtonGroup('HeaderLabel', {'low','','medium','','high'},...
    0);
% -> alignForStimuli = 1
%    no stimulus

t.addRadioButtonGroup('HeaderLabel', {'low','','medium','','high'},...
    0, {stimurl, 'StimulusLabel'});
% -> alignForStimuli = 0
%    stimulus url = stimurl
%    stimulus label = stimulus label


%% Likert scales (same as for radio buttons %%
t.addLikertScale('HeaderLabel', {'low', '', 'medium', '', 'high'});
t.addLikertScale('HeaderLabel', {'low', '', 'medium', '', 'high'},...
	0);
t.addLikertScale('HeaderLabel', {'low', '', 'medium', '', 'high'},...
    0, {stimurl, 'com.Components.Stimulus Label'});

%% Checkboxes %%
t.addCheckBoxGroup('HeaderLabel', {'low', '', 'medium', '', 'high'});
% -> minNoOfSelections = 1
%    maxNoOfSelections = 5
%    alignForStimuli = 0
%    no stimulus

t.addCheckBoxGroup('HeaderLabel', {'low', '', 'medium', '', 'high'}, 2);
% -> minNoOfSelections = 2
%    maxNoOfSelections = 5
%    alignForStimuli = 0
%    no stimulus

t.addCheckBoxGroup('HeaderLabel', {'low', '', 'medium', '', 'high'}, 2, 4);
% -> minNoOfSelections = 2
%    maxNoOfSelections = 4
%    alignForStimuli = 0
%    no stimulus

t.addCheckBoxGroup('HeaderLabel', {'low', '', 'medium', '', 'high'}, 2,...
    4, 1);
% -> minNoOfSelections = 2
%    maxNoOfSelections = 4
%    alignForStimuli = 1
%    no stimulus

t.addCheckBoxGroup('HeaderLabel', {'low', '', 'medium', '', 'high'}, 2,...
    4, 1, {stimurl, 'com.Components.Stimulus Label'});
% -> minNoOfSelections = 2
%    maxNoOfSelections = 4
%    alignForStimuli = 0
%    stimulus url = stimurl
%    stimulus label = stimulus label

%% List select (same as for checkboxes %%
t.addListSelect('HeaderLabel', {'low','','medium','','high'});
t.addListSelect('HeaderLabel', {'low','','medium','','high'}, 3);
t.addListSelect('HeaderLabel', {'low','','medium','','high'}, 3, 3);
t.addListSelect('HeaderLabel', {'low','','medium','','high'}, 3, 3, 0);
t.addListSelect('HeaderLabel', {'low','','medium','','high'}, 3 3, 0,...
    {stimurl, 'com.Components.Stimulus Label'});

