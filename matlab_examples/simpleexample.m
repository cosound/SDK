clc
clear all
addpath('../matlab_bridge')
BINPATH = '../bin/production';
javarmpath(BINPATH);
javaaddpath(BINPATH);

%% EXPERIMENT %%
exid = sprintf('a9f56a58-aaaa-eeee-1355-012345678912');
exname = 'SimpleExample';
exdesc = 'Description of the simple example.';
exfooter = 'Footer for the simple example';
ex = cosexperiment('name', exname, 'id', exid, 'LockQuestion', 0,...
    'EnablePrevious', 1, 'CreatedBy', 'Anders Kirk Uhrenholt',...
    'ExperimentDescription', exdesc, 'FooterLabel', exfooter);


%% TRIAL 1 %%
trial1 = coscreatetrial(ex);
cosaddheader(trial1, 'HeaderLabel', 'Trial 1 header');
cosaddfreetext(trial1, 'Label', 'Only digits here', 'LabelPosition',...
    'top', 'Validation', '^[0-9]+$');

%% TRIAL 2 %%
trial2 = coscreatetrial(ex);
cosaddheader(trial2, 'HeaderLabel', '{{center|Scales}}');

% Valence
likertQval = {'{{center|{{style|font-size: 25px;|Valence scale}}}}'};
%
likertOptionsVal = {...
    '1{{n}}{{b|unpleasant}}{{n}}{{b|bad}}{{n}}{{b|negative}}';...
    '2';...
    '3';...
    '4';...
    '5{{n}}{{b|neutral}}';...
    '6';...
    '7';...
    '8';...
    '9{{n}}{{b|pleasant}}{{n}}{{b|good}}{{n}}{{b|positive}}';...
    };

cosaddlikertscale(trial2, 'HeaderLabel', likertQval,...
    'items', likertOptionsVal, 'AlignForStimuli', 0);


%% TRIAL 3 %%
trial3 = coscreatetrial(ex);
cosaddheader(trial3, 'HeaderLabel', 'Thank you for your participation');

%% End experiment with experiment method:
ex.endExperiment;

%% Write XML to file
xml = ex.getXMLRepresentation();

fid = fopen('../xml_examples/simpleexample.xml', 'w');
fprintf(fid, '%s', char(xml));
fclose(fid);
