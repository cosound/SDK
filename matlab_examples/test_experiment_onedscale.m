clc
clear all
addpath('../matlab_bridge')
BINPATH = '../bin/production';
javarmpath(BINPATH);
javaaddpath(BINPATH);

apikey = fileread('devkey');
URI = 'https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a5896b0c/system/140_item/resources/set1/100.mp3';

%% EXPERIMENT %%
exid = sprintf('a9f56a58-aaaa-eeee-1355-012345678905');
exname = 'OneDScale test';
exdesc = 'Description of the OneDScale test.';
exfooter = 'Footer for the OneDScale test';
ex = cosexperiment('name', exname, 'id', exid, 'LockQuestion', 0, 'EnablePrevious', 1, 'CreatedBy', 'Jens Madsen', 'ExperimentDescription', exdesc, 'FooterLabel', exfooter);

%% TRIAL 1 %%
trial1 = coscreatetrial(ex);

cosaddheader(trial1, 'HeaderLabel', '{{center|OneDScale test}}');

x1ticks = {{'First x1 tick', -0.5}, {'Second x1 tick', 0.0}, {'Third x1 tick', 0.5}};
x2ticks = {{'First x2 tick', -1}, {'Second x2 tick', 0.0}, {'Third x2 tick', 1}};

cosaddonedscale(trial1, 'HeaderLabel', 'OneDScale Header Label no position', 'X1Ticks',...
    x1ticks', 'X2Ticks', x2ticks', 'Y1Ticks', {{'Only y1 tick', 0.5}},...
    'Y2Ticks', {{'Only y2 tick', -0.5}}, 'AlignForStimuli', 0, ...
    'X1AxisLabel', 'Label for X1 axis', 'X2AxisLabel', 'Label for X2 axis', ...
    'Y1AxisLabel', 'Label for Y1 axis', 'Y2AxisLabel', 'Label for Y2 axis');

cosaddonedscale(trial1, 'HeaderLabel', 'OneDScale Header Label (position=0.1)', 'X1Ticks',...
    x1ticks', 'X2Ticks', x2ticks', 'Y1Ticks', {{'Only y1 tick', 0.5}},...
    'Y2Ticks', {{'Only y2 tick', -0.5}}, 'AlignForStimuli', 0, 'Position', 0.9,...
    'X1AxisLabel', 'Label for X1 axis', 'X2AxisLabel', 'Label for X2 axis', ...
    'Y1AxisLabel', 'Label for Y1 axis', 'Y2AxisLabel', 'Label for Y2 axis');


cosaddonedscale(trial1, 'HeaderLabel', 'OneDScale Header Label (position=0.9)', 'X1Ticks',...
    x1ticks', 'X2Ticks', x2ticks', 'Y1Ticks', {{'Only y1 tick', 0.5}},...
    'Y2Ticks', {{'Only y2 tick', -0.5}}, 'AlignForStimuli', 0, 'Position', 0.9,...
    'X1AxisLabel', 'Label for X1 axis', 'X2AxisLabel', 'Label for X2 axis', ...
    'Y1AxisLabel', 'Label for Y1 axis', 'Y2AxisLabel', 'Label for Y2 axis');


%% TRIAL 2 %%
trial2 = coscreatetrial(ex);

x1ticks = {{'First x1 tick', -0.5}, {'Second x1 tick', 0.0}, {'Third x1 tick', 0.5}};
x2ticks = {{'First x2 tick', -1}, {'Second x2 tick', 0.0}, {'Third x2 tick', 1}};

cosaddonedscale(trial2, 'HeaderLabel', 'No position (alignForStimuli=0)', 'X1Ticks',...
    x1ticks', 'X2Ticks', x2ticks', 'Y1Ticks', {{'Only y1 tick', 0.5}},...
    'Y2Ticks', {{'Only y2 tick', -0.5}}, 'AlignForStimuli', 0, ...
    'X1AxisLabel', 'Label for X1 axis', 'X2AxisLabel', 'Label for X2 axis', ...
    'Y1AxisLabel', 'Label for Y1 axis', 'Y2AxisLabel', 'Label for Y2 axis');


cosaddonedscale(trial2, 'HeaderLabel', 'No position (alignForStimuli=1)', 'X1Ticks',...
    x1ticks', 'X2Ticks', x2ticks', 'Y1Ticks', {{'Only y1 tick', 0.5}},...
    'Y2Ticks', {{'Only y2 tick', -0.5}}, 'AlignForStimuli', 1, ...
    'X1AxisLabel', 'Label for X1 axis', 'X2AxisLabel', 'Label for X2 axis', ...
    'Y1AxisLabel', 'Label for Y1 axis', 'Y2AxisLabel', 'Label for Y2 axis');

cosaddonedscale(trial2, 'HeaderLabel', 'No position with stimuli', 'X1Ticks',...
    x1ticks', 'X2Ticks', x2ticks', 'Y1Ticks', {{'Only y1 tick', 0.5}},...
    'Y2Ticks', {{'Only y2 tick', -0.5}}, 'AlignForStimuli', 0, ...
    'X1AxisLabel', 'Label for X1 axis', 'X2AxisLabel', 'Label for X2 axis', ...
    'Y1AxisLabel', 'Label for Y1 axis', 'Y2AxisLabel', 'Label for Y2 axis',...
    'Stimulus', {URI, 'This is my stimuli Label'});

%% TRIAL 3 %%
trial3 = coscreatetrial(ex);
cosaddheader(trial3, 'HeaderLabel', 'Thank you for your participation');

%% End experiment with experiment method:
ex.endExperiment;

%% Write XML to file
xml = ex.getXMLRepresentation();

fid = fopen('../xml_examples/onedscaletest.xml', 'w+');
fprintf(fid, '%s', char(xml));
fclose(fid);

cosuploadexperiments(apikey, 'env', 'dev', ex);
