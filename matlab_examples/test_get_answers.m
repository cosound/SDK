clc
clear all
addpath('../matlab_bridge')
BINPATH = '../bin/production';
javarmpath(BINPATH);
javaaddpath(BINPATH);

apikey = fileread('prodkey');

% cosreindex(apikey, 'Env', 'prod');

% Answers are contained in a nested cell array. Outer array has one inner
% array for each trial. The inner arrays have a struct for each component
% in that trial.
allanswers = cosgetanswers(apikey, 'a9f56a58-aaaa-eeee-1355-aa435ed70050', 'env','prod');

% Iterate outer array
for k = 1:length(allanswers)
    trialanswers = allanswers{k};
    fprintf('---- Trial %d/%d\n ----', k, length(allanswers));
    
    % Print all answers and events for this trial
    for i = 1:length(trialanswers)
        fprintf('-- Component %d/%d --\n', i, length(trialanswers));
        componentanswer = trialanswers{i};
        fprintf('Component type: %s\n', componentanswer.Type);
        fprintf('Header label:   %s\n', componentanswer.HeaderLabel);
        fprintf('Stimulus Label: %s\n', componentanswer.StimulusLabel);
        fprintf('Stimulus URI:   %s\n', componentanswer.StimulusURI);
        fprintf('User response:  %s\n', componentanswer.Response);
        
        % Print events for this component
        fprintf('Events:\n');
        for j = 1:length(componentanswer.Events)
            event = componentanswer.Events(j);
            fprintf('Id: %s\n', event.Id);
            fprintf('Type: %s\n', event.Type);
            fprintf('Method: %s\n', event.Method);
            fprintf('Data: %s\n', event.Data);
            fprintf('DateTime: %s\n\n', event.DateTime);
        end
    end
    fprintf('\n\n\n');
end