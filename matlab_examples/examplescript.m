% Absolute or relative path to jar-file
% JARPATH = 'sdk.jar';
% javarmpath(JARPATH)
% javaaddpath(JARPATH)

BINPATH = '../bin';
javarmpath(BINPATH);
javaaddpath(BINPATH);

%% EXPERIMENT %%
ex = com.MetaComponents.Experiment;
ex.name = 'DTU:Testcase:006';
ex.id = 'a9f56a58-aaaa-eeee-6656-fa435ed72a41';

ex.lockQuestion = false;
ex.enablePrevious = true;
ex.createdBy = 'Anders Kirk Uhrenholt';
ex.experimentDescription = 'My experiment.';


% com.Components should be added through trial objects, i.e.
% >> trialObject.addHeader('com.Components.Header text');

%% TRIAL 1 %%
% Non-interactable components. Just needs a string for initialization.
trial1 = ex.createTrial();
trial1.addHeader('{{center|com.Components.Header text}}');
trial1.addTextBlock('Text block content.');

%% TRIAL 2 %%
trial2 = ex.createTrial();

% addCheckBoxGroup(minNoOfSelections, maxNoOfSelections,
%                  headerLabel, {itemLabels}) 
cbg = trial2.addCheckBoxGroup(2, 4, 'Checkbox Label',...
    {'Option 1', 'Option 2', 'Option 3', 'Option 4', 'Option 5'});

% For all components supporting stimuli, this can be optionally set
% after initialization with either
% >> component.setStimulus(URI, stimulusLabel)
% or
% >> component.setStimulus(URI, stimulusLabel, mediaType)
% where mediaType of type String is either 'audio' or 'video'. If not
% provided the media type will be inferred from filetype in URI.
URI = ['https://s3-eu-west-1.amazonaws.com/ac0926766ce9afd07d63b7c8a589'...
    '6b0c/system/140_item/resources/set1/006.mp3'];
cbg.setStimulus(URI, 'Audio to be played');

% addLikertScale(headerLabel, {itemLabels})
trial2.addLikertScale('Likert scale label',...
    {'Least', 'Less', 'Default', 'More', 'Most'});

% addListSelect(minNoOfSelections, maxNoOfSelections,
%               headerLabel, {itemLabels}) 
trial2.addListSelect(0, 5, 'List select label',...
    {'Option 1', 'Option 2', 'Option 3', 'Option 4', 'Option 5'});
% (ACCEPTED BY CHAOS SERVER BUT NOT ACTUALLY SHOWN)

% addRadioButtonGroup(headerLabel, {itemLabels}) 
trial2.addRadioButtonGroup('Radiobutton Group label',...
    {'Least', 'Less', 'Default', 'More', 'Most'});


%% TRIAL 3 %%
trial3 = ex.createTrial();
% addFreeText(headerLabel, position (string), boxWidth (int),
%             boxHeight (int), resizable (boolean), validation (Regex string))
trial3.addFreeText('Occupation', 'top', 100, 100, 1, '.*');

% addFreeTextHash(headerLabel, position (string),
%   validation (Regex string), forceLowerCase (boolean))
% trial3.addFreeTextHash('Password', 'top', '.*', 0);
% (NOT IMPLEMENTED ON CHAOS SERVER)

%% End experiment with experiment method:
ex.endExperiment();

%% Verify experiment by ASCII representation:
ex.getTextRepresentation()

%% Committing experiment to server
% Login by instantiating com.Utilities.ChaosSDK-object with your API-key:
chaos = com.Utilities.ChaosSDK(['a4dfcd16da84ab06ccbe6e0786fd67a9aeae0f0ab432fcee3d50'...
                  '2c9c97e08062']);

% Delete experiment. An exception will be thrown if experiment does not
% exist, so wrap in try-clause
try chaos.deleteObject(ex.id);
catch fprintf('com.MetaComponents.Experiment could not be deleted.\n');
end;

% Deletion must be followed by re-indexation before a new experiment with
% same ID can be uploaded.
chaos.reindexDatabase();

% Commit experiment to Cockpit server. If the experiment is not in a valid
% state (fx if no trials have been added), and exception will be thrown. A
% detailed account of errors to be fixed can be obtained by
% >> ex.getStateReport()
% Note that if no ID has been set, a random will be created
chaos.commitExperiment(ex);

%% Retrieving experiment after user has reponded
% User response is not registered until after another reindexation
chaos.reindexDatabase();

% Answers are wrapped in a new experiment object
finishedEx = chaos.getObject(ex.id);
finishedEx.getTextRepresentation()


%% com.MetaComponents.Experiment list
% com.MetaComponents.Experiment lists are represented as objects as well. They can either be
% instantiated with an array of experiments, i.e.
% >> list = com.MetaComponents.ExperimentList([exp1, exp2, ...])
% or experiments can be added subsequently, i.e.
% >> list = com.MetaComponents.ExperimentList;
% >> list.addExperiment(ex1);
% >> list.addExperiment(ex2, 10);
% In the latter example 10 copies are made of the provided experiment with
% the ID incremented for each experiment. That is, if the original
% experiment has an ID ending on 6789, the experiments following it in the
% list will have ID's ending on 678a, 678b, etc.
list = com.MetaComponents.ExperimentList;
list.id = '536c0444-33b7-07c9-f2ed-530685679a17';
list.addExperiment(ex, 5);
list.getTextRepresentation

% When committing a list to the server, the SDK will try to commit each
% experiment in the list separately. If an experiment already exists on the
% server with an ID identical to that of an experiment on the list, the
% experiment on the server will NOT be overriden.
try chaos.deleteObject(list.id);
catch fprintf('com.MetaComponents.Experiment list could not be deleted.\n');
end;
list = chaos.commitExperimentList(list);
