package com.CockpitExceptions;

/**
 * Created by kirksmacbookair on 30/08/2016.
 */
public abstract class CockpitException extends Exception {
    public CockpitException(String s) {
        super(s);
    }
}
