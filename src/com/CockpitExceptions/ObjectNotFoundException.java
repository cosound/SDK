package com.CockpitExceptions;

/**
 * Created by kirksmacbookair on 30/08/2016.
 */
public class ObjectNotFoundException extends CockpitException {
    public ObjectNotFoundException(String s) {
        super(s);
    }
}
