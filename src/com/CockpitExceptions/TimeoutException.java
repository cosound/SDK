package com.CockpitExceptions;

/**
 * Created by kirksmacbookair on 30/08/2016.
 */
public class TimeoutException extends CockpitException {
    public TimeoutException(String s) {
        super(s);
    }
}
