package com.CockpitExceptions;

/**
 * Created by kirksmacbookair on 30/08/2016.
 */
public class ConnectionError extends CockpitException {
    public ConnectionError(String s) {
        super(s);
    }
}
