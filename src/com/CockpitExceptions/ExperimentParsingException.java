package com.CockpitExceptions;

/**
 * Created by kirksmacbookair on 02/09/2016.
 */
public class ExperimentParsingException extends CockpitException {
    public ExperimentParsingException(String s) {
        super(s);
    }
}
