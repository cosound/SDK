package com.CockpitExceptions;

/**
 * Created by kirksmacbookair on 30/08/2016.
 */
public class ExperimentAlreadyExistsException extends CockpitException {
    public ExperimentAlreadyExistsException(String s) {
        super(s);
    }
}
