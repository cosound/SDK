package com.CockpitExceptions;

/**
 * Created by kirksmacbookair on 30/08/2016.
 */
public class InvalidExperimentException extends CockpitException {
    public InvalidExperimentException(String s) {
        super(s);
    }
}
