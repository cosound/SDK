package com.CockpitExceptions;

/**
 * Created by kirksmacbookair on 08/09/2016.
 */
public class CommitNotAcceptedException extends CockpitException {
    public CommitNotAcceptedException(String s) {
        super(s);
    }
}
