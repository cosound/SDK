package com.CockpitExceptions;

/**
 * Created by kirksmacbookair on 30/08/2016.
 */
public class OperationNotPermittedException extends CockpitException {
    public OperationNotPermittedException(String s) {
        super(s);
    }
}
