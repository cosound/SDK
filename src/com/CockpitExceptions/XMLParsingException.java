package com.CockpitExceptions;

/**
 * Created by kirksmacbookair on 02/09/2016.
 */
public class XMLParsingException extends CockpitException {
    public XMLParsingException(String s) {
        super(s);
    }
}
