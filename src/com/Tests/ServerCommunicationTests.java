package com.Tests;

import com.CockpitExceptions.CockpitException;
import com.CockpitExceptions.ExperimentParsingException;
import com.CockpitExceptions.XMLParsingException;
import com.MetaComponents.Experiment;
import com.Utilities.ChaosSDK;
import com.Utilities.XMLParser;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Node;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by kirksmacbookair on 02/02/2017.
 */
public class ServerCommunicationTests {
    private static final String xmlDirectoryPath = "xml_examples";
    private static List<File> xmlExperimentFiles = new LinkedList<>();
    private static List<File> xmlExperimentListFiles = new LinkedList<>();
    private static final String charset = java.nio.charset.StandardCharsets.UTF_8.name();
    private static final boolean devEnv = false;

    @BeforeClass
    public static void classSetup() throws IOException {
        File dir = new File(xmlDirectoryPath);
        File[] filesList = dir.listFiles();
        if (filesList == null) {
            throw new IOException("Could not get directory list.");
        }

        for (File file : filesList) {
            file.setReadOnly();
            String filename = file.getName();

            if (!filename.substring(filename.length()-3).equals("xml")) {
                continue;
            }

            byte[] bytes = Files.readAllBytes(file.toPath());
            String expXML = new String(bytes, charset);
            if (expXML.contains("<Experiments>")) {
                xmlExperimentListFiles.add(file);
            } else if (expXML.contains("<Experiment>")) {
                xmlExperimentFiles.add(file);
            } else {
                System.out.println("Could not determine type of file "+filename);
            }
        }
    }

    @Test
    public void testExperimentsCompareLocally() throws IOException {
        String tempDir = Files.createTempDirectory(Paths.get("."), "temp").toString();
        List<Path> tempExperimentPaths = new LinkedList<>();

        try {
            for (File experimentFile : xmlExperimentFiles) {
                Experiment experiment = auxParseExperimentFile(experimentFile);
                String xml = experiment.getXMLRepresentation();

                Path newPath = Paths.get(tempDir, experimentFile.getName());
                File newFile = new File(Files.write(newPath, xml.getBytes()).toUri());
                tempExperimentPaths.add(newPath);

                Experiment reReadExperiment = auxParseExperimentFile(newFile);
                assertEquals(experiment, reReadExperiment);
            }
        } finally {
            for (Path tempExperimentPath : tempExperimentPaths) {
                Files.deleteIfExists(tempExperimentPath);
            }
            Files.delete(Paths.get(tempDir));
        }
    }

    @Test
    public void testUploadAndRetrieveTest() {
        String currentExperimentID = "N/A";
        String randomID = null;
        ChaosSDK sdk = null;

        try {
            // Login
            String envString, key;
            if (devEnv) {
                envString = "dev";
                key = new String(Files.readAllBytes(Paths.get("matlab_examples/debugkey")));
            } else {
                envString = "prod";
                key = new String(Files.readAllBytes(Paths.get("matlab_examples/prodkey")));
            }
            sdk = new ChaosSDK(key, envString);

            // Iterate experiment files
            for (File experimentFile : xmlExperimentFiles) {
                // Try to upload and download file as is without parsing first. If it fails we
                // conclude that the server does not accommodate that XML-file (anymore) and
                // and continue test without error
                randomID = sdk.getRandomID();
                Experiment downloadedExperiment;
                try {
                    sdk.commitExperimentFromXMLFile(experimentFile.getAbsolutePath(), randomID);
                    downloadedExperiment = sdk.getExperiment(randomID);
                } catch (CockpitException | IOException e) {
                    System.out.println("Upload/download failed for raw experiment " +
                            experimentFile.getName());
                    System.out.println(e.getMessage());
                    continue;
                } finally {
                    try { sdk.deleteObject(randomID); } catch (Exception ignored) {}
                }

                // Experiment file was accepted so we compare to locally parsed experiment to see
                // if uploading and downloading has changed experiment unexpectedly

                // Error on CHAOS server means that '+'-signs gets converted to whitespace
                String downloadedXML = downloadedExperiment.getXMLRepresentation()
                        .replaceAll("%2B", " ")
                        .replaceAll("\\+", " ");
                String parsedXML = auxParseExperimentFile(experimentFile).getXMLRepresentation()
                        .replaceAll("\\+", " ")
                        .replaceAll("%2B", " ");

                assertEquals(downloadedXML, parsedXML);
                System.out.println("Succeeded for experiment " + experimentFile.getName());
            }
        } catch (IOException e) {
            System.out.println("IOException for experiment " + currentExperimentID);
            System.out.println(e.getMessage());
            e.printStackTrace();
            fail();
        } catch (CockpitException e) {
            System.out.println("CockpitException for experiment " + currentExperimentID);
            System.out.println(e.getMessage());
            e.printStackTrace();
            fail();
        } finally {
            try { sdk.deleteObject(randomID); } catch (Exception ignored) {}
        }
    }

    private Experiment auxParseExperimentFile(File file) throws IOException {
        byte[] bytes = Files.readAllBytes(file.toPath());
        String expXML = new String(bytes, charset);
        try {
            Node expNode = XMLParser.convertToNode(expXML);
            return XMLParser.parseXML(expNode);
        } catch (XMLParsingException | ExperimentParsingException e) {
            System.out.println("For file "+file.getName());
            System.out.println(e.getMessage());
            e.printStackTrace();
            fail();
            return new Experiment(); // make the compiler happy
        }
    }

}
