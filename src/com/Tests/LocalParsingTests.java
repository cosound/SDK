package com.Tests;

import com.CockpitExceptions.ExperimentParsingException;
import com.CockpitExceptions.XMLParsingException;
import com.MetaComponents.Experiment;
import com.Utilities.XMLParser;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Node;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by kirksmacbookair on 25/01/2017.
 */
public class LocalParsingTests {

    private static String xmlDirectoryPath = "xml_examples";
    private static List<File> xmlExperimentFiles = new LinkedList<>();
    private static List<File> xmlExperimentListFiles = new LinkedList<>();
    private static final String charset = java.nio.charset.StandardCharsets.UTF_8.name();


    @BeforeClass
    public static void classSetup() throws IOException {
        File dir = new File(xmlDirectoryPath);
        File[] filesList = dir.listFiles();
        if (filesList == null) {
            throw new IOException("Could not get directory list.");
        }

        for (File file : filesList) {
            file.setReadOnly();
            String filename = file.getName();

            if (!filename.substring(filename.length()-3).equals("xml")) {
                continue;
            }

            byte[] bytes = Files.readAllBytes(file.toPath());
            String expXML = new String(bytes, charset);
            if (expXML.contains("<Experiments>")) {
                xmlExperimentListFiles.add(file);
            } else if (expXML.contains("<Experiment>")) {
                xmlExperimentFiles.add(file);
            } else {
                System.out.println("Could not determine type of file "+filename);
            }
        }
    }

    @Test
    public void testExperimentsParseLocally() throws IOException {
        for (File experimentFile : xmlExperimentFiles) {
            auxParseExperimentFile(experimentFile);
        }
    }

    @Test
    public void testExperimentsCompareLocally()
            throws IOException, XMLParsingException, ExperimentParsingException
    {
        for (File experimentFile : xmlExperimentFiles) {
            Experiment experiment = auxParseExperimentFile(experimentFile);
            String xml = experiment.getXMLRepresentation();
            Experiment reReadExperiment = XMLParser.parseXML(XMLParser.convertToNode(xml));
            String newXML = reReadExperiment.getXMLRepresentation();

            if (!xml.equals(newXML)) {
                System.out.println(experimentFile.getName() + " failed.");
            }
            assertEquals(experiment, reReadExperiment);
            System.out.println(experimentFile.getName() + " succeeded.");
        }
    }

    private Experiment auxParseExperimentFile(File file) throws IOException {
        byte[] bytes = Files.readAllBytes(file.toPath());
        String expXML = new String(bytes, charset);
        try {
            Node expNode = XMLParser.convertToNode(expXML);
            return XMLParser.parseXML(expNode);
        } catch (XMLParsingException | ExperimentParsingException e) {
            System.out.println("For file "+file.getName());
            System.out.println(e.getMessage());
            e.printStackTrace();
            fail();
            return null; // make the compiler happy
        }
    }
}
