package com.MetaComponents;

import com.CockpitExceptions.InvalidExperimentException;
import com.Utilities.XMLTag;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by kirksmacbookair on 21/08/2016.
 */
public class ExperimentList extends ChaosObject {

    private class ExperimentItem extends ChaosObject {

        public final Experiment exp;
        public final String claimedOnDate;

        public ExperimentItem(Experiment exp, String claimedOnDate) {
            this.exp = exp;
            this.claimedOnDate = claimedOnDate;
        }

        public ExperimentItem(Experiment exp) {
            this(exp, "");
        }

        @Override
        public String getStateReport() {
            String errStr = concatenateErrorReports(getErrorStrings());
            errStr += exp.getStateReport();
            return errStr;
        }

        @Override
        public String getTextRepresentation() {
            String claimedString;
            if (claimedOnDate == null || claimedOnDate.isEmpty()){
                claimedString = "not claimed yet";
            } else {
                claimedString = "claimed on "+claimedOnDate;
            }

            String idString = (exp.id == null || exp.id.isEmpty())
                    ? "**no ID**" : exp.id;
            return String.format("experiment %s (%s)", idString, claimedString);
        }

        @Override
        protected List<String> getErrorStrings() {
            List<String> errs = new LinkedList<String>();

            if (exp == null) {
                errs.add("experiment not set");
            }

            return errs;
        }

        @Override
        public String getXMLRepresentation() throws IllegalStateException {
            return XMLTag.tag("Item", XMLTag.attr("ClaimedOnDate",
                    XMLTag.sanitize(claimedOnDate), "Id", exp.id), null);
        }
    }

    private LinkedList<ExperimentItem> expList = new LinkedList<ExperimentItem>();
    public String id;

    public ExperimentList() {

    }

    @Override
    public String getStateReport() {
        String errStr = concatenateErrorReports(getErrorStrings());
        for (ExperimentItem expItem : expList)
            errStr += expItem.getStateReport();
        return errStr;
    }

    @Override
    public String getTextRepresentation() {
        String out = String.format("Experiment list %s:\n", id);
        for (ExperimentItem expItem : expList) {
            out += String.format("* %s\n", expItem.getTextRepresentation());
        }
        return out;
    }

    @Override
    protected List<String> getErrorStrings() {
        List<String> errs = new LinkedList<String>();

        if (id==null || id.isEmpty())
            errs.add("id is not set");

        return errs;
    }

    @Override
    public String getXMLRepresentation() throws IllegalStateException {
        String listXML = "";
        for (ExperimentItem expItem : expList) {
            listXML += expItem.getXMLRepresentation();
        }

        return XMLTag.tag("Experiments", null, listXML);//+XMLTag.tag("Id", null, id));
    }

    public void addExperiment(Experiment exp) throws InvalidExperimentException
    {
        if (!exp.isValid()) {
            String msg = String.format("Experiment '%s' must be valid before " +
                    "added to experiment list.", exp.id);
            throw new InvalidExperimentException(msg);
        }
        expList.add(new ExperimentItem(exp, ""));
    }

    public Experiment[] getExperiments() {
        Experiment[] exps = new Experiment[expList.size()];
        for (int i = 0; i < expList.size(); i++) {
            exps[i] = expList.get(i).exp;
        }
        return exps;
    }
}
