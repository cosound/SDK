package com.MetaComponents;

import com.Utilities.XMLTag;
import com.Interfaces.XMLConvertable;

import java.util.List;

public class ChaosEvent implements XMLConvertable {
	public String datetime, id, type, method, data;

	public String getXMLRepresentation() {
        return XMLTag.tag("Item", null,
                  XMLTag.tag("Id", null, XMLTag.sanitize(id))+
                  XMLTag.tag("Type", null, XMLTag.sanitize(type))+
                  XMLTag.tag("Method", null, XMLTag.sanitize(method))+
                  XMLTag.tag("Data", null, XMLTag.sanitize(data))+
                  XMLTag.tag("DateTime", null, XMLTag.sanitize(datetime)));
    }

    public static String getEventsXMLRepresentation(List<ChaosEvent> events) {
        String eventsXML = "";
        for (ChaosEvent e : events) {
            eventsXML += e.getXMLRepresentation();
        }
        return XMLTag.tag("Events", null, eventsXML);
    }

    @Override
    public String toString() {
        return "Id:       " + id + "\n" +
               "Type:     " + type + "\n" +
               "Method:   " + method + "\n" +
               "Data:     " + data + "\n" +
               "DateTime: " + datetime + "\n";
    }
}
