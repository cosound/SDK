package com.MetaComponents;

import com.Components.*;
import com.sun.org.apache.bcel.internal.generic.Select;

import java.util.Arrays;
import java.util.List;

/**
 * Created by kirksmacbookair on 09/11/2016.
 */
public class Answer {
    public final String response;
    public final String headerLabel;
    public final String componentType;
    public final String stimulusLabel;
    public final String stimulusURI;
    public final List<ChaosEvent> events;

    private String getStimulusLabel(Stimulus stimulus) {
        if (stimulus == null) {
            return "(no stimulus)";
        } else {
            return stimulus.label;
        }
    }

    private String getStimulusURI(Stimulus stimulus) {
        if (stimulus == null) {
            return "(no stimulus)";
        } else {
            return stimulus.URI;
        }
    }

    public Answer(TrialComponent component) {
        response = component.getOutputValue();
        headerLabel = component.getHeaderLabel();
        componentType = component.getComponentType();
        events = component.getOutputEvents();

        if (component instanceof Selectable) {
            stimulusLabel = getStimulusLabel(((Selectable) component).getStimulus());
            stimulusURI = getStimulusURI(((Selectable) component).getStimulus());
        } else if (component instanceof KACPS) {
            String label = "";
            String uri = "";
            for (KACPS.KACPSItem item : ((KACPS) component).getItems()) {
                if (!label.isEmpty()) {
                    label += ", ";
                    uri += ", ";
                }
                label += getStimulusLabel(item.stimulus);
                uri += getStimulusURI(item.stimulus);
            }
            stimulusLabel = label;
            stimulusURI = uri;
        } else if (component instanceof OneDScale) {
            stimulusLabel = getStimulusLabel(((OneDScale) component).stimulus);
            stimulusURI = getStimulusURI(((OneDScale) component).stimulus);
        } else if (component instanceof Tagging) {
            stimulusLabel = getStimulusLabel(((Tagging) component).stimulus);
            stimulusURI = getStimulusURI(((Tagging) component).stimulus);
        } else {
            stimulusLabel = "(no stimulus)";
            stimulusURI = "(no stimulus)";
        }
    }
}
