package com.MetaComponents;

import com.Components.*;
import com.Utilities.XMLTag;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Trial extends ChaosObject {
	public String taskId = "1";
	public boolean isClosed = false;
	
	private LinkedList<TrialComponent> components;
	
	public Trial() {
		components = new LinkedList<TrialComponent>();
	}
	
	public Trial(String taskId) {
		this();
		this.taskId = taskId;
	}

	public TrialComponent addComponent(TrialComponent component) {
		components.add(component);
        return component;
	}

	public void addComponents(TrialComponent... varComps) {
		for (TrialComponent comp : varComps)
			components.add(comp);
	}

	public void removeComponent(int index) {
		components.remove(index);
	}

	public TrialComponent getComponent(int index) {
		return components.get(index);
	}

	@Override
	public String getXMLRepresentation() throws IllegalStateException {
		String compXML = "";
		for (TrialComponent comp : components) {
			compXML += comp.getXMLRepresentation();
		}
		
		return XMLTag.tag("Trial", XMLTag.attr("TaskId", String.valueOf(taskId)), compXML);
	}
	
	@Override
	public String getStateReport() {
		
		String errStr = concatenateErrorReports(getErrorStrings());
		for (TrialComponent comp : components) {
			errStr += comp.getStateReport();
		}
		
		return errStr;
	}
	
	@Override
	public List<String> getErrorStrings() {
		List<String> errs = new LinkedList<String>();
		
		if (components == null || components.size() == 0)
			errs.add("no components added");
		
		return errs;
	}

	@Override
	public String getTextRepresentation() {
		String out = "";
		for (TrialComponent comp : components) {
			out += String.format("%s\n", comp.getTextRepresentation());
		}
		return out;
	}

	@Override
	public String toString() {
	    return "Trial";
	}


	public List<TrialComponent> getComponents() {
		return components;
	}
}
