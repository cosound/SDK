package com.MetaComponents;

import com.Interfaces.XMLConvertable;

import java.util.List;

public abstract class ChaosObject implements XMLConvertable {

	public boolean isValid() {
		return (getStateReport().length() == 0);
	}
	
	public abstract String getStateReport();
	public abstract String getTextRepresentation();

	protected abstract List<String> getErrorStrings();
	
	protected String concatenateErrorReports(List<String> errs) {
		if (errs.size() == 0)
			return "";
		
		String concat = String.format("for object %s:\n", this.toString());
		for (String err : errs) {
			concat += String.format("* %s\n", err);
		}
		return concat+"\n";
	}
	
	protected String parseBool(boolean b) {
		return b ? "1" : "0";
	}
}
