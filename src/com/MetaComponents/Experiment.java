package com.MetaComponents;

import com.Components.EndOfExperiment;
import com.Components.Monitor;
import com.Components.TrialComponent;
import com.Utilities.XMLTag;

import javax.activity.InvalidActivityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Experiment extends ChaosObject {

	private LinkedList<Trial> trials;
	
	private boolean finalized;
	public int revisionID = 1;
	public String name, targetId, targetName, id, createdBy,
			experimentDescription, footerLabel, redirectOnCloseUrl;
	public String version = "1.0";
	public int noOfTrials, trialsCompleted;
	
	public boolean lockQuestion, enablePrevious;
	
	
	public Experiment() {
		trials = new LinkedList<Trial>();
	}

	@Override
	public String getStateReport() {
		String errStr = concatenateErrorReports(getErrorStrings());
		for (Trial trial : trials)
			errStr += trial.getStateReport();
		
		return errStr;
	}
	
	@Override
	protected List<String> getErrorStrings() {
		List<String> errs = new LinkedList<String>();
		
        if (name == null || name.length() == 0)
            errs.add("name is not set");
        
        if (version == null || version.length() == 0)
            errs.add("version is not set");
        
		if (trials == null || trials.size() == 0)
			errs.add("no trials added");

		return errs;
	}

	public LinkedList<Trial> getTrials() {
		return trials;
	}

	public Trial getTrial(int index) {
		return this.trials.get(index);
	}
	
	public void setTrials(LinkedList<Trial> trials) {
		this.trials = trials;
	}

	public void setTrial(int index, Trial trial) {
		this.trials.set(index, trial);
	}
	
	public Trial createTrial() {
	    return createTrial(trials.size());
	}

    public void createTrial(Trial trial) {
        trials.add(trial);
    }

    /**
	 * Create new trial at specified position. Trial object is returned.
	 * @param index Position of new trial in experiment.
	 * @return New trial.
	 */
	public Trial createTrial(int index) {
		// TODO: Auto-generate trial name and version?
		Trial newTrial = new Trial();
		if (index == trials.size()) {
			trials.add(newTrial);
		} else {
			trials.add(index, newTrial);
		}
		
		return newTrial;
	}
	
	/**
	 * Remove trial at specified position.
	 * @param index Index of trial to be removed.
	 * @throws InvalidActivityException
	 */
	public void removeTrial(int index) throws InvalidActivityException {
		if (true) { // TODO: test if removal is valid
			trials.remove(index);
		} else {
			throw new InvalidActivityException("Cannot remove trial because reason.");
		}
	}
	
	public void removeTrial(Trial trial) throws InvalidActivityException {
		trials.remove(trial);
	}
	
	public TrialComponent endExperiment() throws InvalidActivityException {
		// Check if experiment is valid
        String errs = getStateReport();
		if (errs != null && errs.length() > 0) {
			throw new InvalidActivityException("Experiment is not in a valid state.");
		}

        // Add monitor component to all trials that do not already have one
        for (Trial trial : trials) {
            boolean hasMonitor = false;
            for (TrialComponent comp : trial.getComponents()) {
                if (comp instanceof Monitor) {
                    hasMonitor = true;
                    break;
                }
            }

            if (!hasMonitor) {
                trial.addComponent(new Monitor());
            }
        }

        // Add EndOfExperiment component to last trial
		EndOfExperiment eoe = new EndOfExperiment();
		trials.getLast().addComponent(eoe);

        return eoe;
	}
	
	public void setFinalized(boolean finalized) throws InvalidActivityException {
        if (isValid())
    		this.finalized = finalized;
        else
            throw new InvalidActivityException("Cannot set invalid experiment as finalized.");
	}

	public boolean isFinalized() {
		return finalized;
	}
	
	public String getXMLRepresentation() throws IllegalStateException {
		if (!isValid()) {
			throw new IllegalStateException("Experiment is currently invalid and cannot be " +
					"converted to XML.");
		}
		String trialXML = "";
		for (Trial trial : trials) {
			trialXML += trial.getXMLRepresentation();
		}

        String redirect = XMLTag.tag("RedirectOnCloseUrl", null,
                XMLTag.sanitize(redirectOnCloseUrl));

		return XMLTag.tag("Experiment", null,
				 XMLTag.tag("Id", null, XMLTag.sanitize(id))+
			     XMLTag.tag("Name", null, XMLTag.sanitize(name))+
			     XMLTag.tag("Version", null, "1")+
				 XMLTag.tag("Target", XMLTag.attr("Id",targetId,"Name",targetName), null)+
			     XMLTag.tag("ExperimentDescription", null, XMLTag.sanitize(experimentDescription))+
			     XMLTag.tag("CreatedBy", null, XMLTag.sanitize(createdBy))+
			     XMLTag.tag("LockQuestion", null, parseBool(lockQuestion))+
			     XMLTag.tag("EnablePrevious", null, parseBool(enablePrevious))+
			     XMLTag.tag("NoOfTrials", null, Integer.toString(trials.size()))+
			     XMLTag.tag("TrialsCompleted", null, Integer.toString(trialsCompleted))+
                 XMLTag.tag("FooterLabel", null, XMLTag.sanitize(footerLabel))+
                 redirect+
			     XMLTag.tag("Trials", null, trialXML)
			   );
	}
	
	@Override
	public String getTextRepresentation() {
		String out = String.format("%s\n\n", toString());
		String line = "--------------------------------------";
		for (int i = 0; i < trials.size(); i++) {
			out += String.format("%d. trial\n%s\n%s\n",
					i, line, trials.get(i).getTextRepresentation());
		}
		return out;
	}
	
	@Override
	public String toString() {
		String outName = name != null && !name.isEmpty() ? name : "Unnamed experiment";
		String outId = id != null && !id.isEmpty() ? " ("+id+")" : "";
		return outName + outId;
	}

    @Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Experiment)) {
			return false;
		}
		Experiment otherExperiment = (Experiment) obj;

		return otherExperiment.getXMLRepresentation().equals(getXMLRepresentation());
	}
}
