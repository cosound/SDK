package com.Components;

import com.MetaComponents.ChaosEvent;
import com.MetaComponents.ChaosObject;
import com.Utilities.XMLTag;

import java.util.*;

public abstract class TrialComponent extends ChaosObject {
	protected List<ChaosEvent> outputEvents;
	private String outputValue;
	protected final String genericOutEvents =
        XMLTag.tag("MultiValue", XMLTag.attr("Id", "Events", "Max", "Inf", "Min", "0"),
                   XMLTag.tag("ComplexValue", XMLTag.attr("Id", "Event"),
                  	  XMLTag.tag("SimpleValue", XMLTag.attr("Id", "DateTime",
                  	      "Validation", "(\\d{4}-\\d{2}-\\d{2})T(\\d{2}:\\d{2}:\\d{2}.\\d{3})Z"), null)+
                  	  XMLTag.tag("SimpleValue", XMLTag.attr("Id", "Type", "Validation", ".*"), null)+
                  	  XMLTag.tag("SimpleValue", XMLTag.attr("Id", "Id", "Validation", ".*"), null)+
                  	  XMLTag.tag("SimpleValue", XMLTag.attr("Id", "Data", "Validation", ".*"), null)+
                  	  XMLTag.tag("SimpleValue", XMLTag.attr("Id", "Method", "Validation", ".*"), null)
                   )
		);

	protected TrialComponent() {
		outputEvents = new LinkedList<>();
	}

	public void addOutputEvents(ChaosEvent... events) {
		// TODO: Validation of event?
		for (ChaosEvent e : events) {
			outputEvents.add(e);
		}
	}

	public List<ChaosEvent> getOutputEvents() {
		return new LinkedList<>(outputEvents);
	}

	public void setOutputValue(String value) {
		outputValue = (value == null) ? "" : value;
	}

	public String getOutputValue() {
		return outputValue;
	}

	public abstract String getHeaderLabel();

	public abstract String getComponentType();
}
