package com.Components;

import com.Utilities.XMLTag;

import java.util.LinkedList;
import java.util.List;

public class EndOfExperiment extends Fixed {
	@Override
	public String getXMLRepresentation() throws IllegalStateException{
		return XMLTag.tag("EndOfExperiment", XMLTag.attr("Version", "1"),
				XMLTag.tag("Inputs", null, null)+
					XMLTag.tag("Outputs", null,
					XMLTag.tag("Validation", null, null)+
					XMLTag.tag("Value", null, null)
				  )
				);
	}

	@Override
	public List<String> getErrorStrings() {
		return new LinkedList<String>();
	}
	
	@Override
	public String getStateReport() {
		return "";
	}

	@Override
	public EndOfExperiment clone() {
		return new EndOfExperiment();
	}

	@Override
	public String getComponentType() {
		return "EndOfExperiment";
	}

	@Override
	public String getHeaderLabel() {
		return null;
	}
}
