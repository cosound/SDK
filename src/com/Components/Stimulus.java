package com.Components;

import com.MetaComponents.ChaosObject;
import com.Utilities.XMLTag;

import java.util.List;
import java.util.regex.Pattern;

public class Stimulus extends ChaosObject {
	private enum StimulusType {
		AUDIO, VIDEO, PHOTO
	}
	
	public final StimulusType type;
	public final String URI;
	public final String label;
	
	public Stimulus (String URI, String label) throws IllegalArgumentException {
		this(URI, label, getTypeFromURI(URI));
	}
	
	public Stimulus(String URI, String label, String strType) {
		this.URI = URI;
		this.label = label;
		switch (strType.toLowerCase()) {
            case "audio":
            case "audio/mpeg":
                this.type = StimulusType.AUDIO;
                break;
            case "video":
                this.type = StimulusType.VIDEO;
                break;
            case "photo":
                this.type = StimulusType.PHOTO;
                break;
            default:
                throw new IllegalArgumentException("Unrecognized stimulus type: \""+strType+"\".");
        }
	}
	
	private static String getTypeFromURI(String URI) throws IllegalArgumentException {
		String[] tokens = URI.split("\\.");
		IllegalArgumentException except = new IllegalArgumentException("Stimulus type could not be inferred from URI. Please supply type explicitly.");
		if (tokens.length == 0) {
			throw except;
		}
		
		String filetype = tokens[tokens.length-1].trim();
		if (Pattern.matches("tiff?|gif|jpe?g|jf?if|jp(2|x)|j2(k|c)|fpx|pcd|png|pdf", filetype)) {
			return "photo";
		} else if (Pattern.matches("aa(c|x)|flac|m4a|mp3|oga|raw|wav|wma", filetype)) {
			return "audio";
		} else if (Pattern.matches("mkv|voc|ogv|avi|mov|qt|wmv|mp4|m4(p|v)|mp(g|2|eg|e|v)|m2v|flv|f4(v|p|a|b)", filetype)) {
			return "video";
		}
		
		throw except;
	}

	public String getTypeString() {
		if(type == StimulusType.AUDIO) return "audio/mpeg";
		else if(type == StimulusType.VIDEO) return "video";
        else return "photo";
	}
	
    @Override
    public String getXMLRepresentation() {
        return XMLTag.tag("Stimulus", null,
            XMLTag.tag("Type", null, XMLTag.sanitize(getTypeString()))+
            XMLTag.tag("URI", null, XMLTag.sanitize(URI))+
			XMLTag.tag("Label", null, XMLTag.sanitize(label))
        );
    }
    
    @Override
    public String getTextRepresentation() {
    	return String.format("Stimulus: %s (%s)\n", URI, label);
    }

	@Override
	public String getStateReport() {
		return "";
	}

	@Override
	protected List<String> getErrorStrings() {
		return null;
	}

	@Override
	public Stimulus clone() {
		String typeStr;
		switch(type) {
			case AUDIO:
				typeStr = "audio/mpeg";
				break;
			case VIDEO:
				typeStr = "video";
				break;
			case PHOTO:
				typeStr = "photo";
				break;
			default:
				typeStr = null;
		}
		return new Stimulus(URI, label, typeStr);
	}

	@Override
	public String toString() {
		return String.format("%s (%s)", URI, label);
	}
}
