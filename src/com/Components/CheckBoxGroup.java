package com.Components;

import com.Utilities.XMLTag;

import java.util.*;

public class CheckBoxGroup extends Selectable {

    public int minNoOfSelections = 1;
    public int maxNoOfSelections = 1;
	public String validationID = ".*";

	public CheckBoxGroup() {
		super();
        this.xmlName = "CheckBoxGroup";
	}

	public CheckBoxGroup(String headerLabel, String... itemLabels) {
        this();
        addItems(itemLabels);
        this.headerLabel = headerLabel;
        minNoOfSelections = 1;
        maxNoOfSelections = itemLabels.length;
	}

    @Override
    protected String getInstrumentXML() {
        String minXML = XMLTag.tag("MinNoOfSelections", null,
				String.valueOf(minNoOfSelections));
        String maxXML = XMLTag.tag("MaxNoOfSelections", null,
				String.valueOf(maxNoOfSelections));
        return minXML+maxXML;
    }

    @Override
    protected String getValidationXML() {
		String attr = XMLTag.attr("Id", "Selections",
				"Max", String.valueOf(maxNoOfSelections),
                "Min", String.valueOf(minNoOfSelections));
		return XMLTag.tag("MultiValue", attr,
				XMLTag.tag("SimpleValue",
						XMLTag.attr("Id", "Id", "Validation", validationID),
						null)
                );
    }


	@Override
	public String getStateReport() {
		List<String> errs = super.getErrorStrings();

		errs.addAll(getErrorStrings());

		return concatenateErrorReports(errs);
	}

	@Override
	public List<String> getErrorStrings() {
		List<String> errs = super.getErrorStrings();
		if (errs == null) {
			errs = new LinkedList<>();
		}

		if (minNoOfSelections < 0 || minNoOfSelections >= items.size())
			errs.add("invalid value for minNoOfScalings");

		if (maxNoOfSelections <= 0)
			errs.add("invalid value for maxNoOfScalings");

		if (minNoOfSelections > maxNoOfSelections)
			errs.add("minNoOfScalings larger than maxNoOfScaligns");

		return errs;
	}

	@Override
	public String toString() {
		return "CheckBoxGroup" + getLabelString();
	}
}
