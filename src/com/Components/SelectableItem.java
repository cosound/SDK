package com.Components;

import com.Utilities.XMLTag;

import java.util.LinkedList;
import java.util.List;

public class SelectableItem extends Item {
    // TODO update itemID's before exporting

    public boolean selected = false;
    public String label;
    public String itemID = "";

    public SelectableItem(String label) {
        this.label = label;
    }

    @Override
    public String getXMLRepresentation() throws IllegalStateException {
        if (!this.isValid())
            throw new IllegalStateException("Item is not in a valid state.");

        if (label == null) {
            label = "";
        }

        String idLabel = XMLTag.tag("Id", null, itemID);
        String xmlLabel = XMLTag.tag("Label", null, XMLTag.sanitize(label));
        String selLabel = XMLTag.tag("Selected", null, parseBool(selected));
        return XMLTag.tag("Item", null, idLabel+xmlLabel+selLabel);
    }

    @Override
    public String getStateReport() {
        List<String> errs = new LinkedList<String>();

        if (itemID == null || itemID.length() == 0)
            errs.add("no id");

        return concatenateErrorReports(errs);
    }

    @Override
    public String toString() {
        return label;
    }

    @Override
    public String getTextRepresentation() {
        return toString();
    }

    @Override
    public SelectableItem clone() {
        return new SelectableItem(label);
    }
}
