package com.Components;

import com.Utilities.XMLTag;

import java.util.LinkedList;
import java.util.List;

public class LikertScale extends Selectable {

    public int minNoOfScalings = 1;
	public String validationID = ".*";

	public LikertScale() {
		super();
        this.xmlName = "LikertScale";
	}
	
	public LikertScale(String headerLabel, String... itemLabels) {
        this();
		this.headerLabel = headerLabel;
        addItems(itemLabels);
	}

    @Override
    protected String getInstrumentXML() {
		int maxNoOfScalings = 1;
		String maxXML = XMLTag.tag("MaxNoOfScalings", null, String.valueOf(maxNoOfScalings));
        String minXML = XMLTag.tag("MinNoOfScalings", null, String.valueOf(minNoOfScalings));
        return maxXML+minXML;
    }

    @Override
    protected String getValidationXML() {
        return XMLTag.tag("SimpleValue", XMLTag.attr("Id", "Id", "Validation", validationID), null);
    }

	@Override
	public LikertScale clone() {
		LikertScale ls = new LikertScale();
		ls.headerLabel = headerLabel;
		for (int i = 0; i < items.size(); i++) {
			ls.addItem(items.get(i).label);
			ls.setSelected(i, items.get(i).selected);
		}
		return ls;
	}

	@Override
	public String getStateReport() {
		List<String> errs = super.getErrorStrings();
		if (errs == null) {
			errs = new LinkedList<>();
		}

		if (minNoOfScalings < 0 || minNoOfScalings > 1) {
			errs.add("MinNoOfScalings must be either 0 or 1.");
		}

		errs.addAll(getErrorStrings());
		return concatenateErrorReports(errs);
	}

	@Override
	public String toString() {
		return "LikertScale" + getLabelString();
	}

	@Override
	public String getTextRepresentation() {
		return super.getTextRepresentation();
	}
}
