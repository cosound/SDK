package com.Components;

import com.Utilities.XMLTag;

import java.util.LinkedList;
import java.util.List;

public class FreeTextHash extends Interaction {

    public String validation = ".*";
    public String position = "top";
    public Integer length;
	public boolean forceLowerCase = false;

	public FreeTextHash(String label) {
	    this.headerLabel = label;
    }

	@Override
	public String getXMLRepresentation() throws IllegalStateException {
		if (!isValid()) {
			throw new IllegalStateException("FreeTextHash object is not in a valid state.");
		}

		return XMLTag.tag("FreeTextHash", XMLTag.attr("Version", "1"),
				XMLTag.tag("Inputs", null,
					XMLTag.tag("Events", null, null)+
					    XMLTag.tag("Instrument", null,
						    XMLTag.tag("Label", null, XMLTag.sanitize(headerLabel))+
							    XMLTag.tag("LabelPosition", null, XMLTag.sanitize(position))+
								XMLTag.tag("Validation", null, XMLTag.sanitize(validation))+
                                XMLTag.tag("ForceLowerCase", null, parseBool(forceLowerCase))
								)
				)+
				XMLTag.tag("Outputs", null,
				    XMLTag.tag("Validation", null,
					    XMLTag.tag("SimpleValue", XMLTag.attr("Id", "Text", "Validation",
                                XMLTag.sanitize(validation)), null)+
						    genericOutEvents
                    )
				)
		);
	}

	@Override
	public String getStateReport() {
		return concatenateErrorReports(getErrorStrings());
	}

	@Override
	public List<String> getErrorStrings() {
		List<String> errs = new LinkedList<String>();

		if (headerLabel == null || headerLabel.isEmpty())
			errs.add("label text is empty");

		if (!("top".equals(position) || "right".equals(position) ||
              "bottom".equals(position) || "left".equals(position)))
		{
            errs.add("position must be set to either \"top\", \"right\"," +
                     " \"bottom\", or \"left\"");
        }

		if (validation == null || validation.isEmpty())
			errs.add("validation field not set");
		
		return errs;
	}

	@Override
	public String toString() {
		return String.format("FreeTextHash (\"%s\": \"%s\")", headerLabel, validation);
	}

	@Override
    public String getTextRepresentation() {
        return toString();
    }

	@Override
	public String getComponentType() {
		return "FreeTextHash";
	}
}
