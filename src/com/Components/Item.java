package com.Components;

import com.MetaComponents.ChaosObject;

import java.util.LinkedList;
import java.util.List;

public abstract class Item extends ChaosObject {

	@Override
	public String getStateReport() {
		return "";
	}

	@Override
	public List<String> getErrorStrings() {
		return new LinkedList<String>();
	}

	@Override
	public String getXMLRepresentation() throws IllegalStateException {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public String getTextRepresentation() {
		return toString();
	}

}
