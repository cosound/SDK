package com.Components;

import com.Utilities.XMLTag;

import java.util.LinkedList;
import java.util.List;

public class FreeText extends Interaction {

	public String labelPosition;
    public String validation = ".*";

	public Integer boxWidth;
	public Integer boxHeight;
	public Boolean resizable;
	public String label;

	public boolean alignForStimuli = true;

	public FreeText() {
		this.label = "";
	}


	public FreeText(String label) {
		this.label = label;
	}

	@Override
	public String getTextRepresentation() {
		return toString();
	}

	@Override
	public String getXMLRepresentation() throws IllegalStateException {
		if (!isValid()) {
			throw new IllegalStateException("FreeText is not in valid state.");
		}


        String boxWidthStr = (boxWidth == null) ? null : boxWidth.toString();
        String boxHeightStr = (boxHeight == null) ? null : boxHeight.toString();
        String resizableStr = (resizable == null) ? null : resizable.toString();

		return XMLTag.tag("Freetext", XMLTag.attr("Version", "1"),
				XMLTag.tag("Inputs", null,
					XMLTag.tag("Events", null, null) +
						XMLTag.tag("Instrument", null,
							XMLTag.tag("Label", null, label) +
								XMLTag.tag("LabelPosition", null, XMLTag.sanitize(labelPosition)) +
								XMLTag.tag("Validation", null, XMLTag.sanitize(validation))+
								XMLTag.tag("BoxWidth", null, boxWidthStr) +
								XMLTag.tag("BoxHeight", null, boxHeightStr) +
								XMLTag.tag("Resizable", null, resizableStr)
							)
				) +
				XMLTag.tag("Outputs", null,
					XMLTag.tag("Validation", null,
						XMLTag.tag("SimpleValue", XMLTag.attr("Id", "Text", "Validation", validation), null) +
							genericOutEvents)
				)
		);
	}

	@Override
	public String getStateReport() {
		return concatenateErrorReports(getErrorStrings());
	}

	@Override
	public List<String> getErrorStrings() {
		List<String> errs = new LinkedList<>();

		if (label == null || label.isEmpty())
			errs.add("label text is empty");

		if (!("top".equals(labelPosition) || "right".equals(labelPosition) ||
				"bottom".equals(labelPosition) || "left".equals(labelPosition))) {
			errs.add("position must be set to either \"top\", \"right\"," +
					" \"bottom\", or \"left\"");
		}

		if ((boxWidth != null && boxWidth < 0) ||
                (boxHeight != null && boxHeight< 0)) {
            errs.add("invalid box size");
        }

		return errs;
	}

	@Override
	public String toString() {
		return String.format("FreeText (\"%s\")\n", label);
	}

	@Override
	public String getComponentType() {
		return "FreeText";
	}

	@Override
	public String getHeaderLabel() {
		return label;
	}
}
