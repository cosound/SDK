package com.Components;

import com.Utilities.XMLTag;

import java.util.LinkedList;
import java.util.List;

public class RadioButtonGroup extends Selectable {

	public int minNoOfScalings = 1;
	public String validationID = ".*";

	public RadioButtonGroup() {
		super();
        this.xmlName = "RadioButtonGroup";
	}

	public RadioButtonGroup(String headerLabel, String... itemLabels) {
        this();
		this.headerLabel = headerLabel;
        addItems(itemLabels);
	}
    
    @Override
    protected String getInstrumentXML() {
		String minXML = XMLTag.tag("MinNoOfScalings", null, String.valueOf(minNoOfScalings));
		int maxNoOfScalings = 1;
		String maxXML = XMLTag.tag("MaxNoOfScalings", null, String.valueOf(maxNoOfScalings));
		return minXML+maxXML;
    }

    @Override
    protected String getValidationXML() {
		return XMLTag.tag("SimpleValue", XMLTag.attr("Id", "Id", "Validation", validationID), null);
    }

	@Override
	public String getStateReport() {
		List<String> errs = super.getErrorStrings();
		
		errs.addAll(getErrorStrings());

		return concatenateErrorReports(errs);
	}

	@Override
	public List<String> getErrorStrings() {
		List<String> errs = super.getErrorStrings();
		if (errs == null) {
			errs = new LinkedList<>();
		}
		
		int c = 0;
		for (SelectableItem item : items) {
			if (item.selected)
				c++;
			if (c > 1)
				break;
		}
		
		if (c > 1) {
			errs.add("more than 1 item set to selected");
		}


		if (minNoOfScalings < 0 || minNoOfScalings > 1) {
			errs.add("MinNoOfScalings must be either 0 or 1.");
		}

		return errs;
	}
	
	@Override
	public String toString() {
		return "RadioButtonGroup" + getLabelString();
	}

	@Override
	public RadioButtonGroup clone() {
		RadioButtonGroup rbg = new RadioButtonGroup();
		rbg.headerLabel = headerLabel;
		for (int i = 0; i < items.size(); i++) {
			rbg.addItem(items.get(i).label);
			rbg.setSelected(i, items.get(i).selected);
		}
		return rbg;
	}
}
