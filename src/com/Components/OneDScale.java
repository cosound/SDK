package com.Components;

import com.Utilities.XMLTag;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by kirksmacbookair on 05/10/2016.
 */
public class OneDScale extends Interaction {

    public class AxisTick {
        public final String label;
        public final double position;

        public AxisTick(String label, double position) throws IllegalArgumentException {
            if (position < -1 || position > 1) {
                throw new IllegalArgumentException("Position argument must be in interval [-1,1].");
            }
            this.label = label;
            this.position = position;
        }
    }

    public Double position = null;
    public boolean alignForStimuli = false;
    private List<AxisTick> x1Ticks = new LinkedList<>();
    private List<AxisTick> x2Ticks = new LinkedList<>();
    private List<AxisTick> y1Ticks = new LinkedList<>();
    private List<AxisTick> y2Ticks = new LinkedList<>();
    public Stimulus stimulus;
    public String x1AxisLabel, x2AxisLabel, y1AxisLabel, y2AxisLabel;

    private List<AxisTick> parseObjectArray(Object... ticks) throws IllegalArgumentException {
        List<AxisTick> res = new LinkedList<>();
        IllegalArgumentException e = new IllegalArgumentException("Argument must have format " +
                "{String, double, String, double, ...}.");
        if (ticks.length % 2 != 0) {
            throw e;
        }
        for (int i = 0; i < ticks.length; i += 2) {
            try {
                String axisLabel = (String)ticks[i];
                Double axisPosition = (Double)ticks[i+1];
                res.add(new AxisTick(axisLabel, axisPosition));
            } catch (ClassCastException cce) {
                throw e;
            }
        }
        return res;
    }

    public void addX1AxisTick(String label, double position) throws IllegalArgumentException {
        x1Ticks.add(new AxisTick(label, position));
    }

    public void addX2AxisTick(String label, double position) throws IllegalArgumentException {
        x2Ticks.add(new AxisTick(label, position));
    }

    public void addY1AxisTick(String label, double position) throws IllegalArgumentException {
        y1Ticks.add(new AxisTick(label, position));
    }

    public void addY2AxisTick(String label, double position) throws IllegalArgumentException {
        y2Ticks.add(new AxisTick(label, position));
    }

    public void setStimulus(String... varargs) {
        switch (varargs.length) {
            case 2:
                stimulus = new Stimulus(varargs[0], varargs[1]);
                break;
            case 3:
                stimulus = new Stimulus(varargs[0], varargs[1], varargs[2]);
                break;
            default:
                throw new IllegalArgumentException("Method must be called with either 2 or 3 arguments.");
        }
    }

    public String getXMLRepresentation() {
        String tickTags = "";

        Object[][] axisArray = new Object[][]{
                {"X1AxisTick", x1Ticks}, {"X2AxisTick", x2Ticks},
                {"Y1AxisTick", y1Ticks}, {"Y2AxisTick", y2Ticks}};

        for (Object[] stringTick : axisArray) {
            String axisTag = (String)stringTick[0];
            List<AxisTick> list = (List<AxisTick>)stringTick[1];
            String innerTags = "";
            for (AxisTick tick : list) {
                innerTags += XMLTag.tag(axisTag, null,
                                XMLTag.tag("Label", null, XMLTag.sanitize(tick.label))+
                                XMLTag.tag("Position", null, Double.toString(tick.position)));
            }
            tickTags += XMLTag.tag(axisTag+"s", null, innerTags);
        }

        String positionString = position == null ? null : String.valueOf(position);

        String stimXML;
        if (stimulus == null)  {
            stimXML = XMLTag.tag("Stimulus", null, null);
        } else {
            stimXML = stimulus.getXMLRepresentation();
        }

        return XMLTag.tag("OneDScale", XMLTag.attr("Version", "1"),
                XMLTag.tag("Inputs", null,
                    XMLTag.tag("Events", null, null)+
                    XMLTag.tag("Instrument", null,
                        XMLTag.tag("HeaderLabel", null, XMLTag.sanitize(headerLabel))+
                        XMLTag.tag("Position", null, XMLTag.sanitize(positionString))+
                        XMLTag.tag("AlignForStimuli", null, parseBool(alignForStimuli))+
                        XMLTag.tag("X1AxisLabel", null, XMLTag.sanitize(x1AxisLabel))+ // FIXME not showing
                        XMLTag.tag("X2AxisLabel", null, XMLTag.sanitize(x2AxisLabel))+ // FIXME not showing
                        XMLTag.tag("Y1AxisLabel", null, XMLTag.sanitize(y1AxisLabel))+ // FIXME not showing
                        XMLTag.tag("Y2AxisLabel", null, XMLTag.sanitize(y2AxisLabel))+ // FIXME not showing
                        tickTags+
                        stimXML
                    )
                )+
                XMLTag.tag("Outputs", null,
                    XMLTag.tag("Validation", null,
                        XMLTag.tag("SimpleValue", XMLTag.attr("Id", "Position", "Validation", ".*"),
                                null)+
                        genericOutEvents
                    )+
                    XMLTag.tag("Value", null, null)
                )
        );
    }

    public String getTextRepresentation() {
        String out = "OneDScale:\n";
        out += "Label: "+headerLabel+"\n";
        if (stimulus != null) {
            out += stimulus.getTextRepresentation()+"\n";
        }

        out += x1AxisLabel != null && !x1AxisLabel.isEmpty() ?
                    x1AxisLabel+" (x1): " : "x1 (no label): ";
        for (int i = 0; i < x1Ticks.size(); i++) {
            out += String.format("(%s, %.2f)", x1Ticks.get(i).label, x1Ticks.get(i).position);
            if (i < x1Ticks.size()-1) {
                out += ", ";
            }
        }
        out += "\n";

        out += x2AxisLabel != null && !x2AxisLabel.isEmpty() ?
                x2AxisLabel+" (x2): " : "x2 (no label): ";
        for (int i = 0; i < x2Ticks.size(); i++) {
            out += String.format("(%s, %.2f)", x2Ticks.get(i).label, x2Ticks.get(i).position);
            if (i < x2Ticks.size()-1) {
                out += ", ";
            }
        }
        out += "\n";

        out += y1AxisLabel != null && !y1AxisLabel.isEmpty() ?
                y1AxisLabel+" (y1): " : "y1 (no label): ";
        for (int i = 0; i < y1Ticks.size(); i++) {
            out += String.format("(%s, %.2f)", y1Ticks.get(i).label, y1Ticks.get(i).position);
            if (i < y1Ticks.size()-1) {
                out += ", ";
            }
        }
        out += "\n";

        out += y2AxisLabel != null && !y2AxisLabel.isEmpty() ?
                y2AxisLabel+" (y2): " : "y2 (no label): ";
        for (int i = 0; i < y2Ticks.size(); i++) {
            out += String.format("(%s, %.2f)", y2Ticks.get(i).label, y2Ticks.get(i).position);
            if (i < y2Ticks.size()-1) {
                out += ", ";
            }
        }
        out += "\n";

        return out;
    }

    public List<String> getErrorStrings() {
        return new LinkedList<String>();
    }

    public String getStateReport() {
        List<String> errs = getErrorStrings();
        return concatenateErrorReports(errs);
    }

    @Override
    public String getComponentType() {
        return "OneDScale";
    }


}
