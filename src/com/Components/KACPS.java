package com.Components;

import com.Interfaces.XMLConvertable;
import com.Utilities.XMLTag;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by kirksmacbookair on 10/10/2016.
 */
public class KACPS extends Interaction {
    public class KACPSItem implements XMLConvertable {
        public String itemId = null;
        public final String choiceButtonLabel;
        public final boolean choiceButtonSelected;
        public Stimulus stimulus;

        public KACPSItem(String choiceButtonLabel, boolean choiceButtonSelected)
        {
            this.choiceButtonLabel = choiceButtonLabel;
            this.choiceButtonSelected = choiceButtonSelected;
        }

        public KACPSItem(String choiceButtonLabel, boolean choiceButtonSelected, String stimulusURI,
                         String stimulusLabel)
        {
            this(choiceButtonLabel, choiceButtonSelected);
            this.stimulus = new Stimulus(stimulusURI, stimulusLabel);
        }

        public String getXMLRepresentation() {
            String stimulusString = stimulus != null ? stimulus.getXMLRepresentation() : "";
            return XMLTag.tag("Item", null,
                    XMLTag.tag("Id", null, itemId) +
                    stimulusString +
                    XMLTag.tag("ChoiceButton", null,
                        XMLTag.tag("Label", null, XMLTag.sanitize(choiceButtonLabel)))
            );
        }

        public String getTextRepresentation() {
            String selString = choiceButtonSelected ? " (selected)" : "";
            String stimString = stimulus == null ? "No Stimulus\n":stimulus.getTextRepresentation();
            return "* "+choiceButtonLabel + selString + "\n  " + stimString;
        }

    }

    public final String headerLabel;
    private final int minNoOfScalings = 1;
    private final int maxNoOfScalings = 1;
    private LinkedList<KACPSItem> kacpsItems = new LinkedList<>();

    public KACPS(String headerLabel) {
        this.headerLabel = headerLabel;
    }

    public List<KACPSItem> getItems() {
        return new LinkedList<>(kacpsItems);
    }

    public KACPSItem addButton(String buttonLabel, boolean selected)
    {
        KACPSItem item = new KACPSItem(buttonLabel, selected);
        kacpsItems.add(item);
        return item;
    }

    public KACPSItem addButton(String buttonLabel, boolean selected, String stimulusURI,
                          String stimulusLabel)
    {
        KACPSItem item = new KACPSItem(buttonLabel, selected, stimulusURI, stimulusLabel);
        kacpsItems.add(item);
        return item;
    }

    public String getXMLRepresentation() {

        String itemsString = "";
        for (int i = 0; i < kacpsItems.size(); i++) {
            KACPSItem item = kacpsItems.get(i);
            if (item.itemId == null || item.itemId.isEmpty()) {
                item.itemId = String.format("%d:%s", i, item.choiceButtonLabel);
            }

            String stimulusStr = null;
            String stimulusLabel = null;
            if (item.stimulus != null) {
                stimulusStr = XMLTag.tag("Type", null, item.stimulus.getTypeString())+
                              XMLTag.tag("URI", null, XMLTag.sanitize(item.stimulus.URI))+
                              XMLTag.tag("Label", null, null);
                stimulusLabel = item.stimulus.label;
            }

            itemsString += XMLTag.tag("Item", null,
                XMLTag.tag("Id", null, item.itemId)+
                XMLTag.tag("StimulusButton", null,
                    XMLTag.tag("Label", null, XMLTag.sanitize(stimulusLabel))
                )+
                XMLTag.tag("ChoiceButton", null,
                    XMLTag.tag("Label", null, XMLTag.sanitize(item.choiceButtonLabel))+
                    XMLTag.tag("Selected", null, parseBool(item.choiceButtonSelected))
                )+
                XMLTag.tag("Stimulus", null, stimulusStr)
            );
        }

        return XMLTag.tag("KacPS", XMLTag.attr("Version", "1"),
                XMLTag.tag("Inputs", null,
                    XMLTag.tag("Events", null, null)+
                    XMLTag.tag("Instrument", null,
                        XMLTag.tag("HeaderLabel", null, XMLTag.sanitize(headerLabel))+
                        XMLTag.tag("MinNoOfScalings", null, Integer.toString(minNoOfScalings))+
                        XMLTag.tag("MaxNoOfScalings", null, Integer.toString(maxNoOfScalings))+
                        XMLTag.tag("Items", null, itemsString)
                    )
                )+
                XMLTag.tag("Outputs", null,
                    XMLTag.tag("Validation", null,
                        XMLTag.tag("MultiValue", XMLTag.attr("Id", "Selections",
                                                             "Max", "1",
                                                             "Min", "0"),
                            XMLTag.tag("SimpleValue", XMLTag.attr("Id", "Id", "Validation", ".*"), null)
                        )+
                        genericOutEvents
                    )+
                    XMLTag.tag("Value", null, null)
                )
        );
    }

    public String getTextRepresentation() {
        String out = "KAC-PS:\n";
        out += "Label: "+headerLabel+"\n";
        out += "Items:\n";
        for (KACPSItem item : kacpsItems) {
            out += item.getTextRepresentation() + "\n";
        }
        return out;
    }

    public List<String> getErrorStrings() {
        List<String> errs = new LinkedList<>();
        return errs;
//        if (x1Ticks == null || x1Ticks.size() == 0) {
//            errs.add("No X1 axis ticks.");
//        }
//
//        if (x2Ticks == null || x2Ticks.size() == 0) {
//            errs.add("No X2 axis ticks.");
//        }
//
//        if (y1Ticks == null || y1Ticks.size() == 0) {
//            errs.add("No Y1 axis ticks.");
//        }
//
//        if (y2Ticks == null || y2Ticks.size() == 0) {
//            errs.add("No Y2 axis ticks.");
//        }
//
//        return errs;
    }

    public String getStateReport() {
        List<String> errs = getErrorStrings();
        return concatenateErrorReports(errs);
    }

    @Override
    public String getComponentType() {
        return "KACPS";
    }
}
