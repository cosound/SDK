package com.Components;

import com.CockpitExceptions.CockpitException;
import com.CockpitExceptions.InvalidExperimentException;
import com.Utilities.XMLTag;
import com.sun.javaws.exceptions.InvalidArgumentException;

import java.util.LinkedList;
import java.util.List;

public abstract class Selectable extends Interaction {
	protected List<SelectableItem> items;
	protected Stimulus stimulus;
	public boolean alignForStimuli = true;

	protected String xmlName;
    
	protected Selectable() {
		items = new LinkedList<SelectableItem>();
	}
	
	protected Selectable(String headerLabel, String... itemLabels) {
		this();
        this.headerLabel = headerLabel;
		addItems(itemLabels);
	}
    
    protected abstract String getInstrumentXML();
    protected abstract String getValidationXML();

	public Stimulus getStimulus() {
		return stimulus;
	}

	public void setSelected(int index, boolean selected) throws IndexOutOfBoundsException {
		try {
			items.get(index).selected = selected;
		} catch (IndexOutOfBoundsException e) {
			String errMsg = String.format("Could not modify item %d: %s only has %d items.",
					index, toString(), items.size());
			throw new IndexOutOfBoundsException(errMsg);
		}
	}
	
	public void setItemID(int index, String id) throws IndexOutOfBoundsException {
		try {
			items.get(index).itemID = id;
		} catch (IndexOutOfBoundsException e) {
			String errMsg = String.format("Could not modify item %d: %s only has %d items.",
					index, toString(), items.size());
			throw new IndexOutOfBoundsException(errMsg);
		}
	}
	
    public String getXMLRepresentation() {
        if (!isValid()) {
            String msg = String.format("%s object is not in a valid state.", toString());   
			throw new IllegalStateException(msg);
        }
        
		String labelXML = XMLTag.tag("HeaderLabel", null, XMLTag.sanitize(headerLabel));
		String alignXML = XMLTag.tag("AlignForStimuli", null, parseBool(alignForStimuli));
        
		String itemXMLContent = "";
		for(SelectableItem item : items) {
			itemXMLContent += item.getXMLRepresentation();
		}

		String itemsXML = XMLTag.tag("Items", null, itemXMLContent);		
		
        String stimXML;
        if (stimulus == null)  {
            stimXML = XMLTag.tag("Stimulus", null, null);
        } else {
            stimXML = stimulus.getXMLRepresentation();
        }

		return XMLTag.tag(xmlName, XMLTag.attr("Version", "1"),
				XMLTag.tag("Inputs", null,
				  XMLTag.tag("Events", null, null) +
				  XMLTag.tag("Instrument", null,
					labelXML+
                    getInstrumentXML()+
					alignXML+
					itemsXML+
					stimXML
				  )
				)+
				XMLTag.tag("Outputs", null,
				    XMLTag.tag("Validation", null,
                            getValidationXML()+
                            genericOutEvents))
			  );
    }

    public void setStimulus(String... varargs) {
		switch(varargs.length) {
			case 2:
				stimulus = new Stimulus(varargs[0], varargs[1]);
				break;
			case 3:
				stimulus = new Stimulus(varargs[0], varargs[1], varargs[2]);
				break;
			default:
				throw new IllegalArgumentException("Method must be called with either 2 or 3 arguments.");
		}

	}
	public List<SelectableItem> getItems() {
		return items;
	}
	
	public SelectableItem getItem(int index) {
		return items.get(index);
	}
	
	public void removeItem(int index) throws IndexOutOfBoundsException {
		if (index < 0 || index >= items.size()) {
			throw new IndexOutOfBoundsException(String.format("Invalid index. There are currently %d item(s) defined.", items.size()));
		}
		
		items.remove(index);
		
		// Update label id's accordingly
		updatePostfix(index);
	}
	
	public SelectableItem addItem(String label) {
		return addItem(items.size(), label);
	}

	public SelectableItem addItem(int index, String label) {
		items.add(index, new SelectableItem(label));
		updatePostfix(index);
		return items.get(index);
	}
	
	public void addItems(String... labels) {
		for (String s : labels)
			addItem(s);
	}
	
	public void addItems(int startIndex, String... labels) {
		for (String s : labels)
			addItem(startIndex++, s);
	}

	private void updatePostfix(int startIndex) {
		for (int i = startIndex; i < items.size(); i++) {
            SelectableItem item = items.get(i);
            item.itemID = String.format("%d:%s", i, item.label);
		}
	}
	
	@Override
	public List<String> getErrorStrings() {
		List<String> errs = new LinkedList<String>();

		if (items == null || items.size() == 0)
			errs.add("no items added");

		return errs;
	}

	@Override
	public String getTextRepresentation() {
		String out = String.format("%s:\n", toString());
		if (stimulus != null) {
			out += stimulus.getTextRepresentation()+"\n";
		}
		for (SelectableItem item : items) {

			out += String.format("* %s\n", item.getTextRepresentation());
		}

		return out;
	}

	protected String getLabelString() {
		if (headerLabel != null && !headerLabel.isEmpty()) {
			return " ("+headerLabel+")";
		} else {
			return "";
		}
	}

	@Override
	public String getComponentType() {
		return xmlName;
	}

}
