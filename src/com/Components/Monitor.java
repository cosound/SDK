package com.Components;

import com.MetaComponents.ChaosEvent;
import com.Utilities.XMLTag;

import java.util.LinkedList;
import java.util.List;

public class Monitor extends Fixed {

	public String contextType, contextData;

	public String getXMLRepresentation() throws IllegalStateException {
		String dtVal = "(\\d{4}-\\d{2}-\\d{2})T(\\d{2}:\\d{2}:\\d{2}.\\d{3})Z";
		String validationContexts =
			XMLTag.tag("MultiValue",
				XMLTag.attr("Id", "Contexts", "Max", "Inf", "Min", "0"),
					XMLTag.tag("ComplexValue", XMLTag.attr("Id", "Context"),
						XMLTag.tag("SimpleValue",
							XMLTag.attr("Id", "DateTime", "Validation", dtVal), null) +
							XMLTag.tag("SimpleValue",
								XMLTag.attr("Id", "Type",  "Validation", ".*"), null) +
								XMLTag.tag("SimpleValue",
									XMLTag.attr("Id", "Data","Validation",".*"), null)
						)
				);

		return XMLTag.tag("Monitor", XMLTag.attr("Version", "1"),
				XMLTag.tag("Inputs", null,
						XMLTag.tag("Validation", null, null)+
						XMLTag.tag("Events", null, null)
				) +
				XMLTag.tag("Outputs", null,
				XMLTag.tag("Validation", null,
					genericOutEvents+
					validationContexts
				) +
				XMLTag.tag("Value", null,
					ChaosEvent.getEventsXMLRepresentation(outputEvents)+
                    XMLTag.tag("Context", null,
                        XMLTag.tag("Type", null, XMLTag.sanitize(contextType))+
                        XMLTag.tag("Data", null, XMLTag.sanitize(contextData))
                    )
                )
			));
	}

	@Override
	public List<String> getErrorStrings() {
		return new LinkedList<String>();
	}
	
	@Override
	public String getStateReport() {
		return "";
	}

	@Override
	public Monitor clone() {
	    return new Monitor();
    }

	@Override
	public String getComponentType() {
		return "Monitor";
	}

	@Override
	public String getHeaderLabel() {
		return "(none)";
	}
}
