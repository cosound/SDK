package com.Components;

import com.Interfaces.XMLConvertable;
import com.Utilities.XMLTag;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by kirksmacbookair on 05/10/2016.
 */
public abstract class Tagging extends Interaction {

    public class Tag implements XMLConvertable {
        public final String label;
        public final int position;
        public final String id;

        public Tag(String label, int position, String id) throws IllegalArgumentException {
            if (position < 0) {
                throw new IllegalArgumentException("Position argument must be larger than 0.");
            }
            this.label = label;
            this.position = position;
            this.id = id;
        }


        @Override
        public String getXMLRepresentation() throws IllegalStateException {
            return XMLTag.tag("Item", null,
                    XMLTag.tag("Id", null, id)+
                    XMLTag.tag("Label", null, label)+
                    XMLTag.tag("Position", null, ""+position));
        }
    }

    public String selectionTagBoxLabel;
    public String userTagBoxLabel;
    public String textField;
    public Stimulus stimulus;
    public List<Tag> selectionTags = new LinkedList<>();
    public List<Tag> userTags = new LinkedList<>();

    public void addSelectionTag(String label) {
        addSelectionTag(label, selectionTags.size()+1);
    }

    public void addSelectionTag(String label, int position) {
        addSelectionTag(label, position, ""+position);
    }

    public void addSelectionTag(String label, int position, String id) {
        selectionTags.add(new Tag(label, position, id));
    }

    public void addUserTag(String label) {
        addUserTag(label, userTags.size()+1);
    }

    public void addUserTag(String label, int position) {
        addUserTag(label, position, ""+position);
    }

    public void addUserTag(String label, int position, String id) {
        userTags.add(new Tag(label, position, id));
    }


    public void setStimulus(String... varargs) {
        switch (varargs.length) {
            case 2:
                stimulus = new Stimulus(varargs[0], varargs[1]);
                break;
            case 3:
                stimulus = new Stimulus(varargs[0], varargs[1], varargs[2]);
                break;
            default:
                throw new IllegalArgumentException("Method must be called with either 2 or 3 arguments.");
        }
    }

    public String getXMLRepresentation() {
        String selectionTagsXML = "";
        for (Tag tag : selectionTags) {
            selectionTagsXML += tag.getXMLRepresentation();
        }

        String userTagsXML = "";
        for (Tag tag : userTags) {
            userTagsXML += tag.getXMLRepresentation();
        }

        String stimXML;
        if (stimulus == null)  {
            stimXML = XMLTag.tag("Stimulus", null, null);
        } else {
            stimXML = stimulus.getXMLRepresentation();
        }

        return XMLTag.tag(getComponentType(), XMLTag.attr("Version", "1"),
                XMLTag.tag("Inputs", null,
                        XMLTag.tag("Events", null, null)+
                                XMLTag.tag("Instrument", null,
                                        XMLTag.tag("HeaderLabel", null, XMLTag.sanitize(headerLabel))+
                                                XMLTag.tag("SelectionTagBoxLabel", null, XMLTag.sanitize(selectionTagBoxLabel))+
                                                XMLTag.tag("UserTagBoxLabel", null, XMLTag.sanitize(userTagBoxLabel))+
                                                XMLTag.tag("TextField", null, XMLTag.sanitize(textField))+
                                                XMLTag.tag("SelectionTags", null, selectionTagsXML)+
                                                XMLTag.tag("UserTags", null, userTagsXML)+
                                                stimXML
                                )
                )+
                        XMLTag.tag("Outputs", null,
                                XMLTag.tag("Validation", null, null)+
                                        XMLTag.tag("Value", null, null)
                        )
        );
    }

    public String getTextRepresentation() {
        return "";
    }

    public List<String> getErrorStrings() {
        return new LinkedList<>();
    }

    public String getStateReport() {
        List<String> errs = getErrorStrings();
        return concatenateErrorReports(errs);
    }
}
