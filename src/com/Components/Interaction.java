package com.Components;

import com.MetaComponents.ChaosEvent;
import com.Utilities.XMLTag;

public abstract class Interaction extends TrialComponent {
	
	public String headerLabel;

	@Override
	public String getHeaderLabel() {
		return headerLabel;
	}

	protected String getEventsXML() {
		String eventsXML = "";
		for (ChaosEvent event : outputEvents) {
			eventsXML += event.getXMLRepresentation();
		}
		return XMLTag.tag("Events", null, eventsXML);
	}
}
