package com.Components;

import com.Utilities.XMLTag;

import java.util.LinkedList;
import java.util.List;


public class TextBlock extends Fixed {
	public String text;

	public TextBlock() {
		this.text = "";
	}

	public TextBlock(String text) {
		this.text = text;
	}

	public String getXMLRepresentation() throws IllegalStateException {
		if (!isValid())
			throw new IllegalStateException("TextBlock object is not in a valid state.");

		return XMLTag.tag("TextBlock", XMLTag.attr("Version", "1"),
				XMLTag.tag("Inputs", null,
						XMLTag.tag("Events", null, null) +
								XMLTag.tag("Instrument", null,
										XMLTag.tag("Text", null, XMLTag.sanitize(text))
								)
				) +
						XMLTag.tag("Outputs", null,
								XMLTag.tag("Validation", null,
										genericOutEvents
								)
						)
		);
	}

	@Override
	public List<String> getErrorStrings() {
		List<String> errs = new LinkedList<>();

		if (text == null || text.length() == 0)
			errs.add("label text is empty");

		return errs;
	}

	@Override
	public String getComponentType() {
		return "TextBlock";
	}

	@Override
	public String getHeaderLabel() {
		return text;
	}

	@Override
	public String getStateReport() {
		return concatenateErrorReports(getErrorStrings());
	}

	@Override
	public String toString() {
		return String.format("TextBlock (\"%s\")", this.text);
	}

}
