package com.Components;

import com.Utilities.XMLTag;

import java.util.*;

/**
 * Created by kirksmacbookair on 21/08/2016.
 */
public class ListSelect extends Selectable {

    public int minNoOfSelections = 1;
    public int maxNoOfSelections = 1;

    public ListSelect() {
        super();
        this.xmlName = "ListSelect";
    }

    public ListSelect(String headerLabel, String... itemLabels) {
        this();
        addItems(itemLabels);
        this.headerLabel = headerLabel;
        minNoOfSelections = 1;
        maxNoOfSelections = itemLabels.length;
    }

    @Override
    protected String getInstrumentXML() {
        String minXML = XMLTag.tag("MinNoOfSelections", null,
                String.valueOf(minNoOfSelections));
        String maxXML = XMLTag.tag("MaxNoOfSelections", null,
                String.valueOf(maxNoOfSelections));
        return minXML+maxXML;
    }

    @Override
    protected String getValidationXML() {
        String attr = XMLTag.attr("Id", "Selections",
                "Max", String.valueOf(maxNoOfSelections),
                "Min", String.valueOf(minNoOfSelections));
        return XMLTag.tag("MultiValue", attr,
                XMLTag.tag("SimpleValue",
                        XMLTag.attr("Id", "Id", "Validation", ".*"),
                        null)
        );
    }

    @Override
    public String getStateReport() {
        List<String> errs = super.getErrorStrings();

        errs.addAll(getErrorStrings());

        return concatenateErrorReports(errs);
    }

    @Override
    public List<String> getErrorStrings() {
        List<String> errs = new LinkedList<String>();

        if (minNoOfSelections < 0 || minNoOfSelections >= items.size())
            errs.add("invalid value for minNoOfScaligns");

        if (maxNoOfSelections <= 0)
            errs.add("invalid value for maxNoOfScaligns");

        if (minNoOfSelections > maxNoOfSelections)
            errs.add("minNoOfScalings larger than maxNoOfScaligns");

        return errs;
    }

    @Override
    public String toString() {
        return "ListSelect" + getLabelString();
    }

    @Override
    public ListSelect clone() {
        ListSelect ls = new ListSelect(headerLabel);
        ls.minNoOfSelections = minNoOfSelections;
        ls.maxNoOfSelections = maxNoOfSelections;
        for (int i = 0; i < items.size(); i++) {
            ls.addItem(items.get(i).label);
            ls.setSelected(i, items.get(i).selected);
        }
        return ls;
    }

}
