package com.Components;

import com.Utilities.XMLTag;

import java.util.LinkedList;
import java.util.List;


public class Header extends Fixed {
	
	public String headerLabel;

	public Header() {
		this.headerLabel = "";
	}
	public Header(String headerLabel) {
		this.headerLabel = headerLabel;
	}
	
	public String getXMLRepresentation() throws IllegalStateException {
		if (!isValid())
			throw new IllegalStateException("Header object is not in a valid state.");
		
		return XMLTag.tag("Header", XMLTag.attr("Version", "1"),
				XMLTag.tag("Inputs", null,
				  XMLTag.tag("HeaderLabel", null, XMLTag.sanitize(headerLabel))
				)+
				XMLTag.tag("Outputs", null,
				    XMLTag.tag("Validation", null, null)+
				    XMLTag.tag("Value", null, null)
				)
			  );
	}

	@Override
	public List<String> getErrorStrings() {
		List<String> errs = new LinkedList<String>();
		
		if (headerLabel == null || headerLabel.length() == 0)
			errs.add("label text is empty");
		
		return errs;
	}


	@Override
	public String getStateReport() {
		return concatenateErrorReports(getErrorStrings());
	}

	@Override
	public String toString() {
		return String.format("Header (\"%s\")", this.headerLabel);
	}

	@Override
	public String getHeaderLabel() {
		return headerLabel;
	}

	@Override
	public String getComponentType() {
		return "Header";
	}
}
