package com.Interfaces;

public interface XMLConvertable  {
	
	public String getXMLRepresentation() throws IllegalStateException;

}
