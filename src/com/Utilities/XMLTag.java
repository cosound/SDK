package com.Utilities;

public class XMLTag {
	private XMLTag() {
		// Prevent initialization
	}
	
	public static <T> T[] array(T... elems) {
		return elems;
	}
	
	public static String attr(String... attrTuples) throws ArithmeticException {
		if (attrTuples.length > 0 && attrTuples.length % 2 != 0) {
			throw new ArithmeticException(); // TODO: Wrong exception
		}
		
		String[] attrConcat = new String[attrTuples.length/2];
		for(int i = 0; i < attrConcat.length; i++) {
			attrConcat[i] = attrTuples[i*2]+"=\""+sanitize(attrTuples[i*2+1])+"\"";
		}
		return " "+StringJoin(" ", attrConcat);
	}

	public static String sanitize(String input) {
        if (input == null) {
            return null;
        } else {
			/*
			// XML escaping
            return input.replaceAll("&", "&amp;")
                        .replaceAll("\"", "&quot;")
                        .replaceAll("'", "&apos;")
                        .replaceAll("<", "&lt;")
                        .replaceAll(">", "&gt;")
						.replaceAll("&", "%26")
						.replaceAll("%", "%25")
						.replaceAll("\\+", "%2B"); */
			// UTF8 escaping
			return input.replaceAll("%", "%25")
						.replaceAll("\"", "%22")
						.replaceAll("'", "%27")
						.replaceAll("<", "%3c")
						.replaceAll(">", "%3e")
						.replaceAll("&", "&amp;")
						.replaceAll("&", "%26")
						.replaceAll("\\+", "%2B");
        }
    }

    public static String desanitize(String input) {
		// UTF8 escaping
		return input.replaceAll("%25", "%")
				.replaceAll("%22", "\"")
				.replaceAll("%27", "'")
				.replaceAll("%3c", "<")
				.replaceAll("%3e", ">")
				.replaceAll("%26", "&")
				.replaceAll("&amp;", "&")
				.replaceAll("%2B", "\\+");

	}
	
	public static String tag(String tagName, String attributes, String content) {	
		attributes = attributes == null ? "" : attributes;
		if(content == null || content.isEmpty()) {
			return "<"+tagName+attributes+"/>";
		}
		
		return "<"+tagName+attributes+">"+content+"</"+tagName+">";
	}
	
	private static String StringJoin(String joint, String[] elms) {
		String out = "";
		if (elms.length == 0)
			return out;
		
		for (int i = 0; i < elms.length-1; i++) {
			out += elms[i] + joint;
		}
		return out+elms[elms.length-1];
	}

}
