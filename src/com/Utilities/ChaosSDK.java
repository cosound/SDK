package com.Utilities;

import com.CockpitExceptions.*;
import com.Components.TrialComponent;
import com.MetaComponents.Answer;
import com.MetaComponents.Experiment;
import com.MetaComponents.ExperimentList;
import com.MetaComponents.Trial;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.*;

public class ChaosSDK {
	private String apiKey; 		// private key, used for logging in
	private String sessionGUID; // session specific token
    private boolean dev = true;

    // Constants
    private static int idCreationRetrys = 10;
	private static final String charset = java.nio.charset.StandardCharsets.UTF_8.name();
	private static final String expFolderID = "99999775";
    private static final String expListFolderID = "99999776";
    private static final String expObjectTypeID = "775";
	private static final String expListObjectTypeID = "776";
	private static final String expMetaSchemaGUID = "ffffffff-ffff-ffff-ffff-fff775000001";
    private static final String expListMetaSchemaGUID = "00000000000000000000000000000111";
    private static final String languageCode = "dk";
	
	public static void main(String[] args) throws Exception {
        String prodkey = new String(Files.readAllBytes(Paths.get("matlab_examples/prodkey")));
        String devkey = new String(Files.readAllBytes(Paths.get("matlab_examples/debugkey")));

        ChaosSDK sdk = new ChaosSDK(prodkey, "prod");
//        sdk.reindexDatabase();
        List<List<Answer>> trials = sdk.getExperimentAnswers("a9f56a58-aaaa-eeee-1355-aa435ed70050");

//        Experiment retrievedExp = sdk.getExperiment(exp.id);
        Experiment retrievedExp = sdk.getExperiment("ec40d15c-68fe-4968-bbc4-bff17efa0591");


//        Experiment retrievedExp = sdk.getExperiment("ec40d15c-68fe-4968-bbc4-bff17efa0591");
        String xml = retrievedExp.getXMLRepresentation();
        System.out.println("");


//        try {
//            sdk.commitExperimentList(expList);
//        } catch (CockpitException e) {
//            System.out.println("Exception while uploading list.");
//            throw e;
//        } finally {
//            System.out.println("Done.");
//            for (Experiment exp : expList.getExperiments()) {
//                try { sdk.deleteObject(exp.id); } catch (Exception ignored) {}
//            }
//            try { sdk.deleteObject(expList.id); } catch (Exception ignored) {}
//        }
    }

	public ChaosSDK(String apiKey) throws CockpitException {
		this(apiKey, "dev");
	}
    
    public ChaosSDK(String apiKey, String env) throws CockpitException {
        if ("dev".equals(env)) {
            this.dev = true;
        } else if ("prod".equals(env)) {
            this.dev = false;
        } else {
            throw new IllegalArgumentException("Environment argument must " +
                    "be either 'prod' or 'dev'.");
        }
        this.apiKey = apiKey;
		login();
    }
	
	private static class ParamMap extends HashMap<String, String> {
		/**
		 * 
		 */
		private static final long serialVersionUID = -2343456834544464827L;

		public ParamMap(int capacity) {
			super(capacity);
		}
		
		public ParamMap(String... params) throws IllegalArgumentException {
			this(params.length >> 1);
			if (params.length % 2 != 0) {
				throw new IllegalArgumentException("Parameter list must be of even length.");
			}
			
	        for (int i = 0; i < params.length; i += 2)
	        	put(params[i], params[i+1]);
		}
		
		public String getRequestString() {
			String requestString = new String();
			for (String key : this.keySet()) {
				String value = this.get(key);
				requestString += "&"+key+"="+value;
			}
			
			// Skip first "&"-character
			return requestString.substring(1);
		}
	}
	
	private static class ConnectionState {
		public final String sessionGuid;
		public final String userGuid;
		public ConnectionState(String sessionGuid, String userGuid) {
			this.sessionGuid = sessionGuid;
			this.userGuid = userGuid;
		}
	}

	private static final String devBaseURL = "https://dev-api.cosound.dk/v6/";
	private static final String prodBaseURL = "https://prod-api.cosound.dk/v6/";
	
	/**
	 * Send request and return XML in byte array.
	 * 
	 */
	private String getXML(String cmdPath, String... params)
            throws TimeoutException, ConnectionError
    {
        String baseURL = dev ? devBaseURL : prodBaseURL;
        String url = baseURL+cmdPath;
        String query = new ParamMap(params).getRequestString();
        
        // Open connection with 10s connection timeout and 20m read timeout
		URLConnection connection;
		try {
			connection = new URL(url).openConnection();
			connection.setConnectTimeout(10000);
			connection.setReadTimeout(1200000);
		} catch (IOException e) {
			throw new TimeoutException("Could not connect to Chaos server: " +
                    e.getMessage());
		}

		connection.setDoOutput(true); // Triggers POST.
		connection.setRequestProperty("Accept-Charset", charset);
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);

		try (OutputStream output = connection.getOutputStream()) {
		    output.write(query.getBytes(charset));
		} catch (IOException e) {
			throw new ConnectionError("Error while contacting CHAOS server: " +
            e.getMessage());
		}
		
		String returnString = "";
		try {
		    InputStreamReader isr =
                    new InputStreamReader(connection.getInputStream(), "UTF-8");
            BufferedReader reader = new BufferedReader(isr);
		    for (String line; (line = reader.readLine()) != null;) {
		        returnString += line;
		    }
		} catch (SocketTimeoutException e) {
			throw new TimeoutException("Timeout occurred while waiting for " +
                    "response from CHAOS server.");
		} catch (IOException e) {
		    throw new ConnectionError("Unknown error occurred while reading " +
                    "response from CHAOS-server: " + e.getMessage());
        }

        // DEBUGGING: log response to file
        int start = returnString.indexOf('<');
        if (start > 0) {
            returnString = returnString.substring(start);
        }

		return returnString;
	}
	
	
	// Might be to expensive to re-instantiate DocumentBuilder-object on every call
    // TODO move to XML-specific class
	private static String getValueForKey(byte[] charBuf, String key) {
		if (charBuf.length == 0)
			return null;
		
		// Remove preceding illegal characters
		int n = 0;
		for (; (char) charBuf[n] != '<'; n++);
		
		byte[] cleanCharBuf = new byte[charBuf.length-n];
		for (int i = n; i < charBuf.length; i++) {
			cleanCharBuf[i-n] = charBuf[i];
		}
		
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		Document doc;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			InputSource is =
                    new InputSource(new ByteArrayInputStream(cleanCharBuf));
			is.setEncoding(charset);
			doc = dBuilder.parse(is);
			doc.getDocumentElement().normalize();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new IllegalStateException();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new IllegalStateException();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new IllegalStateException();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new IllegalStateException();
		}
		
		// FIXME null-pointer exception will be thrown if the tags are not
        //       found. Surround with try-catch or check explicitly.
		
		try {
			return doc.getElementsByTagName(key).item(0).getTextContent();
		} catch (NullPointerException e) {
			return null;
		}
	}
	
	/**
	 * Acquire session guid and user guid wrapped in ConnectionState-object.
	 * 
	 * @return
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	private void login() throws CockpitException
    {
        String xml = getXML("AuthKey/Login",
                "format", "xml2",
                "token", apiKey);

        CockpitException err = getError(xml);
        if (err != null) {
            throw err;
        }

        Node n = XMLParser.convertToNode(xml);
        sessionGUID = XMLParser.secureGetTextContent(n, "PortalResponse", "Body",
                "Results", "Result", "Guid");
	}

    private static void writeXMLToFile(byte[] byteBuf, String filename) {
        try (PrintWriter out = new PrintWriter(filename)){
            out.println(new String(byteBuf));
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }
    
    private static void appendToFile(String str, String filename) {
        try (PrintWriter out = new PrintWriter(new FileOutputStream(
                new File(filename), true /* append = true */)))
        {
            out.append(str+"\n");
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

	/**
	 * Retrieve and parse experiment XML, and return Java-representation.
	 * 
	 * @param expId
	 * @return
	 * @throws CockpitException
	 */
	public Experiment getExperiment(String expId)
            throws CockpitException
    {
        Node topNode = XMLParser.convertToNode(getExperimentXML(expId));
       	Experiment exp = XMLParser.parseXML(topNode);
        exp.revisionID = 1;
        return exp;
	}

	public List<List<Answer>> getExperimentAnswers(String expId) throws CockpitException {
	    Experiment exp = getExperiment(expId);
	    List<List<Answer>> trialList = new LinkedList<>();

        for (Trial trial : exp.getTrials()) {
            List<Answer> answerList = new LinkedList<>();
            for (TrialComponent component : trial.getComponents()) {
                answerList.add(new Answer(component));
            }
            trialList.add(answerList);
        }

        return trialList;
    }

	public String getExperimentXML(String expId) throws CockpitException {
        String xml =
                getXML("View/Get",
                        "view", "Object",
                        "query", "Id:" + expId,
                        "objectTypeID", expObjectTypeID, // FIXME not only exp
                        "sort", "DateCreated desc",
                        "format", "xml2",
                        "pageSize", "1",
                        "pageIndex", "0",
                        "includeMetadata", "True",
                        "includeFiles", "True",
                        "includeAccessPoints" , "True",
                        "includeObjectRelations", "True",
                        "includeFolders" , "True",
                        "sessionGUID", sessionGUID);

        // Check error
        Node docNode = XMLParser.convertToNode(xml);
        Node countNode = XMLParser.findChildNode(docNode, "PortalResponse",
                "Body", "Count");
        if (countNode == null) {
            // FIXME Wrong exception
            throw new ExperimentParsingException("No count-field in search " +
                    "response from CHAOS-server.");
        }
        else if (Integer.parseInt(countNode.getTextContent()) == 0) {
            // if search returns empty, throw exception
            String msg = String.format("Object with id '%s' does not exist on " +
                    "server.", expId);
            throw new ObjectNotFoundException(msg);
        }

        Node meta = XMLParser.findChildNode(docNode, "PortalResponse", "Body",
                "Results", "Result", "Metadatas", "Result", "MetadataXml");
        if (meta == null) {
            throw new ExperimentParsingException("No metadata-tag in server " +
                    "response.");
        }

        return meta.getTextContent();
    }

	public static Experiment loadExperimentFromFile(String path)
            throws IOException, XMLParsingException, ExperimentParsingException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        String expXML = new String(encoded, charset);
        Node expNode = XMLParser.convertToNode(expXML);
        return XMLParser.parseXML(expNode);
    }

    public static void writeExperimentToFile(Experiment experiment, String path)
            throws IOException
    {
        Files.write(Paths.get(path), (experiment.getXMLRepresentation()+"\n").getBytes(),
                StandardOpenOption.CREATE);
    }

    private static CockpitException getError(String byteBuf) {
        if (byteBuf == null || byteBuf.equals("")) {
            return null;
        }

        try {
            Node docNode = XMLParser.convertToNode(byteBuf);
            Node errNode = XMLParser.findChildNode(docNode,
                    "PortalResponse",  "Error");
            if (errNode == null) {
                return null;
            }

            NamedNodeMap errAttr = errNode.getAttributes();
            Node errNameNode = errAttr.getNamedItem("Fullname");
            if (errNameNode == null) {
                return null;
            }

            // Append to error log
            System.out.println("Writing to log...");
            try (PrintWriter out = new PrintWriter("errorlog")){
                String time = (new SimpleDateFormat("dd/mm hh-mm-ss")).format(new Date());
                out.append("\n"+time+"\n");
                out.append(byteBuf);
                out.append("\n\n");
                out.close();
            } catch (FileNotFoundException ignored) {
                System.out.println("Could not write to log: "+ignored.getMessage());
            }
            System.out.println("Done.");

            switch (errNameNode.getTextContent()) {
                // TODO
                case "System.NullReferenceException":
                    return new ObjectNotFoundException("Object was not found.");
                case "Chaos.Portal.Core.Exceptions.InsufficientPermissions" +
                        "Exception":
                    return new OperationNotPermittedException("CHAOS server " +
                            "reported insufficient priviliges. This is " +
                            "sometimes indicative of serverside bug so you " +
                            "might want to try again.");
                case "System.Collections.Generic.KeyNotFoundException":
                    return new OperationNotPermittedException("Unknown key in" +
                            " request to CHAOS server. This is sometimes " +
                            "indicative of serverside bug so you might want " +
                            "to try again.");
                case "Chaos.Mcm.Exception.InvalidRevisionException":
                    return new CommitNotAcceptedException("Experiment was not" +
                            " accepted by CHAOS server due to invalid " +
                            "revision ID. This sometimes occurs if the " +
                            "has not been deleted and/or the database " +
                            "reindexed prior to the commit.");
                default: {
                    System.out.printf("Uncaught exception: '%s'\n",
                            errNameNode.getTextContent());
                    String msg = "Generic exception (to be" +
                            " implemented. Please notify author on " +
                            "akuhren@gmail.com).";
                    return new OperationNotPermittedException(msg);
                }
            }


        } catch(XMLParsingException e) {
            return new XMLParsingException("Could not parse XML from " +
                    "Cockpit-server.");
        }
    }
	
    private String getNHex(int n) {
    	String s = "";
    	Random rand = new Random();
    	for (int i = 0; i < n; i++) {
    		s += Integer.toHexString(rand.nextInt(16));
    	}
    	return s;
    }

    public String getRandomID() throws CockpitException {
        // Create random ID and check if it already exists
    	for (int i = 0; i < idCreationRetrys; i++) {
    		String randID = String.format("%s-%s-%s-%s-%s",
    				getNHex(8), getNHex(4), getNHex(4),
    				getNHex(4), getNHex(12));
            try {
                getExperiment(randID);
            } catch (ObjectNotFoundException e) {
    			return randID;
            }
    	}
    	throw new RuntimeException("Could not generate unique GUID (something "+
                "is wrong in implementation of ChaosSDK.getRandomID()");
    }

    private void createObject(String guid, String objectTypeID, String folderID)
            throws CockpitException
    {
        String xml =
                getXML("Object/Create",
                        "guid", guid,
                        "objectTypeID", objectTypeID,
                        "folderID", folderID,
                        "format", "xml2",
                        "sessionGUID", sessionGUID);

        CockpitException err = getError(xml);
        if (err != null) {
            throw err;
        }

    }

    private void setMetaData(String objectGuid, String metaDataSchemaGUID, String metaData)
            throws CockpitException
    {
        String xml =
                getXML("Metadata/Set",
                        "format", "xml2",
                        "objectGuid", objectGuid,
                        "metadataSchemaGuid", metaDataSchemaGUID,
                        "metadataXml", metaData,
                        "revisionID", "1",
                        "sessionGUID", sessionGUID);

        CockpitException err = getError(xml);
        if (err != null) {
            throw err;
        }
    }

    public ExperimentList commitExperimentList(ExperimentList list) throws CockpitException
    {
        // If no GUID is specified we create a new one
        if (list.id == null || list.id.equals("")) {
            list.id = getRandomID();
        } else {
            try {deleteObject(list.id); } catch (Exception ignored) {}
        }

        System.out.println("Creating list object...");
        createObject(list.id, expListObjectTypeID, expListFolderID);
        System.out.println("Done.");

        Experiment[] exps = list.getExperiments();
        for (int i = 0; i < exps.length; i++) {
            Experiment exp = exps[i];
            System.out.println(String.format("Committing experiment %d/%d...",
                    i, exps.length));
            try {
                commitExperiment(exp);
            } catch (ExperimentAlreadyExistsException e) {
                System.out.println("Already exists, continuing.");
                continue;
            } catch (OperationNotPermittedException e) {
                String msg = String.format("Could not commit experiment %d due"+
                        "to insufficient privileges.", i);
                System.out.println(msg);
                throw e;
            }  catch (CockpitException e) {
                // FIXME
                continue;
            }
            System.out.println("Done.");
        }

        System.out.println("Setting metadata...");
        setMetaData(list.id, expListMetaSchemaGUID, list.getXMLRepresentation());
        System.out.println("Done.");

        // Print URL
        String tok = dev ? "test" : "experiments";
        System.out.printf(
                "Experiment list available on:\nhttps://cockpit.cosound.dk/%s/#ExperimentList/%s\n",
                tok, list.id);

        return list;
    }

    public Experiment commitExperiment(Experiment exp)
            throws CockpitException
    {
		// Check that experiment is  valid
		String stateReport = exp.getStateReport();
		if (!stateReport.isEmpty()) {
			throw new InvalidExperimentException("Experiment is not in valid " +
                    "state:\n" + stateReport);
		}

        exp.revisionID = 1;

		// If no GUID is specified we create a new one
		if (exp.id == null || exp.id.isEmpty()) {
            System.out.println("Creating random ID...");
			exp.id = getRandomID();
		} else {
            try {deleteObject(exp.id); } catch (Exception ignored) {}
        }

        // Create empty experiment object
        System.out.println("Creating empty experiment object in DB...");
        createObject(exp.id, expObjectTypeID, expFolderID);

		// If target name/id is not set explicitly, we set it equal to name/id
		if (exp.targetName == null || exp.targetName.isEmpty())
			exp.targetName = exp.name;
		if (exp.targetId == null || exp.targetId.isEmpty())
			exp.targetId= exp.id;

        // Set experiment meta data
        System.out.println("Setting metadata...");
        setMetaData(exp.id, expMetaSchemaGUID, exp.getXMLRepresentation());

        // Print URL
        String tok = dev ? "test" : "experiments";
        System.out.printf("Experiment available on:\n" +
                        "https://cockpit.cosound.dk/%s/#Experiment/%s\n",
                tok, exp.id);


        tok = dev ? "dev-api" : "prod-api";
        System.out.println("\nJSON representation available on:");
        System.out.printf("https://%s.cosound.dk/v6/Question/Get?id=%s"+
                        "&index=0&format=json3&userHTTPStatusCodes=False\n",
                tok, exp.id);

        return exp;
    }

//    /** DEBUG METHOD **/
	public void commitExperimentFromXMLFile(String xmlFile, String id)
            throws IOException, CockpitException {
		byte[] encoded = Files.readAllBytes(Paths.get(xmlFile));
		String expXML = new String(encoded, charset);

        // Create empty object if experiment is new
        System.out.println("Creating empty experiment object in DB...");
        String byteBuf =
                getXML("Object/Create",
                        "guid", id,
                        "objectTypeID",expObjectTypeID,
                        "folderID", expFolderID,
                        "format","xml2",
                        "sessionGUID",sessionGUID);

        CockpitException err = getError(byteBuf);
        if (err != null) {
            String errMsg = String.format("Could not create experiment object: %s", err);
            throw new IOException(errMsg);
        }

        // Set experiment meta data
        System.out.println("Setting metadata...");
        byteBuf =
           	getXML("Metadata/Set",
            	   "format", "xml2",
                   "objectGuid", id,
                   "metadataSchemaGuid", expMetaSchemaGUID,
                   "revisionID", "1",
                   "metadataXml", expXML,
                   "sessionGUID", sessionGUID);

        err = getError(byteBuf);
        if (err != null) {
            String errMsg = String.format("Could not set experiment metadata: %s", err);
            throw new IOException(errMsg);
        }

        // Print URLs
        String tok = dev ? "test" : "experiments";
        System.out.printf("Experiment available on:\n" +
                        "https://cockpit.cosound.dk/%s/#Experiment/%s\n",
                tok, id);

        tok = dev ? "dev-api" : "prod-api";
        System.out.printf("https://%s.cosound.dk/v6/Question/Get?id=%s"+
                        "&index=0&format=json3&userHTTPStatusCodes=False\n",
                tok, id);
	}
    
    public void deleteObject(String expID)
            throws CockpitException
    {
        String xml =
            getXML("Object/Delete",
                   "guid", expID,
                   "format", "xml2",
                   "sessionGUID", sessionGUID);
        CockpitException err = getError(xml);
        if (err != null) {
            throw err;
        }

    }

    public void reindexDatabase()
            throws CockpitException
    {
        System.out.println("Re-indexing database (might take awhile)...");
        String xml =
                getXML("Mcm/Index",
                        "format", "xml2",
                        "view", "Object",
                        "cleanIndex", "True",
                        "sessionGUID", sessionGUID);
        CockpitException err = getError(xml);
        if (err != null) {
            throw err;
        }
        System.out.println("Done.");
    }
}
 