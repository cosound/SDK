package com.Utilities;

import com.CockpitExceptions.ExperimentParsingException;
import com.CockpitExceptions.XMLParsingException;
import com.Components.*;
import com.MetaComponents.ChaosEvent;
import com.MetaComponents.Experiment;
import com.MetaComponents.ExperimentList;
import com.MetaComponents.Trial;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import javax.activity.InvalidActivityException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class XMLParser {
	

	public static Node convertToNode(String xml) throws XMLParsingException {
		String str = new String(xml);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		Document doc;
        try {
			dbFactory.setIgnoringElementContentWhitespace(true);
			dbFactory.setIgnoringComments(true);
			dBuilder = dbFactory.newDocumentBuilder();
			InputSource is = new InputSource();
            byte[] byteStream = xml.getBytes(Charset.forName("UTF-8"));
            is.setByteStream(new ByteArrayInputStream(byteStream));
            is.setEncoding("UTF-8");
			doc = dBuilder.parse(is);
			doc.getDocumentElement().normalize();
			return doc;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			String msg = String.format("Could not parse XML while " +
					"converting to SAX node. XML:\n%s\n", xml);
			throw new XMLParsingException(msg);
		}
	}

	public static ExperimentList parseExperimentList(Node topNode)
		throws ExperimentParsingException
	{
        throw new ExperimentParsingException("Method \"parseExperimentList\" not imlemented.");
//		com.MetaComponents.ExperimentList expList = new ExperimentList();
//
//		if (!topNode.getNodeName().equals("Experiments")) {
//			throw new ExperimentParsingException("Meta-data for " +
//					"experiment list is invalid.");
//		}
//
//		try {
//			for (Node itemNode = topNode.getFirstChild();
//				 itemNode != null;
//				 itemNode = secureGetNextSibling(itemNode))
//			{
//
//			}
//		}
	}

	public static Experiment parseXML(Node topNode)
            throws ExperimentParsingException
    {
        Experiment exp = new Experiment();

        Node expNode = findChildNode(topNode, "Experiment");
        if (expNode == null) {
            throw new ExperimentParsingException("No \"Experiment\"-tag in XML.");
        }

        // Fields that *must* be present
        exp.id = secureGetTextContent(expNode, "Id");
        exp.name = secureGetTextContent(expNode, "Name");
        exp.version = secureGetTextContent(expNode, "Version");

        // Fields that *may* be present
        try {
            exp.lockQuestion = secureGetTextContent(expNode, "LockQuestion").equals("1");
        } catch (ExperimentParsingException e){}

        try {
            exp.enablePrevious = secureGetTextContent(expNode, "EnablePrevious").equals("1");
        } catch (ExperimentParsingException e){}

        try {
            exp.redirectOnCloseUrl = secureGetTextContent(expNode, "RedirectOnCloseUrl");
        } catch (ExperimentParsingException e){}

        try {
            exp.footerLabel = secureGetTextContent(expNode, "FooterLabel");
        } catch (ExperimentParsingException e){}

        try {
            exp.createdBy = secureGetTextContent(expNode, "CreatedBy");
        } catch (ExperimentParsingException e){}

        try {
            exp.experimentDescription = secureGetTextContent(expNode, "ExperimentDescription");
        } catch (ExperimentParsingException e){}


        Node targetNode = findChildNode(expNode, "Target");
        if (targetNode != null) {
            NamedNodeMap expAttr = targetNode.getAttributes();
            exp.targetId = secureGetTextContent(expAttr.getNamedItem("Id"));
            exp.targetName = secureGetTextContent(expAttr.getNamedItem("Name"));
        }

        // Traverse trials
        for (Node trialNode = secureGetFirstChild(findChildNode(expNode, "Trials"));
             trialNode != null;
             trialNode = secureGetNextSibling(trialNode))
        {
            Trial trial = exp.createTrial();

    NamedNodeMap trialAttr = trialNode.getAttributes();

            if (trialAttr != null) {
                Node taskIdNode = trialAttr.getNamedItem("TaskId");
                if (taskIdNode != null)
                    trial.taskId = secureGetTextContent(taskIdNode);

                Node isClosedNode = trialAttr.getNamedItem("IsClosed");
                if (isClosedNode != null)
                    trial.isClosed = secureGetTextContent(isClosedNode).equals("true");
            }


            // Traverse components
            for (Node componentNode = secureGetFirstChild(trialNode);
                 componentNode != null;
                 componentNode = secureGetNextSibling(componentNode))
            {
                TrialComponent newComp = null;
                switch (componentNode.getNodeName()) {
                    case "CheckBoxGroup":
                    case "CheckBoxGroup1":
                        newComp = parseCheckBoxGroupNode(componentNode);
                        break;
                    case "Freetext":
                    case "Freetext_1":
                    case "Freetext_2":
                        newComp = parseFreeTextNode(componentNode);
                        break;
                    case "FreetextHash":
                        newComp = parseFreeTextHashNode(componentNode);
                        break;
                    case "Header":
                        newComp = parseHeaderNode(componentNode);
                        break;
                    case "LikertScale":
                    case "LikertScale_1":
                    case "LikertScale_2":
                        newComp = parseLikertScaleNode(componentNode);
                        break;
                    case "Monitor":
                        newComp = parseMonitorNode(componentNode);
                        break;
                    case "RadioButtonGroup":
                        newComp = parseRadioButtonGroupNode(componentNode);
                        break;
                    case "TextBlock":
                    case "TextBlock1":
                    case "TextBlock_1":
                        newComp = parseTextBlockNode(componentNode);
                        break;
                    case "OneDScale":
                        newComp = parseOneDScaleNode(componentNode);
                        break;
                    case "ListSelect":
                        newComp = parseListSelectNode(componentNode);
                        break;
                    case "KacPS":
                        newComp = parseKACPS(componentNode);
                        break;
                    case "TaggingA":
                    case "TaggingB":
                        newComp = parseTaggingNode(componentNode);
                        break;
                    case "EndOfExperiment": {
                        try {
                            exp.endExperiment();
                        } catch (InvalidActivityException e) {
                            throw new ExperimentParsingException("Experiment is invalid:\n" +
                                    exp.getStateReport());
                        }
                    } break;
                    default:
                        String errMsg = String.format("Unrecognized component" +
                                " type: %s", componentNode.getNodeName());
                        throw new ExperimentParsingException(errMsg);
                }

                // General parsing for all component types
                if (newComp != null) {
                    ChaosEvent[] events = parseEvents(componentNode);
                    newComp.addOutputEvents(events);
                    trial.addComponent(newComp);
                }
            }
        }

        return exp;
    }

    private static TextBlock parseTextBlockNode(Node componentNode)
            throws ExperimentParsingException
    {
        Node contentNode = findChildNode(componentNode, "Inputs", "Instrument", "Text");
        return new TextBlock(secureGetTextContent(contentNode));
    }

	private static RadioButtonGroup parseRadioButtonGroupNode(Node componentNode)
            throws ExperimentParsingException
    {
		RadioButtonGroup radioButtonGroup = new RadioButtonGroup();
		parseSelectable(componentNode, radioButtonGroup);

		// Set output value (id of selected option)
		Node idNode = findChildNode(componentNode, "Outputs", "Value", "Id");
		if (idNode != null && !secureGetTextContent(idNode).isEmpty()) {
            radioButtonGroup.setOutputValue(secureGetTextContent(idNode));
        }

		return radioButtonGroup;
	}

	private static Monitor parseMonitorNode(Node componentNode)
            throws ExperimentParsingException
    {
		Monitor monitor = new Monitor();
        Node typeNode = findChildNode(componentNode, "Outputs", "Value", "Context", "Type");
        Node dataNode = findChildNode(componentNode, "Outputs", "Value", "Context", "Data");

        if (typeNode != null) {
            monitor.contextType = secureGetTextContent(typeNode);
        }

        if (dataNode != null) {
            monitor.contextData = secureGetTextContent(dataNode);
        }

		return monitor;
	}

	private static void parseSelectable(Node componentNode, Selectable selectable)
            throws ExperimentParsingException
    {
		selectable.headerLabel = secureGetTextContent(componentNode, "Inputs", "Instrument",
                "HeaderLabel");

        Node alignNode = findChildNode(componentNode, "Inputs", "Instrument", "AlignForStimuli");
        if (alignNode != null) {
            selectable.alignForStimuli = secureGetTextContent(alignNode).equals("1");
        } else {
            selectable.alignForStimuli = false;
        }

		Node itemNodes = findChildNode(componentNode, "Inputs", "Instrument", "Items");

		int i = 0;
		for (Node itemNode = secureGetFirstChild(itemNodes);
			 itemNode != null;
			 itemNode = secureGetNextSibling(itemNode), i++)
		{
			String label = secureGetTextContent(itemNode, "Label");
			boolean selected = secureGetTextContent(itemNode, "Selected").equals("1");
			String id = secureGetTextContent(itemNode, "Id");

			selectable.addItem(label);
			selectable.setSelected(i, selected);
			selectable.setItemID(i, id);
		}

		// Set stimulus, if present
		Node stimulusNode = findChildNode(componentNode, "Inputs", "Instrument", "Stimulus");
		if (stimulusNode != null && stimulusNode.getChildNodes().getLength() > 0) {
			String URI = secureGetTextContent(stimulusNode, "URI");

            String label = "";
            Node labelNode = findChildNode(stimulusNode, "Label");
            if (labelNode != null) {
                label = secureGetTextContent(labelNode);
            }

			Node typeNode = findChildNode(stimulusNode, "Type");
            if (typeNode != null) {
                String type = secureGetTextContent(typeNode);
                selectable.setStimulus(URI, label, type);
            } else {
                selectable.setStimulus(URI, label);
            }
		}
	}

	private static LikertScale parseLikertScaleNode(Node componentNode)
            throws ExperimentParsingException
    {
		LikertScale likertScale = new LikertScale();
		parseSelectable(componentNode, likertScale);

		// Set output value (id of selected option)
		Node idNode = findChildNode(componentNode, "Outputs", "Value", "Id");
		if (idNode != null && !secureGetTextContent(idNode).isEmpty()) {
            likertScale.setOutputValue(secureGetTextContent(idNode));
        }

		return likertScale;
	}

	private static Header parseHeaderNode(Node componentNode)
            throws ExperimentParsingException
    {
		Node headerLabelNode = findChildNode(componentNode, "Inputs", "HeaderLabel");
		return new Header(secureGetTextContent(headerLabelNode));
	}

	private static FreeTextHash parseFreeTextHashNode(Node componentNode)
            throws ExperimentParsingException
    {
		String label = secureGetTextContent(componentNode, "Inputs",
                "Instrument", "Label");
		String labelPosition = secureGetTextContent(componentNode, "Inputs",
                "Instrument", "LabelPosition");
		String validation = secureGetTextContent(componentNode, "Inputs",
                "Instrument", "Validation");

        boolean forceLowerCase = false;
		try {
			forceLowerCase = secureGetTextContent(componentNode, "Inputs",
                    "Instrument", "ForceLowerCase").equals("1");
		} catch (NullPointerException e) {
			System.out.println("No ForceLowerCase field for FreeTextHash");
		}

        FreeTextHash freeTextHash = new FreeTextHash(label);
		freeTextHash.position = labelPosition;
		freeTextHash.validation = validation;
        freeTextHash.forceLowerCase = forceLowerCase;

        // Set output value (hash) and length
		Node textNode = findChildNode(componentNode, "Outputs", "Value", "Text");
		if (textNode != null) {
            freeTextHash.setOutputValue(secureGetTextContent(textNode));
        }

        Node lengthNode = findChildNode(componentNode, "Outputs", "Value", "Length");
        if (lengthNode != null) {
            String lengthStr = secureGetTextContent(lengthNode);
            freeTextHash.length = Integer.parseInt(lengthStr);
        }

		return freeTextHash;
	}

	private static FreeText parseFreeTextNode(Node componentNode)
            throws ExperimentParsingException
    {
		String label = secureGetTextContent(componentNode, "Inputs", "Instrument",
                "Label");

        FreeText freeText = new FreeText(label);

        Node labelPosNode = findChildNode(componentNode, "Inputs", "Instrument", "LabelPosition");
        if (labelPosNode != null) {
            freeText.labelPosition = secureGetTextContent(labelPosNode);
        }

		Node valNode= findChildNode(componentNode, "Inputs", "Instrument", "Validation");
        if (valNode != null) {
            freeText.validation  = secureGetTextContent(valNode);
        }

        Node boxWidthNode = findChildNode(componentNode, "Inputs", "Instrument", "BoxWidth");
        if (boxWidthNode != null) {
            String width = secureGetTextContent(boxWidthNode);
            try {
                if (!width.isEmpty()) {
                    freeText.boxWidth = Integer.parseInt(width);
                }
            } catch (NumberFormatException e) {
                throw new ExperimentParsingException("In box width field in Text Block component: "+
                        "Could not parse field as integer: \""+width+"\".");
            }
        }

        Node boxHeightNode = findChildNode(componentNode, "Inputs", "Instrument", "BoxHeight");
        if (boxHeightNode != null) {
            String height = secureGetTextContent(boxHeightNode);
            try {
                if (!height.isEmpty()) {
                    freeText.boxHeight = Integer.parseInt(height);
                }
            } catch (NumberFormatException e) {
                throw new ExperimentParsingException("In box height field in Text Block component: "+
                        "Could not parse field as integer: \""+height+"\".");
            }
        }

        Node resizableNode = findChildNode(componentNode, "Inputs", "Instrument", "Resizable");
        if (resizableNode != null) {
            switch(secureGetTextContent(resizableNode).toLowerCase()) {
                case "1":
                case "true":
                    freeText.resizable = true;
                    break;
                case "0":
                case "false":
                    freeText.resizable = false;
                    break;
                case "":
                    break;
                default: throw new ExperimentParsingException("Cannot parse resizable argument " +
                        "\""+secureGetTextContent(resizableNode)+"\" for FreeTextNode.");
            }
        }

		// Set output value (text)
        Node textNode = findChildNode(componentNode, "Outputs", "Value", "Text");
        if (textNode != null) {
            freeText.setOutputValue(secureGetTextContent(textNode));
        }
        return freeText;
	}

	private static CheckBoxGroup parseCheckBoxGroupNode(Node componentNode)
            throws ExperimentParsingException {
		CheckBoxGroup checkBoxGroup = new CheckBoxGroup();
		parseSelectable(componentNode, checkBoxGroup);

		// Set output value (id of selected option)
		Node selectionsNode = findChildNode(componentNode, "Outputs", "Value",
                "Selections");
		if (selectionsNode != null) {
			String selections = ""; // TODO should be list

			for (Node itemNode = secureGetFirstChild(selectionsNode);
				 itemNode != null;
				 itemNode = secureGetNextSibling(itemNode))
			{
                if (!selections.isEmpty()) {
                    selections += ", ";
                }
                selections += secureGetTextContent(itemNode);
			}

			checkBoxGroup.setOutputValue(selections);
		}

		return checkBoxGroup;
	}

    private static Tagging parseTaggingNode(Node componentNode)
            throws ExperimentParsingException {
        Tagging tagging;
        if (componentNode.getNodeName().equals("TaggingA")) {
            tagging = new TaggingA();
        } else if (componentNode.getNodeName().equals("TaggingB")) {
            tagging = new TaggingB();
        } else {
            throw new ExperimentParsingException("Unknown tagging component: " +
                    componentNode.getNodeName());
        }

        tagging.headerLabel = secureGetTextContent(componentNode, "Inputs",
                "Instrument", "HeaderLabel");
        tagging.selectionTagBoxLabel = secureGetTextContent(componentNode, "Inputs",
                "Instrument", "SelectionTagBoxLabel");
        tagging.userTagBoxLabel = secureGetTextContent(componentNode, "Inputs",
                "Instrument", "UserTagBoxLabel");
        tagging.textField = secureGetTextContent(componentNode, "Inputs",
                "Instrument", "TextField");

        Node selectionTagsNode = findChildNode(componentNode, "Inputs", "Instrument",
                "SelectionTags");
        for (Node node = secureGetFirstChild(selectionTagsNode);
             node != null;
             node = secureGetNextSibling(node))
        {
            String id = secureGetTextContent(node, "Id");
            String label = secureGetTextContent(node, "Label");
            int position = Integer.parseInt(secureGetTextContent(node, "Position"));
            tagging.addSelectionTag(label, position, id);
        }

        Node userTagsNode = findChildNode(componentNode, "Inputs", "Instrument",
                "UserTags");
        for (Node node = secureGetFirstChild(userTagsNode);
             node != null;
             node = secureGetNextSibling(node))
        {
            String id = secureGetTextContent(node, "Id");
            String label = secureGetTextContent(node, "Label");
            int position = Integer.parseInt(secureGetTextContent(node, "Position"));
            tagging.addUserTag(label, position, id);
        }

        // Set stimulus, if present
        Node stimulusNode = findChildNode(componentNode, "Inputs", "Instrument", "Stimulus");
        if (stimulusNode != null && stimulusNode.getChildNodes().getLength() > 0) {
            String URI = secureGetTextContent(stimulusNode, "URI");

            String label = "";
            Node labelNode = findChildNode(stimulusNode, "Label");
            if (labelNode != null) {
                label = secureGetTextContent(labelNode);
            }

            Node typeNode = findChildNode(stimulusNode, "Type");
            if (typeNode != null) {
                String type = secureGetTextContent(typeNode);
                tagging.setStimulus(URI, label, type);
            } else {
                tagging.setStimulus(URI, label);
            }
        }

        // Set output value (array of {id,label} of picked tags)
        Node tagsNode = findChildNode(componentNode, "Outputs", "Value",
                "Tags");
        if (tagsNode != null) {
            String tags = ""; // TODO should be list
            for (Node tagNode = secureGetFirstChild(tagsNode);
                 tagNode != null;
                 tagNode = secureGetNextSibling(tagNode))
            {
                if (!tags.isEmpty()) {
                    tags += ", ";
                }

                String id = secureGetTextContent(tagNode, "Id");
                String label = secureGetTextContent(tagNode, "Label");
                tags += "("+id+", "+label+")";
            }
            tagging.setOutputValue(tags);
        }

        return tagging;
    }

    private static ListSelect parseListSelectNode(Node componentNode)
            throws ExperimentParsingException
    {
        ListSelect listSelect = new ListSelect();
        parseSelectable(componentNode, listSelect);

        // Set output value (id of selected option)
        Node selectionsNode = findChildNode(componentNode, "Outputs", "Value",
                "Selections");
        if (selectionsNode != null) {
            String selections = ""; // TODO should be list

            for (Node itemNode = secureGetFirstChild(selectionsNode);
                 itemNode != null;
                 itemNode = secureGetNextSibling(itemNode))
            {
                if (!selections.isEmpty()) {
                    selections += ", ";
                }
                selections += secureGetTextContent(itemNode);
            }

            listSelect.setOutputValue(selections);
        }

        return listSelect;
    }

    private static OneDScale parseOneDScaleNode(Node componentNode)
            throws ExperimentParsingException
    {
        Node headerLabelNode = findChildNode(componentNode, "Inputs", "Instrument", "HeaderLabel");
        OneDScale oneDScale = new OneDScale();
        oneDScale.headerLabel = secureGetTextContent(headerLabelNode);

        Node inPositionNode = findChildNode(componentNode, "Inputs", "Instrument", "Position");
        try {
            oneDScale.position = Double.parseDouble(secureGetTextContent(inPositionNode));
        } catch (Exception e) {}

        Node alignNode = findChildNode(componentNode, "Inputs", "Instrument", "AlignForStimuli");
        if (alignNode != null) {
            oneDScale.alignForStimuli = "1".equals(secureGetTextContent(alignNode));
        } else {
            oneDScale.alignForStimuli = false;
        }

        // Parse axis ticks and positions
        String[] prefixes = new String[]{"X1", "X2", "Y1", "Y2"};
        for (int i = 0; i < prefixes.length; i++) {
            String prefix = prefixes[i];
            Node tickNode = findChildNode(componentNode, "Inputs", "Instrument",
                    prefix + "AxisTicks", prefix + "AxisTick");
            for (; tickNode != null; tickNode = secureGetNextSibling(tickNode)) {
                String label;
                double position;
                try {
                    label = secureGetTextContent(tickNode, "Label");
                } catch (NullPointerException e) {
                    label = "";
                }

                try {
                    String positionString = secureGetTextContent(tickNode, "Position");
                    position = Double.parseDouble(positionString);
                } catch (NullPointerException e) {
                    position = -1;
                }

                switch(prefix) {
                    case "X1":
                        oneDScale.addX1AxisTick(label, position);
                        break;
                    case "X2":
                        oneDScale.addX2AxisTick(label, position);
                        break;
                    case "Y1":
                        oneDScale.addY1AxisTick(label, position);
                        break;
                    case "Y2":
                        oneDScale.addY2AxisTick(label, position);
                        break;
                }
            }
        }

        // Parse stimulus
        Node stimulusNode = findChildNode(componentNode, "Inputs", "Instrument", "Stimulus");
        if (stimulusNode != null && stimulusNode.getChildNodes().getLength() > 0) {
            String type = secureGetTextContent(stimulusNode, "Type");
            String URI = secureGetTextContent(stimulusNode, "URI");
            String label = secureGetTextContent(stimulusNode, "Label");
            oneDScale.setStimulus(URI, label, type);
        }

        // Get out position if available
        Node outPositionNode = findChildNode(componentNode, "Outputs", "Value", "Position");
        if (outPositionNode != null && !secureGetTextContent(outPositionNode).isEmpty()) {
            oneDScale.setOutputValue(secureGetTextContent(outPositionNode));
        }

        return oneDScale;
    }

    private static KACPS parseKACPS(Node componentNode)
            throws ExperimentParsingException
    {
        String headerLabel = secureGetTextContent(componentNode, "Inputs", "Instrument", "HeaderLabel");
        KACPS kacps = new KACPS(headerLabel);

        // Parse items
        for (Node itemNode = findChildNode(componentNode, "Inputs", "Instrument", "Items", "Item");
             itemNode != null;
             itemNode = secureGetNextSibling(itemNode))
        {
            try {
                String itemId = secureGetTextContent(itemNode, "Id");
                String choiceLabel = secureGetTextContent(itemNode, "ChoiceButton", "Label");
                String choiceSel = secureGetTextContent(itemNode, "ChoiceButton", "Selected");

                Node stimNode = findChildNode(itemNode, "Stimulus");
                KACPS.KACPSItem item;
                if (stimNode != null &&
                    secureGetTextContent(stimNode) != null && !secureGetTextContent(stimNode).isEmpty())
                {
                    String stimString = secureGetTextContent(itemNode, "StimulusButton", "Label");
                    String stimURI = secureGetTextContent(stimNode, "URI");
                    item = kacps.addButton(choiceLabel, "1".equals(choiceSel), stimURI, stimString);
                } else {
                    item = kacps.addButton(choiceLabel, "1".equals(choiceSel));
                }
                item.itemId = itemId;

            } catch(Exception e){
                throw new ExperimentParsingException("Could not parse item in K-AC-PS component: " +
                        e.getMessage());
            }
        }

        // Parse output
        Node outputNode = findChildNode(componentNode, "Outputs", "Value", "Id");
        if (outputNode != null) {
            kacps.setOutputValue(secureGetTextContent(outputNode));
        }

        return kacps;
    }

	public static Node findChildNode(Node node, String... tagPath) {
		if (tagPath.length == 0) {
			return node;
		}

		// Pop tagPath
		String tag = tagPath[0];
		tagPath = Arrays.copyOfRange(tagPath, 1, tagPath.length);

		for(node = secureGetFirstChild(node);
			node != null;
			node = secureGetNextSibling(node))
		{
			if (node.getNodeName().equals(tag)) {
				return findChildNode(node, tagPath);
			}
		}

		return null;
	}
	
	private static Node secureGetFirstChild(Node node) {
		Node child = node.getFirstChild();
		while (child != null &&
			   child.getNodeType() == Node.TEXT_NODE &&
			   child.getNodeName().trim().equals("#text"))
		{
			child = secureGetNextSibling(child);
		}
		return child;
	}
	
	private static Node secureGetNextSibling(Node node) {
		node = node.getNextSibling();
		while (node != null &&
			   node.getNodeType() == Node.TEXT_NODE &&
			   node.getNodeName().trim().equals("#text"))
		{
			node = node.getNextSibling();
		}
		return node;
	}

    /**
     * Helper function for getting the text content of a node. If the provided node is null we throw
     * an ExperimentParserException. This is merely a convenience method for avoiding littering the
     * code with if-then-else clauses each time we need to get the text content of a node.
     * @param node
     * @return
     * @throws ExperimentParsingException
     */
	public static String secureGetTextContent(Node ancestor, String... tagPath)
            throws ExperimentParsingException
    {
        Node node = findChildNode(ancestor, tagPath);
        if (node == null) {
            String ppTagPath = tagPath[0];
            for (int i = 1; i < tagPath.length; i++) {
                ppTagPath += "->"+tagPath[i];
            }
            throw new ExperimentParsingException("Node \""+ancestor.getNodeName()+"\" has no" +
                    " descendant \""+ppTagPath+"\".");
        } else {
            return XMLTag.desanitize(node.getTextContent());
        }
    }
	
	private static ChaosEvent[] parseEvents(Node componentNode)
            throws ExperimentParsingException
    {
		Node eventsNode = findChildNode(componentNode, "Outputs", "Value",
                "Events");
		if (eventsNode == null) {
			return new ChaosEvent[]{};
		}

        LinkedList<ChaosEvent> eventList = new LinkedList<>();
		for (Node itemNode = secureGetFirstChild(eventsNode);
             itemNode != null;
             itemNode = secureGetNextSibling(itemNode))
        {
            if (!itemNode.getNodeName().equals("Item")) {
                continue;
            }

			ChaosEvent event = new ChaosEvent();
			event.id = secureGetTextContent(itemNode, "Id");
			event.type = secureGetTextContent(itemNode, "Type");
			event.method = secureGetTextContent(itemNode, "Method");
			event.data = secureGetTextContent(itemNode, "Data");
			event.datetime = secureGetTextContent(itemNode, "DateTime");
            eventList.add(event);
		}

        return Arrays.copyOf(eventList.toArray(), eventList.size(), ChaosEvent[].class);
	}

}
