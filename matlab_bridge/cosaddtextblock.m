function textblock = cosaddtextblock(trial, varargin)

if ~strcmp(char(trial.getClass.toString()), 'class com.MetaComponents.Trial')
    error('First argument must be Trial object.');
end

textblock = com.Components.TextBlock;
params = {'text'};

cosparseargs(textblock, params, varargin{:});

trial.addComponent(textblock);

end