function checkboxgroup = cosaddcheckboxgroup(trial, varargin)

if ~strcmp(char(trial.getClass.toString()), 'class com.MetaComponents.Trial')
    error('First argument must be Trial object.');
end

checkboxgroup = com.Components.CheckBoxGroup;
params = {'headerLabel','items', 'stimulus', 'minNoOfSelections',...
    'maxNoOfSelections', 'alignForStimuli', 'validationID'};

cosparseargs(checkboxgroup, params, varargin{:});

trial.addComponent(checkboxgroup);

end