function experiment = cosexperiment(varargin)

experiment = com.MetaComponents.Experiment;
params = {'id', 'name', 'targetId', 'targetName', 'revisionID',...
    'createdBy', 'experimentDescription', 'footerLabel',...
    'redirectOnCloseUrl', 'finalized', 'lockQuestion', 'enablePrevious'};

cosparseargs(experiment, params, varargin{:});

end