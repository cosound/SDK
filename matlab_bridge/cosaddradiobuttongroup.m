function radiobuttongroup = cosaddradiobuttongroup(trial, varargin)

if ~strcmp(char(trial.getClass.toString()), 'class com.MetaComponents.Trial')
    error('First argument must be Trial object.');
end

radiobuttongroup = com.Components.RadioButtonGroup;
params = {'headerLabel','items', 'stimulus', 'alignForStimuli',...
    'minNoOfScalings', 'validationID'};

cosparseargs(radiobuttongroup, params, varargin{:});

trial.addComponent(radiobuttongroup);

end