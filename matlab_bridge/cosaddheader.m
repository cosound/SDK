function header = cosaddheader(trial, varargin)
% h = cosaddheader(trial, paams...) creates a header component.
%
% Optional parameters:
%   HeaderLabel: The text to be displayed. Default is the an empty string.
%

if ~strcmp(char(trial.getClass.toString()), 'class com.MetaComponents.Trial')
    error('First argument must be Trial object.');
end

header = com.Components.Header;
params = {'headerLabel'};

cosparseargs(header, params, varargin{:});

trial.addComponent(header);

end