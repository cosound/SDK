function cosparseargs(object, params, varargin)

argstruct = struct;

if isempty(varargin)
    return
end

if mod(length(varargin), 2) ~= 0
    error('Argument list must be even.');
end

try
    for i = 1:length(varargin)
        if mod(i, 2) == 1
            varargin(i) = lower(varargin(i));
        else
            argstruct.(varargin{i-1}) = varargin{i};
        end
    end
catch
    error('Could not parse arguments.');
end

fs = fields(argstruct);
for i = 1:length(fs)
    param = fs{i};
    lparam = lower(param);
    value = argstruct.(lparam);
    
    if ~any(strcmpi(param, params))
        error(['Unknown parameter "' param '".']);
    else
        c = params(strcmpi(param, params));
        param = c{:};
    end

    try
        switch lparam
            case 'items'
                object.addItems(value);
            case 'stimulus'
                object.setStimulus(value);
            case 'alignforstimuli'
                object.(param) = parsebool(param, value);
            case 'resizable'
                object.(param) = parsebool(param, value);
            case 'finalized'
                object.(param) = parsebool(param, value);
            case 'lockquestion'
                object.(param) = parsebool(param, value);
            case 'enableprevious'
                object.(param) = parsebool(param, value);
            case 'x1ticks'
                for j = 1:length(value)
                    tick = value{j};
                    object.addX1AxisTick(tick{1}, tick{2});
                end
            case 'x2ticks'
                for j = 1:length(value)
                    tick = value{j};
                    object.addX2AxisTick(tick{1}, tick{2});
                end
            case 'y1ticks'
                for j = 1:length(value)
                    tick = value{j};
                    object.addY1AxisTick(tick{1}, tick{2});
                end
            case 'y2ticks'
                for j = 1:length(value)
                    tick = value{j};
                    object.addY2AxisTick(tick{1}, tick{2});
                end
            case 'selectiontags'
                for j = 1:length(value)
                    object.addSelectionTag(value{j});
                end
            case 'usertags'
                for j = 1:length(value)
                    object.addUserTag(value{j});
                end
            case 'position'
                object.(param) = java.lang.Double(value);
            otherwise
                object.(param) = value;
        end
    catch
        error(['Could not parse value for parameter "' param '".']);
    end
end

function boolval = parsebool(param, boolarg)
    if boolarg == 1
        boolval = true;
    elseif boolarg == 0
        boolval = false;
    else
        error(['Parameter "' param '" must have boolean type.']);
    end
end

function assigngeneric(object, param, value)
    try
        c = char(object.(param).class);
    catch
        % object field is simple type, so we assign and hope for the best
        object.(param) = value;
        return
    end
    
    switch c
        case 'java.lang.Double'
            object.(param) = java.lang.Double(value);
        case 'java.lang.Integer'
            object.(param) = java.lang.Integer(value);
        case 'java.lang.Float'            
            object.(param) = java.lang.Float(value);
        case 'java.lang.Long'
            object.(param) = java.lang.Long(value);
        case 'java.lang.Boolean'           
            object.(param) = java.lang.Boolean(parsebool(param, value));
        case 'java.lang.String'
            object.(param) = char(value);
        otherwise
            error(['Unrecognized Java class: ' c '.']);
    end 
end
end
