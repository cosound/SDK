function allanswers = cosgetanswers(apikey, experimentid, varargin)
% cosgetanswers(apikey, experiments, params) logs into the server with
% 'apikey' and retrieves the answers for the experiment with id
% 'experimentid'.
% 
% Optional parameters:
%     Env (String)       : 'dev' for development environment and 'prod' for
%                          production environment. Default is 'dev'.


argstruct = struct('env', 'dev');
if mod(length(varargin), 2) ~= 0
    error('Argument list must be even.');
end

try
    for j = 1:length(varargin)
        if mod(j, 2) == 1
            varargin(j) = lower(varargin(j));
        else
            argstruct.(varargin{j-1}) = varargin{j};
        end
    end
catch
    error('Could not parse arguments.');
end

if length(fields(argstruct)) ~= 1
    error('Unrecognized parameter.')
end

try
    session = com.Utilities.ChaosSDK(apikey, argstruct.env);
catch e
    error(['Could not login: ' e.message]);
end

j_trials = session.getExperimentAnswers(experimentid);

allanswers = cell(j_trials.size(), 1);
for i = 1:j_trials.size()
    j_trialanswers = j_trials.get(i-1);
    trialanswers = cell(j_trialanswers.size(), 1);
    for j = 1:j_trialanswers.size()
        j_answer = j_trialanswers.get(j-1);
        headerlabel = char(j_answer.headerLabel);
        componenttype = char(j_answer.componentType);
        stimuluslabel = char(j_answer.stimulusLabel);
        stimulusuri = char(j_answer.stimulusURI);
        response = char(j_answer.response);
        events = [];
        for k = 1:j_answer.events.size()
            event = j_answer.events.get(k-1);
            events = [events, struct(...
                'Id', char(event.id),...
                'Type', char(event.type),...
                'Method', char(event.method),...
                'Data', char(event.data),...
                'DateTime', char(event.datetime))];
        end
        trialanswers{j} = struct(...
            'HeaderLabel', headerlabel,...
            'Type', componenttype,...
            'StimulusLabel', stimuluslabel,...
            'StimulusURI', stimulusuri,...
            'Response', response,...
            'Events', events);
    end
    allanswers{i} = trialanswers;
end
end