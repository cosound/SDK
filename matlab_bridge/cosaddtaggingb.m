function taggingb = cosaddtaggingb(trial, varargin)
% comp = cosaddtagginga(trial, params) adds a TaggingB component
%        to the supplied trial.
% 
% Required parameters:
%     HeaderLabel (String)
%     SelectionTagBoxLabel (String)
%     UserTagBoxLabel (String)
%     TextField (String)
%     SelectionTags (String array) : Array of labels for selection tags
%     UserTags (String array)      : Array of labels for user tags
% 
% 
% Optional parameters:
%     Stimulus (Stimulus)  : Related stimulus of form {URI, label}
% 

if ~strcmp(char(trial.getClass.toString()), 'class com.MetaComponents.Trial')
    error('First argument must be Trial object.');
end

taggingb = com.Components.TaggingB;
params = {'headerLabel', 'selectionTagBoxLabel', 'userTagBoxLabel',...
    'textField', 'selectionTags', 'userTags', 'stimulus'};

cosparseargs(taggingb, params, varargin{:});

trial.addComponent(taggingb);

end