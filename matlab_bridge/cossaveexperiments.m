function cossaveexperiments(apikey, experiments, varargin)
% cosuploadexperiments(apikey, experiments, params) logs in to the server
% with 'apikey' and retrieves the experiments with id's in listed in
% 'experiments'. The XML for those experiments are then saved in the
% working directory or 'TargetDir' if such is set.
% 
% Optional parameters:
%     Env (String)       : 'dev' for development environment and 'prod' for
%                          production environment. Default is 'dev'.
%     TargetDir (String) : Directory of XML-files, either absolute or
%                          relative to working directory. Default is '.'.


argstruct = struct('targetdir', '.', 'env', 'dev');
if mod(length(varargin), 2) ~= 0
    error('Argument list must be even.');
end

try
    for i = 1:length(varargin)
        if mod(i, 2) == 1
            varargin(i) = lower(varargin(i));
        else
            argstruct.(varargin{i-1}) = varargin{i};
        end
    end
catch
    error('Could not parse arguments.');
end

if length(fields(argstruct)) ~= 2
    error('Unrecognized parameter.')
end

if isempty(experiments)
    error('Empty experiment list.');
end

try
    session = com.Utilities.ChaosSDK(apikey, argstruct.env);
catch e
    error(['Could not login: ' e.message]);
end

noexperiments = length(experiments);
for i = 1:noexperiments
    exid = experiments{i};
    try
        fprintf('Retrieving experiment %d/%d...\n', i, noexperiments);
        xml = char(session.getExperimentXML(exid));

        fprintf('Writing XML file...\n');
        f = fullfile(argstruct.targetdir, [exid '.xml']);
        fid = fopen(f, 'w');
        fprintf(fid, '%s', xml);
        fclose(fid);

        fprintf('Done.\n');
    catch E
        fprintf('Error while saving experiment %d.\n', i);
        rethrow(E);
    end
end

end