function experimentids = cosuploadexperiments(apikey, varargin)
% ids = cosuploadexperiments(apikey, params) logs in to the server with
%       apikey and uploads experiments in params. The associated ID's are
%        returned.
% 
% Arguments params must have format: ['env', envarg, experiments...]
% where envarg is either 'dev' (for development server) or 'prod' (for
% producion server). Argument experiments is a variable sized list of
% experiments to be uploaded.

if strcmpi('env', varargin{1})
    if strcmpi('dev', varargin{2})
        prodenv = false;
    elseif strcmpi('prod', varargin{2})
        prodenv = true;
    else
        error('Argument for "env" must be either "dev" or "prod"');
    end
    experiments = varargin{3:end};
else
    prodenv = false;
    experiments = varargin{:};
end

if isempty(experiments)
    error('Empty experiment list.');
end

% Check validity first for all-or-nothing semantics
for i = 1:length(experiments)
    experiment = experiments(i);
    if ~strcmp(char(experiment.getClass.toString()), 'class com.MetaComponents.Experiment')
        error('Experiment %d has incorrect class.', i);
    end
    
    if ~experiment.isValid()
        error(sprintf('Experiment %d is invalid.', i));
    end
end

try
    if prodenv
        session = com.Utilities.ChaosSDK(apikey, 'prod');
    else
        session = com.Utilities.ChaosSDK(apikey, 'dev');
    end
catch e
    error(['Could not login: ' e]);
end

noexperiments = length(experiments);
experimentids = cell(1, noexperiments);

for i = 1:noexperiments
    experiment = experiments(i);
    try
        session.commitExperiment(experiment);
        fprintf('Experiment %d/%d was uploaded.\n', i, noexperiments);
        % TODO print xml and json link
        
        experimentids(i) = experiment.id;
    catch e
        fprintf('Error while uploading experiment %d. ', i);
        fprintf('Undoing changes...\n');
        
        % clean up
        for staleexperimentid = experimentids
            if isempty(staleexperimentid)
                break;
            end
            
            try
                session.deleteObject(staleexperimentid);
            catch
                % ignore
            end
        end
        
        error('Error while uploading experiment %d:\n%s.\n', i, e.message);
    end
end

end