function cosreindex(apikey, varargin)
% cosreindex(apikey, params) reindexes the database.
% 
% Optional parameters:
%     Env (String)       : 'dev' for development environment and 'prod' for
%                          production environment. Default is 'dev'.


argstruct = struct('env', 'dev');
if mod(length(varargin), 2) ~= 0
    error('Argument list must be even.');
end

try
    for j = 1:length(varargin)
        if mod(j, 2) == 1
            varargin(j) = lower(varargin(j));
        else
            argstruct.(varargin{j-1}) = varargin{j};
        end
    end
catch
    error('Could not parse arguments.');
end

if length(fields(argstruct)) ~= 1
    error('Unrecognized parameter.')
end

try
    session = com.Utilities.ChaosSDK(apikey, argstruct.env);
catch e
    error(['Could not login: ' e.message]);
end

session.reindexDatabase();

end