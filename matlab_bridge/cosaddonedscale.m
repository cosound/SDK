function onedscale = cosaddonedscale(trial, varargin)
% comp = cosaddonedscale(trial, params) adds a OneDScale
%        to the supplied trial.
% 
% Required parameters:
%     HeaderLabel (String)
%     X1Ticks (List)       : Array of ticks along the x1 axis of the form {{label, position}, ...}
%     X2Ticks (List)       : Same as above
%     Y1Ticks (List)       : Same as above
%     Y2Ticks (List)       : Same as above
%     AlignForStimuli (Boolean)
% 
% 
% Optional parameters:
%     Position (Double)    : Start position of slider, must be in range [0,1]
%     Stimulus (Stimulus)  : Related stimulus of form {URI, label}
%     X1AxisLabel (String)
%     X2AxisLabel (String)
%     Y1AxisLabel (String)
%     Y2AxisLabel (String)
% 

if ~strcmp(char(trial.getClass.toString()), 'class com.MetaComponents.Trial')
    error('First argument must be Trial object.');
end

onedscale = com.Components.OneDScale;
params = {'headerLabel', 'x1Ticks', 'x2Ticks', 'y1Ticks', 'y2Ticks',...
    'x1AxisLabel', 'x2AxisLabel', 'y1AxisLabel', 'y2AxisLabel',...
    'stimulus', 'position', 'alignForStimuli'};

cosparseargs(onedscale, params, varargin{:});

trial.addComponent(onedscale);

end