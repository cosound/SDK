function freetexthash = cosaddfreetexthash(trial, varargin)

if ~strcmp(char(trial.getClass.toString()), 'class com.MetaComponents.Trial')
    error('First argument must be Trial object.');
end

freetexthash = com.Components.FreeTextHash;
params = {'label', 'labelPosition', 'validation', 'forceLowerCase'};

cosparseargs(freetexthash, params, varargin{:});

trial.addComponent(freetexthash);

end