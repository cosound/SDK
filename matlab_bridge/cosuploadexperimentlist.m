function experimentlist = cosuploadexperimentlist(apikey, experiments, varargin)
% explist = cosuploadexperimentlist(apikey, params, experiments...) creates
%           an experiment list with the provided experiments and uploads
%           the list and the experiments to the server. Experiments already
%           on the server will not be overridden.
%
% Optional parameters:
%   env (String): The environment to which to upload the experiment list
%                 and associated experiments. 'dev' for development and
%                 'prod' for production. Default is 'dev'.
%   id (String):  The ID for the experiment list. If none is provided a
%                 random will be created.
% 

prodenv = false;
argstruct = struct;
experimentlist = com.MetaComponents.ExperimentList;

try
    for exp = 1:2:length(varargin)
        if strcmpi(varargin{exp}, 'env')
            if strcmpi('dev', varargin{2})
                prodenv = false;
            elseif strcmpi('prod', varargin{2})
                prodenv = true;
            else
                error;
            end
        elseif strcmpi(varargin{exp}, 'id')
            experimentlist.id = varargin{exp+1};
        else
            error;
        end 
    end
catch
    error('Could not parse arguments.');
end

if isempty(experiments)
    error('Empty experiment list.');
end

for i = 1:length(experiments)
    experimentlist.addExperiment(experiments{i});
end

try
    if prodenv
        session = com.Utilities.ChaosSDK(apikey, 'prod');
    else
        session = com.Utilities.ChaosSDK(apikey, 'dev');
    end
catch e
    error(['Could not login: ' e]);
end

experimentlist = session.commitExperimentList(experimentlist);

end