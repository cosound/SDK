function tagginga = cosaddtagginga(trial, varargin)
% comp = cosaddtagginga(trial, params) adds a TaggingA component
%        to the supplied trial.
% 
% Required parameters:
%     HeaderLabel (String)
%     SelectionTagBoxLabel (String)
%     UserTagBoxLabel (String)
%     TextField (String)
%     SelectionTags (String array) : Array of labels for selection tags
%     UserTags (String array)      : Array of labels for user tags
% 
% 
% Optional parameters:
%     Stimulus (Stimulus)  : Related stimulus of form {URI, label}
% 

if ~strcmp(char(trial.getClass.toString()), 'class com.MetaComponents.Trial')
    error('First argument must be Trial object.');
end

tagginga = com.Components.TaggingA;
params = {'headerLabel', 'selectionTagBoxLabel', 'userTagBoxLabel',...
    'textField', 'selectionTags', 'userTags', 'stimulus'};

cosparseargs(tagginga, params, varargin{:});

trial.addComponent(tagginga);

end