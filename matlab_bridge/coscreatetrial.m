function trial = coscreatetrial(experiment)

if ~strcmp(char(experiment.getClass.toString()), 'class com.MetaComponents.Experiment')
    error('First argument must be Experiment object.');
end

trial = experiment.createTrial();

end