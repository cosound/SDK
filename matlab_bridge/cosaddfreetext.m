function freetext = cosaddfreetext(trial, varargin)

if ~strcmp(char(trial.getClass.toString()), 'class com.MetaComponents.Trial')
    error('First argument must be Trial object.');
end

freetext = com.Components.FreeText;
params = {'label','labelPosition', 'boxWidth', 'boxHeight',...
    'validation', 'resizable', 'alignForStimuli'};

cosparseargs(freetext, params, varargin{:});

trial.addComponent(freetext);

end