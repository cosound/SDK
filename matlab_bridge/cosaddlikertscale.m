function likertscale = cosaddlikertscale(trial, varargin)

if ~strcmp(char(trial.getClass.toString()), 'class com.MetaComponents.Trial')
    error('First argument must be Trial object.');
end

likertscale = com.Components.LikertScale;
params = {'headerLabel', 'items', 'stimulus', 'alignForStimuli',...
    'minNoOfScalings', 'validationID'};

cosparseargs(likertscale, params, varargin{:});

trial.addComponent(likertscale);

end